/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchAcademicRecordException;
import info.diit.portal.model.AcademicRecord;
import info.diit.portal.model.impl.AcademicRecordImpl;
import info.diit.portal.model.impl.AcademicRecordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the academic record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AcademicRecordPersistence
 * @see AcademicRecordUtil
 * @generated
 */
public class AcademicRecordPersistenceImpl extends BasePersistenceImpl<AcademicRecord>
	implements AcademicRecordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AcademicRecordUtil} to access the academic record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AcademicRecordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST =
		new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AcademicRecordImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentAcademicRecordList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST =
		new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AcademicRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentAcademicRecordList",
			new String[] { Long.class.getName() },
			AcademicRecordModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTACADEMICRECORDLIST =
		new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentAcademicRecordList",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AcademicRecordImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AcademicRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the academic record in the entity cache if it is enabled.
	 *
	 * @param academicRecord the academic record
	 */
	public void cacheResult(AcademicRecord academicRecord) {
		EntityCacheUtil.putResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordImpl.class, academicRecord.getPrimaryKey(),
			academicRecord);

		academicRecord.resetOriginalValues();
	}

	/**
	 * Caches the academic records in the entity cache if it is enabled.
	 *
	 * @param academicRecords the academic records
	 */
	public void cacheResult(List<AcademicRecord> academicRecords) {
		for (AcademicRecord academicRecord : academicRecords) {
			if (EntityCacheUtil.getResult(
						AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AcademicRecordImpl.class, academicRecord.getPrimaryKey()) == null) {
				cacheResult(academicRecord);
			}
			else {
				academicRecord.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all academic records.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AcademicRecordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AcademicRecordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the academic record.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AcademicRecord academicRecord) {
		EntityCacheUtil.removeResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordImpl.class, academicRecord.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AcademicRecord> academicRecords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AcademicRecord academicRecord : academicRecords) {
			EntityCacheUtil.removeResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AcademicRecordImpl.class, academicRecord.getPrimaryKey());
		}
	}

	/**
	 * Creates a new academic record with the primary key. Does not add the academic record to the database.
	 *
	 * @param academicRecordId the primary key for the new academic record
	 * @return the new academic record
	 */
	public AcademicRecord create(long academicRecordId) {
		AcademicRecord academicRecord = new AcademicRecordImpl();

		academicRecord.setNew(true);
		academicRecord.setPrimaryKey(academicRecordId);

		return academicRecord;
	}

	/**
	 * Removes the academic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param academicRecordId the primary key of the academic record
	 * @return the academic record that was removed
	 * @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord remove(long academicRecordId)
		throws NoSuchAcademicRecordException, SystemException {
		return remove(Long.valueOf(academicRecordId));
	}

	/**
	 * Removes the academic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the academic record
	 * @return the academic record that was removed
	 * @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AcademicRecord remove(Serializable primaryKey)
		throws NoSuchAcademicRecordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AcademicRecord academicRecord = (AcademicRecord)session.get(AcademicRecordImpl.class,
					primaryKey);

			if (academicRecord == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAcademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(academicRecord);
		}
		catch (NoSuchAcademicRecordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AcademicRecord removeImpl(AcademicRecord academicRecord)
		throws SystemException {
		academicRecord = toUnwrappedModel(academicRecord);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, academicRecord);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(academicRecord);

		return academicRecord;
	}

	@Override
	public AcademicRecord updateImpl(
		info.diit.portal.model.AcademicRecord academicRecord, boolean merge)
		throws SystemException {
		academicRecord = toUnwrappedModel(academicRecord);

		boolean isNew = academicRecord.isNew();

		AcademicRecordModelImpl academicRecordModelImpl = (AcademicRecordModelImpl)academicRecord;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, academicRecord, merge);

			academicRecord.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AcademicRecordModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((academicRecordModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(academicRecordModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTACADEMICRECORDLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST,
					args);

				args = new Object[] {
						Long.valueOf(academicRecordModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTACADEMICRECORDLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST,
					args);
			}
		}

		EntityCacheUtil.putResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordImpl.class, academicRecord.getPrimaryKey(),
			academicRecord);

		return academicRecord;
	}

	protected AcademicRecord toUnwrappedModel(AcademicRecord academicRecord) {
		if (academicRecord instanceof AcademicRecordImpl) {
			return academicRecord;
		}

		AcademicRecordImpl academicRecordImpl = new AcademicRecordImpl();

		academicRecordImpl.setNew(academicRecord.isNew());
		academicRecordImpl.setPrimaryKey(academicRecord.getPrimaryKey());

		academicRecordImpl.setAcademicRecordId(academicRecord.getAcademicRecordId());
		academicRecordImpl.setCompanyId(academicRecord.getCompanyId());
		academicRecordImpl.setUserId(academicRecord.getUserId());
		academicRecordImpl.setUserName(academicRecord.getUserName());
		academicRecordImpl.setCreateDate(academicRecord.getCreateDate());
		academicRecordImpl.setModifiedDate(academicRecord.getModifiedDate());
		academicRecordImpl.setOrganisationId(academicRecord.getOrganisationId());
		academicRecordImpl.setDegree(academicRecord.getDegree());
		academicRecordImpl.setBoard(academicRecord.getBoard());
		academicRecordImpl.setYear(academicRecord.getYear());
		academicRecordImpl.setResult(academicRecord.getResult());
		academicRecordImpl.setRegistrationNo(academicRecord.getRegistrationNo());
		academicRecordImpl.setStudentId(academicRecord.getStudentId());

		return academicRecordImpl;
	}

	/**
	 * Returns the academic record with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the academic record
	 * @return the academic record
	 * @throws com.liferay.portal.NoSuchModelException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AcademicRecord findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the academic record with the primary key or throws a {@link info.diit.portal.NoSuchAcademicRecordException} if it could not be found.
	 *
	 * @param academicRecordId the primary key of the academic record
	 * @return the academic record
	 * @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord findByPrimaryKey(long academicRecordId)
		throws NoSuchAcademicRecordException, SystemException {
		AcademicRecord academicRecord = fetchByPrimaryKey(academicRecordId);

		if (academicRecord == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + academicRecordId);
			}

			throw new NoSuchAcademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				academicRecordId);
		}

		return academicRecord;
	}

	/**
	 * Returns the academic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the academic record
	 * @return the academic record, or <code>null</code> if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AcademicRecord fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the academic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param academicRecordId the primary key of the academic record
	 * @return the academic record, or <code>null</code> if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord fetchByPrimaryKey(long academicRecordId)
		throws SystemException {
		AcademicRecord academicRecord = (AcademicRecord)EntityCacheUtil.getResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AcademicRecordImpl.class, academicRecordId);

		if (academicRecord == _nullAcademicRecord) {
			return null;
		}

		if (academicRecord == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				academicRecord = (AcademicRecord)session.get(AcademicRecordImpl.class,
						Long.valueOf(academicRecordId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (academicRecord != null) {
					cacheResult(academicRecord);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AcademicRecordImpl.class, academicRecordId,
						_nullAcademicRecord);
				}

				closeSession(session);
			}
		}

		return academicRecord;
	}

	/**
	 * Returns all the academic records where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findByStudentAcademicRecordList(long studentId)
		throws SystemException {
		return findByStudentAcademicRecordList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the academic records where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of academic records
	 * @param end the upper bound of the range of academic records (not inclusive)
	 * @return the range of matching academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findByStudentAcademicRecordList(
		long studentId, int start, int end) throws SystemException {
		return findByStudentAcademicRecordList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the academic records where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of academic records
	 * @param end the upper bound of the range of academic records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findByStudentAcademicRecordList(
		long studentId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTACADEMICRECORDLIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<AcademicRecord> list = (List<AcademicRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AcademicRecord academicRecord : list) {
				if ((studentId != academicRecord.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_ACADEMICRECORD_WHERE);

			query.append(_FINDER_COLUMN_STUDENTACADEMICRECORDLIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<AcademicRecord>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first academic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching academic record
	 * @throws info.diit.portal.NoSuchAcademicRecordException if a matching academic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord findByStudentAcademicRecordList_First(
		long studentId, OrderByComparator orderByComparator)
		throws NoSuchAcademicRecordException, SystemException {
		AcademicRecord academicRecord = fetchByStudentAcademicRecordList_First(studentId,
				orderByComparator);

		if (academicRecord != null) {
			return academicRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAcademicRecordException(msg.toString());
	}

	/**
	 * Returns the first academic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching academic record, or <code>null</code> if a matching academic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord fetchByStudentAcademicRecordList_First(
		long studentId, OrderByComparator orderByComparator)
		throws SystemException {
		List<AcademicRecord> list = findByStudentAcademicRecordList(studentId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last academic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching academic record
	 * @throws info.diit.portal.NoSuchAcademicRecordException if a matching academic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord findByStudentAcademicRecordList_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchAcademicRecordException, SystemException {
		AcademicRecord academicRecord = fetchByStudentAcademicRecordList_Last(studentId,
				orderByComparator);

		if (academicRecord != null) {
			return academicRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAcademicRecordException(msg.toString());
	}

	/**
	 * Returns the last academic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching academic record, or <code>null</code> if a matching academic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord fetchByStudentAcademicRecordList_Last(
		long studentId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByStudentAcademicRecordList(studentId);

		List<AcademicRecord> list = findByStudentAcademicRecordList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the academic records before and after the current academic record in the ordered set where studentId = &#63;.
	 *
	 * @param academicRecordId the primary key of the current academic record
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next academic record
	 * @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord[] findByStudentAcademicRecordList_PrevAndNext(
		long academicRecordId, long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchAcademicRecordException, SystemException {
		AcademicRecord academicRecord = findByPrimaryKey(academicRecordId);

		Session session = null;

		try {
			session = openSession();

			AcademicRecord[] array = new AcademicRecordImpl[3];

			array[0] = getByStudentAcademicRecordList_PrevAndNext(session,
					academicRecord, studentId, orderByComparator, true);

			array[1] = academicRecord;

			array[2] = getByStudentAcademicRecordList_PrevAndNext(session,
					academicRecord, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AcademicRecord getByStudentAcademicRecordList_PrevAndNext(
		Session session, AcademicRecord academicRecord, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ACADEMICRECORD_WHERE);

		query.append(_FINDER_COLUMN_STUDENTACADEMICRECORDLIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(academicRecord);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AcademicRecord> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the academic records.
	 *
	 * @return the academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the academic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of academic records
	 * @param end the upper bound of the range of academic records (not inclusive)
	 * @return the range of academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the academic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of academic records
	 * @param end the upper bound of the range of academic records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AcademicRecord> list = (List<AcademicRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ACADEMICRECORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ACADEMICRECORD;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AcademicRecord>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AcademicRecord>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the academic records where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentAcademicRecordList(long studentId)
		throws SystemException {
		for (AcademicRecord academicRecord : findByStudentAcademicRecordList(
				studentId)) {
			remove(academicRecord);
		}
	}

	/**
	 * Removes all the academic records from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AcademicRecord academicRecord : findAll()) {
			remove(academicRecord);
		}
	}

	/**
	 * Returns the number of academic records where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching academic records
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentAcademicRecordList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTACADEMICRECORDLIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ACADEMICRECORD_WHERE);

			query.append(_FINDER_COLUMN_STUDENTACADEMICRECORDLIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTACADEMICRECORDLIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of academic records.
	 *
	 * @return the number of academic records
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ACADEMICRECORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the academic record persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.AcademicRecord")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AcademicRecord>> listenersList = new ArrayList<ModelListener<AcademicRecord>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AcademicRecord>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AcademicRecordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ACADEMICRECORD = "SELECT academicRecord FROM AcademicRecord academicRecord";
	private static final String _SQL_SELECT_ACADEMICRECORD_WHERE = "SELECT academicRecord FROM AcademicRecord academicRecord WHERE ";
	private static final String _SQL_COUNT_ACADEMICRECORD = "SELECT COUNT(academicRecord) FROM AcademicRecord academicRecord";
	private static final String _SQL_COUNT_ACADEMICRECORD_WHERE = "SELECT COUNT(academicRecord) FROM AcademicRecord academicRecord WHERE ";
	private static final String _FINDER_COLUMN_STUDENTACADEMICRECORDLIST_STUDENTID_2 =
		"academicRecord.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "academicRecord.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AcademicRecord exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AcademicRecord exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AcademicRecordPersistenceImpl.class);
	private static AcademicRecord _nullAcademicRecord = new AcademicRecordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AcademicRecord> toCacheModel() {
				return _nullAcademicRecordCacheModel;
			}
		};

	private static CacheModel<AcademicRecord> _nullAcademicRecordCacheModel = new CacheModel<AcademicRecord>() {
			public AcademicRecord toEntityModel() {
				return _nullAcademicRecord;
			}
		};
}
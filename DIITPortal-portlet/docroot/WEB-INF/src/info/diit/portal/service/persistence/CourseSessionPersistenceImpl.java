/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchCourseSessionException;
import info.diit.portal.model.CourseSession;
import info.diit.portal.model.impl.CourseSessionImpl;
import info.diit.portal.model.impl.CourseSessionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the course session service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseSessionPersistence
 * @see CourseSessionUtil
 * @generated
 */
public class CourseSessionPersistenceImpl extends BasePersistenceImpl<CourseSession>
	implements CourseSessionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CourseSessionUtil} to access the course session persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CourseSessionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SESSIONNAME =
		new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBysessionName",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SESSIONNAME =
		new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBysessionName", new String[] { String.class.getName() },
			CourseSessionModelImpl.SESSIONNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SESSIONNAME = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBysessionName",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSEID = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCourseId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID =
		new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCourseId", new String[] { Long.class.getName() },
			CourseSessionModelImpl.COURSEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEID = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCourseId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByorganization", new String[] { Long.class.getName() },
			CourseSessionModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBycompany", new String[] { Long.class.getName() },
			CourseSessionModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED,
			CourseSessionImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the course session in the entity cache if it is enabled.
	 *
	 * @param courseSession the course session
	 */
	public void cacheResult(CourseSession courseSession) {
		EntityCacheUtil.putResult(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionImpl.class, courseSession.getPrimaryKey(),
			courseSession);

		courseSession.resetOriginalValues();
	}

	/**
	 * Caches the course sessions in the entity cache if it is enabled.
	 *
	 * @param courseSessions the course sessions
	 */
	public void cacheResult(List<CourseSession> courseSessions) {
		for (CourseSession courseSession : courseSessions) {
			if (EntityCacheUtil.getResult(
						CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
						CourseSessionImpl.class, courseSession.getPrimaryKey()) == null) {
				cacheResult(courseSession);
			}
			else {
				courseSession.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all course sessions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CourseSessionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CourseSessionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the course session.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CourseSession courseSession) {
		EntityCacheUtil.removeResult(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionImpl.class, courseSession.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CourseSession> courseSessions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CourseSession courseSession : courseSessions) {
			EntityCacheUtil.removeResult(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
				CourseSessionImpl.class, courseSession.getPrimaryKey());
		}
	}

	/**
	 * Creates a new course session with the primary key. Does not add the course session to the database.
	 *
	 * @param sessionId the primary key for the new course session
	 * @return the new course session
	 */
	public CourseSession create(long sessionId) {
		CourseSession courseSession = new CourseSessionImpl();

		courseSession.setNew(true);
		courseSession.setPrimaryKey(sessionId);

		return courseSession;
	}

	/**
	 * Removes the course session with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param sessionId the primary key of the course session
	 * @return the course session that was removed
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession remove(long sessionId)
		throws NoSuchCourseSessionException, SystemException {
		return remove(Long.valueOf(sessionId));
	}

	/**
	 * Removes the course session with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the course session
	 * @return the course session that was removed
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseSession remove(Serializable primaryKey)
		throws NoSuchCourseSessionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CourseSession courseSession = (CourseSession)session.get(CourseSessionImpl.class,
					primaryKey);

			if (courseSession == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCourseSessionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(courseSession);
		}
		catch (NoSuchCourseSessionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CourseSession removeImpl(CourseSession courseSession)
		throws SystemException {
		courseSession = toUnwrappedModel(courseSession);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, courseSession);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(courseSession);

		return courseSession;
	}

	@Override
	public CourseSession updateImpl(
		info.diit.portal.model.CourseSession courseSession, boolean merge)
		throws SystemException {
		courseSession = toUnwrappedModel(courseSession);

		boolean isNew = courseSession.isNew();

		CourseSessionModelImpl courseSessionModelImpl = (CourseSessionModelImpl)courseSession;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, courseSession, merge);

			courseSession.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CourseSessionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((courseSessionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SESSIONNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						courseSessionModelImpl.getOriginalSessionName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SESSIONNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SESSIONNAME,
					args);

				args = new Object[] { courseSessionModelImpl.getSessionName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SESSIONNAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SESSIONNAME,
					args);
			}

			if ((courseSessionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSessionModelImpl.getOriginalCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID,
					args);

				args = new Object[] {
						Long.valueOf(courseSessionModelImpl.getCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID,
					args);
			}

			if ((courseSessionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSessionModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(courseSessionModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((courseSessionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSessionModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(courseSessionModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}
		}

		EntityCacheUtil.putResult(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
			CourseSessionImpl.class, courseSession.getPrimaryKey(),
			courseSession);

		return courseSession;
	}

	protected CourseSession toUnwrappedModel(CourseSession courseSession) {
		if (courseSession instanceof CourseSessionImpl) {
			return courseSession;
		}

		CourseSessionImpl courseSessionImpl = new CourseSessionImpl();

		courseSessionImpl.setNew(courseSession.isNew());
		courseSessionImpl.setPrimaryKey(courseSession.getPrimaryKey());

		courseSessionImpl.setSessionId(courseSession.getSessionId());
		courseSessionImpl.setCompanyId(courseSession.getCompanyId());
		courseSessionImpl.setOrganizationId(courseSession.getOrganizationId());
		courseSessionImpl.setUserId(courseSession.getUserId());
		courseSessionImpl.setUserName(courseSession.getUserName());
		courseSessionImpl.setCreateDate(courseSession.getCreateDate());
		courseSessionImpl.setModifiedDate(courseSession.getModifiedDate());
		courseSessionImpl.setSessionName(courseSession.getSessionName());
		courseSessionImpl.setCourseId(courseSession.getCourseId());

		return courseSessionImpl;
	}

	/**
	 * Returns the course session with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the course session
	 * @return the course session
	 * @throws com.liferay.portal.NoSuchModelException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseSession findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course session with the primary key or throws a {@link info.diit.portal.NoSuchCourseSessionException} if it could not be found.
	 *
	 * @param sessionId the primary key of the course session
	 * @return the course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findByPrimaryKey(long sessionId)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchByPrimaryKey(sessionId);

		if (courseSession == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + sessionId);
			}

			throw new NoSuchCourseSessionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				sessionId);
		}

		return courseSession;
	}

	/**
	 * Returns the course session with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the course session
	 * @return the course session, or <code>null</code> if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseSession fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course session with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param sessionId the primary key of the course session
	 * @return the course session, or <code>null</code> if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchByPrimaryKey(long sessionId)
		throws SystemException {
		CourseSession courseSession = (CourseSession)EntityCacheUtil.getResult(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
				CourseSessionImpl.class, sessionId);

		if (courseSession == _nullCourseSession) {
			return null;
		}

		if (courseSession == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				courseSession = (CourseSession)session.get(CourseSessionImpl.class,
						Long.valueOf(sessionId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (courseSession != null) {
					cacheResult(courseSession);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CourseSessionModelImpl.ENTITY_CACHE_ENABLED,
						CourseSessionImpl.class, sessionId, _nullCourseSession);
				}

				closeSession(session);
			}
		}

		return courseSession;
	}

	/**
	 * Returns all the course sessions where sessionName = &#63;.
	 *
	 * @param sessionName the session name
	 * @return the matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findBysessionName(String sessionName)
		throws SystemException {
		return findBysessionName(sessionName, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course sessions where sessionName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param sessionName the session name
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @return the range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findBysessionName(String sessionName, int start,
		int end) throws SystemException {
		return findBysessionName(sessionName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course sessions where sessionName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param sessionName the session name
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findBysessionName(String sessionName, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SESSIONNAME;
			finderArgs = new Object[] { sessionName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SESSIONNAME;
			finderArgs = new Object[] { sessionName, start, end, orderByComparator };
		}

		List<CourseSession> list = (List<CourseSession>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSession courseSession : list) {
				if (!Validator.equals(sessionName,
							courseSession.getSessionName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESESSION_WHERE);

			if (sessionName == null) {
				query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_1);
			}
			else {
				if (sessionName.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (sessionName != null) {
					qPos.add(sessionName);
				}

				list = (List<CourseSession>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course session in the ordered set where sessionName = &#63;.
	 *
	 * @param sessionName the session name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findBysessionName_First(String sessionName,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchBysessionName_First(sessionName,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("sessionName=");
		msg.append(sessionName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the first course session in the ordered set where sessionName = &#63;.
	 *
	 * @param sessionName the session name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchBysessionName_First(String sessionName,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSession> list = findBysessionName(sessionName, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course session in the ordered set where sessionName = &#63;.
	 *
	 * @param sessionName the session name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findBysessionName_Last(String sessionName,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchBysessionName_Last(sessionName,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("sessionName=");
		msg.append(sessionName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the last course session in the ordered set where sessionName = &#63;.
	 *
	 * @param sessionName the session name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchBysessionName_Last(String sessionName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBysessionName(sessionName);

		List<CourseSession> list = findBysessionName(sessionName, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course sessions before and after the current course session in the ordered set where sessionName = &#63;.
	 *
	 * @param sessionId the primary key of the current course session
	 * @param sessionName the session name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession[] findBysessionName_PrevAndNext(long sessionId,
		String sessionName, OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = findByPrimaryKey(sessionId);

		Session session = null;

		try {
			session = openSession();

			CourseSession[] array = new CourseSessionImpl[3];

			array[0] = getBysessionName_PrevAndNext(session, courseSession,
					sessionName, orderByComparator, true);

			array[1] = courseSession;

			array[2] = getBysessionName_PrevAndNext(session, courseSession,
					sessionName, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSession getBysessionName_PrevAndNext(Session session,
		CourseSession courseSession, String sessionName,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESESSION_WHERE);

		if (sessionName == null) {
			query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_1);
		}
		else {
			if (sessionName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_3);
			}
			else {
				query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (sessionName != null) {
			qPos.add(sessionName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSession);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSession> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course sessions where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findByCourseId(long courseId)
		throws SystemException {
		return findByCourseId(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the course sessions where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @return the range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findByCourseId(long courseId, int start, int end)
		throws SystemException {
		return findByCourseId(courseId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course sessions where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findByCourseId(long courseId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID;
			finderArgs = new Object[] { courseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSEID;
			finderArgs = new Object[] { courseId, start, end, orderByComparator };
		}

		List<CourseSession> list = (List<CourseSession>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSession courseSession : list) {
				if ((courseId != courseSession.getCourseId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESESSION_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<CourseSession>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course session in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findByCourseId_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchByCourseId_First(courseId,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the first course session in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchByCourseId_First(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSession> list = findByCourseId(courseId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course session in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findByCourseId_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchByCourseId_Last(courseId,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the last course session in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchByCourseId_Last(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCourseId(courseId);

		List<CourseSession> list = findByCourseId(courseId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course sessions before and after the current course session in the ordered set where courseId = &#63;.
	 *
	 * @param sessionId the primary key of the current course session
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession[] findByCourseId_PrevAndNext(long sessionId,
		long courseId, OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = findByPrimaryKey(sessionId);

		Session session = null;

		try {
			session = openSession();

			CourseSession[] array = new CourseSessionImpl[3];

			array[0] = getByCourseId_PrevAndNext(session, courseSession,
					courseId, orderByComparator, true);

			array[1] = courseSession;

			array[2] = getByCourseId_PrevAndNext(session, courseSession,
					courseId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSession getByCourseId_PrevAndNext(Session session,
		CourseSession courseSession, long courseId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESESSION_WHERE);

		query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSession);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSession> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course sessions where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course sessions where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @return the range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findByorganization(long organizationId,
		int start, int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course sessions where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findByorganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<CourseSession> list = (List<CourseSession>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSession courseSession : list) {
				if ((organizationId != courseSession.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESESSION_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<CourseSession>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course session in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchByorganization_First(organizationId,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the first course session in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSession> list = findByorganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course session in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the last course session in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<CourseSession> list = findByorganization(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course sessions before and after the current course session in the ordered set where organizationId = &#63;.
	 *
	 * @param sessionId the primary key of the current course session
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession[] findByorganization_PrevAndNext(long sessionId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = findByPrimaryKey(sessionId);

		Session session = null;

		try {
			session = openSession();

			CourseSession[] array = new CourseSessionImpl[3];

			array[0] = getByorganization_PrevAndNext(session, courseSession,
					organizationId, orderByComparator, true);

			array[1] = courseSession;

			array[2] = getByorganization_PrevAndNext(session, courseSession,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSession getByorganization_PrevAndNext(Session session,
		CourseSession courseSession, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESESSION_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSession);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSession> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course sessions where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findBycompany(long companyId)
		throws SystemException {
		return findBycompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the course sessions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @return the range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findBycompany(long companyId, int start, int end)
		throws SystemException {
		return findBycompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course sessions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findBycompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<CourseSession> list = (List<CourseSession>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSession courseSession : list) {
				if ((companyId != courseSession.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESESSION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<CourseSession>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course session in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findBycompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchBycompany_First(companyId,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the first course session in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchBycompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSession> list = findBycompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course session in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession findBycompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = fetchBycompany_Last(companyId,
				orderByComparator);

		if (courseSession != null) {
			return courseSession;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSessionException(msg.toString());
	}

	/**
	 * Returns the last course session in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course session, or <code>null</code> if a matching course session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession fetchBycompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycompany(companyId);

		List<CourseSession> list = findBycompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course sessions before and after the current course session in the ordered set where companyId = &#63;.
	 *
	 * @param sessionId the primary key of the current course session
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course session
	 * @throws info.diit.portal.NoSuchCourseSessionException if a course session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSession[] findBycompany_PrevAndNext(long sessionId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchCourseSessionException, SystemException {
		CourseSession courseSession = findByPrimaryKey(sessionId);

		Session session = null;

		try {
			session = openSession();

			CourseSession[] array = new CourseSessionImpl[3];

			array[0] = getBycompany_PrevAndNext(session, courseSession,
					companyId, orderByComparator, true);

			array[1] = courseSession;

			array[2] = getBycompany_PrevAndNext(session, courseSession,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSession getBycompany_PrevAndNext(Session session,
		CourseSession courseSession, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESESSION_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSessionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSession);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSession> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course sessions.
	 *
	 * @return the course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course sessions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @return the range of course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the course sessions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course sessions
	 * @param end the upper bound of the range of course sessions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSession> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CourseSession> list = (List<CourseSession>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COURSESESSION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COURSESESSION.concat(CourseSessionModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CourseSession>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CourseSession>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the course sessions where sessionName = &#63; from the database.
	 *
	 * @param sessionName the session name
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBysessionName(String sessionName)
		throws SystemException {
		for (CourseSession courseSession : findBysessionName(sessionName)) {
			remove(courseSession);
		}
	}

	/**
	 * Removes all the course sessions where courseId = &#63; from the database.
	 *
	 * @param courseId the course ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseId(long courseId) throws SystemException {
		for (CourseSession courseSession : findByCourseId(courseId)) {
			remove(courseSession);
		}
	}

	/**
	 * Removes all the course sessions where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (CourseSession courseSession : findByorganization(organizationId)) {
			remove(courseSession);
		}
	}

	/**
	 * Removes all the course sessions where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycompany(long companyId) throws SystemException {
		for (CourseSession courseSession : findBycompany(companyId)) {
			remove(courseSession);
		}
	}

	/**
	 * Removes all the course sessions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CourseSession courseSession : findAll()) {
			remove(courseSession);
		}
	}

	/**
	 * Returns the number of course sessions where sessionName = &#63;.
	 *
	 * @param sessionName the session name
	 * @return the number of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public int countBysessionName(String sessionName) throws SystemException {
		Object[] finderArgs = new Object[] { sessionName };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SESSIONNAME,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESESSION_WHERE);

			if (sessionName == null) {
				query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_1);
			}
			else {
				if (sessionName.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_SESSIONNAME_SESSIONNAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (sessionName != null) {
					qPos.add(sessionName);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SESSIONNAME,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course sessions where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the number of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseId(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESESSION_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course sessions where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESESSION_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course sessions where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESESSION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course sessions.
	 *
	 * @return the number of course sessions
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COURSESESSION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the course session persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.CourseSession")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CourseSession>> listenersList = new ArrayList<ModelListener<CourseSession>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CourseSession>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CourseSessionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COURSESESSION = "SELECT courseSession FROM CourseSession courseSession";
	private static final String _SQL_SELECT_COURSESESSION_WHERE = "SELECT courseSession FROM CourseSession courseSession WHERE ";
	private static final String _SQL_COUNT_COURSESESSION = "SELECT COUNT(courseSession) FROM CourseSession courseSession";
	private static final String _SQL_COUNT_COURSESESSION_WHERE = "SELECT COUNT(courseSession) FROM CourseSession courseSession WHERE ";
	private static final String _FINDER_COLUMN_SESSIONNAME_SESSIONNAME_1 = "courseSession.sessionName IS NULL";
	private static final String _FINDER_COLUMN_SESSIONNAME_SESSIONNAME_2 = "courseSession.sessionName = ?";
	private static final String _FINDER_COLUMN_SESSIONNAME_SESSIONNAME_3 = "(courseSession.sessionName IS NULL OR courseSession.sessionName = ?)";
	private static final String _FINDER_COLUMN_COURSEID_COURSEID_2 = "courseSession.courseId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "courseSession.organizationId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "courseSession.companyId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "courseSession.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CourseSession exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CourseSession exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CourseSessionPersistenceImpl.class);
	private static CourseSession _nullCourseSession = new CourseSessionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CourseSession> toCacheModel() {
				return _nullCourseSessionCacheModel;
			}
		};

	private static CacheModel<CourseSession> _nullCourseSessionCacheModel = new CacheModel<CourseSession>() {
			public CourseSession toEntityModel() {
				return _nullCourseSession;
			}
		};
}
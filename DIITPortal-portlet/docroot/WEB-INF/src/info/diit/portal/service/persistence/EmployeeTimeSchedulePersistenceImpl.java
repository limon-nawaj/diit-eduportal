/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchEmployeeTimeScheduleException;
import info.diit.portal.model.EmployeeTimeSchedule;
import info.diit.portal.model.impl.EmployeeTimeScheduleImpl;
import info.diit.portal.model.impl.EmployeeTimeScheduleModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the employee time schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see EmployeeTimeSchedulePersistence
 * @see EmployeeTimeScheduleUtil
 * @generated
 */
public class EmployeeTimeSchedulePersistenceImpl extends BasePersistenceImpl<EmployeeTimeSchedule>
	implements EmployeeTimeSchedulePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmployeeTimeScheduleUtil} to access the employee time schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmployeeTimeScheduleImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployee",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE =
		new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmployee",
			new String[] { Long.class.getName() },
			EmployeeTimeScheduleModelImpl.EMPLOYEEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEE = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployee",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_EMPLOYEEDAY = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByEmployeeDay",
			new String[] { Long.class.getName(), Integer.class.getName() },
			EmployeeTimeScheduleModelImpl.EMPLOYEEID_COLUMN_BITMASK |
			EmployeeTimeScheduleModelImpl.DAY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEEDAY = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployeeDay",
			new String[] { Long.class.getName(), Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the employee time schedule in the entity cache if it is enabled.
	 *
	 * @param employeeTimeSchedule the employee time schedule
	 */
	public void cacheResult(EmployeeTimeSchedule employeeTimeSchedule) {
		EntityCacheUtil.putResult(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class,
			employeeTimeSchedule.getPrimaryKey(), employeeTimeSchedule);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
			new Object[] {
				Long.valueOf(employeeTimeSchedule.getEmployeeId()),
				Integer.valueOf(employeeTimeSchedule.getDay())
			}, employeeTimeSchedule);

		employeeTimeSchedule.resetOriginalValues();
	}

	/**
	 * Caches the employee time schedules in the entity cache if it is enabled.
	 *
	 * @param employeeTimeSchedules the employee time schedules
	 */
	public void cacheResult(List<EmployeeTimeSchedule> employeeTimeSchedules) {
		for (EmployeeTimeSchedule employeeTimeSchedule : employeeTimeSchedules) {
			if (EntityCacheUtil.getResult(
						EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeTimeScheduleImpl.class,
						employeeTimeSchedule.getPrimaryKey()) == null) {
				cacheResult(employeeTimeSchedule);
			}
			else {
				employeeTimeSchedule.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all employee time schedules.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EmployeeTimeScheduleImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EmployeeTimeScheduleImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the employee time schedule.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmployeeTimeSchedule employeeTimeSchedule) {
		EntityCacheUtil.removeResult(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class, employeeTimeSchedule.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(employeeTimeSchedule);
	}

	@Override
	public void clearCache(List<EmployeeTimeSchedule> employeeTimeSchedules) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmployeeTimeSchedule employeeTimeSchedule : employeeTimeSchedules) {
			EntityCacheUtil.removeResult(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeTimeScheduleImpl.class,
				employeeTimeSchedule.getPrimaryKey());

			clearUniqueFindersCache(employeeTimeSchedule);
		}
	}

	protected void clearUniqueFindersCache(
		EmployeeTimeSchedule employeeTimeSchedule) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
			new Object[] {
				Long.valueOf(employeeTimeSchedule.getEmployeeId()),
				Integer.valueOf(employeeTimeSchedule.getDay())
			});
	}

	/**
	 * Creates a new employee time schedule with the primary key. Does not add the employee time schedule to the database.
	 *
	 * @param employeeTimeScheduleId the primary key for the new employee time schedule
	 * @return the new employee time schedule
	 */
	public EmployeeTimeSchedule create(long employeeTimeScheduleId) {
		EmployeeTimeSchedule employeeTimeSchedule = new EmployeeTimeScheduleImpl();

		employeeTimeSchedule.setNew(true);
		employeeTimeSchedule.setPrimaryKey(employeeTimeScheduleId);

		return employeeTimeSchedule;
	}

	/**
	 * Removes the employee time schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param employeeTimeScheduleId the primary key of the employee time schedule
	 * @return the employee time schedule that was removed
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule remove(long employeeTimeScheduleId)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		return remove(Long.valueOf(employeeTimeScheduleId));
	}

	/**
	 * Removes the employee time schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the employee time schedule
	 * @return the employee time schedule that was removed
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeTimeSchedule remove(Serializable primaryKey)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EmployeeTimeSchedule employeeTimeSchedule = (EmployeeTimeSchedule)session.get(EmployeeTimeScheduleImpl.class,
					primaryKey);

			if (employeeTimeSchedule == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmployeeTimeScheduleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(employeeTimeSchedule);
		}
		catch (NoSuchEmployeeTimeScheduleException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmployeeTimeSchedule removeImpl(
		EmployeeTimeSchedule employeeTimeSchedule) throws SystemException {
		employeeTimeSchedule = toUnwrappedModel(employeeTimeSchedule);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, employeeTimeSchedule);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(employeeTimeSchedule);

		return employeeTimeSchedule;
	}

	@Override
	public EmployeeTimeSchedule updateImpl(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule,
		boolean merge) throws SystemException {
		employeeTimeSchedule = toUnwrappedModel(employeeTimeSchedule);

		boolean isNew = employeeTimeSchedule.isNew();

		EmployeeTimeScheduleModelImpl employeeTimeScheduleModelImpl = (EmployeeTimeScheduleModelImpl)employeeTimeSchedule;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, employeeTimeSchedule, merge);

			employeeTimeSchedule.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EmployeeTimeScheduleModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((employeeTimeScheduleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeTimeScheduleModelImpl.getOriginalEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);

				args = new Object[] {
						Long.valueOf(employeeTimeScheduleModelImpl.getEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);
			}
		}

		EntityCacheUtil.putResult(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeTimeScheduleImpl.class,
			employeeTimeSchedule.getPrimaryKey(), employeeTimeSchedule);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
				new Object[] {
					Long.valueOf(employeeTimeSchedule.getEmployeeId()),
					Integer.valueOf(employeeTimeSchedule.getDay())
				}, employeeTimeSchedule);
		}
		else {
			if ((employeeTimeScheduleModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_EMPLOYEEDAY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeTimeScheduleModelImpl.getOriginalEmployeeId()),
						Integer.valueOf(employeeTimeScheduleModelImpl.getOriginalDay())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEDAY,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
					new Object[] {
						Long.valueOf(employeeTimeSchedule.getEmployeeId()),
						Integer.valueOf(employeeTimeSchedule.getDay())
					}, employeeTimeSchedule);
			}
		}

		return employeeTimeSchedule;
	}

	protected EmployeeTimeSchedule toUnwrappedModel(
		EmployeeTimeSchedule employeeTimeSchedule) {
		if (employeeTimeSchedule instanceof EmployeeTimeScheduleImpl) {
			return employeeTimeSchedule;
		}

		EmployeeTimeScheduleImpl employeeTimeScheduleImpl = new EmployeeTimeScheduleImpl();

		employeeTimeScheduleImpl.setNew(employeeTimeSchedule.isNew());
		employeeTimeScheduleImpl.setPrimaryKey(employeeTimeSchedule.getPrimaryKey());

		employeeTimeScheduleImpl.setEmployeeTimeScheduleId(employeeTimeSchedule.getEmployeeTimeScheduleId());
		employeeTimeScheduleImpl.setOrganizationId(employeeTimeSchedule.getOrganizationId());
		employeeTimeScheduleImpl.setCompanyId(employeeTimeSchedule.getCompanyId());
		employeeTimeScheduleImpl.setEmployeeId(employeeTimeSchedule.getEmployeeId());
		employeeTimeScheduleImpl.setDay(employeeTimeSchedule.getDay());
		employeeTimeScheduleImpl.setStartTime(employeeTimeSchedule.getStartTime());
		employeeTimeScheduleImpl.setEndTime(employeeTimeSchedule.getEndTime());
		employeeTimeScheduleImpl.setDuration(employeeTimeSchedule.getDuration());

		return employeeTimeScheduleImpl;
	}

	/**
	 * Returns the employee time schedule with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee time schedule
	 * @return the employee time schedule
	 * @throws com.liferay.portal.NoSuchModelException if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeTimeSchedule findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee time schedule with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeTimeScheduleException} if it could not be found.
	 *
	 * @param employeeTimeScheduleId the primary key of the employee time schedule
	 * @return the employee time schedule
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule findByPrimaryKey(long employeeTimeScheduleId)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = fetchByPrimaryKey(employeeTimeScheduleId);

		if (employeeTimeSchedule == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					employeeTimeScheduleId);
			}

			throw new NoSuchEmployeeTimeScheduleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				employeeTimeScheduleId);
		}

		return employeeTimeSchedule;
	}

	/**
	 * Returns the employee time schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee time schedule
	 * @return the employee time schedule, or <code>null</code> if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeTimeSchedule fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee time schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param employeeTimeScheduleId the primary key of the employee time schedule
	 * @return the employee time schedule, or <code>null</code> if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule fetchByPrimaryKey(long employeeTimeScheduleId)
		throws SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = (EmployeeTimeSchedule)EntityCacheUtil.getResult(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeTimeScheduleImpl.class, employeeTimeScheduleId);

		if (employeeTimeSchedule == _nullEmployeeTimeSchedule) {
			return null;
		}

		if (employeeTimeSchedule == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				employeeTimeSchedule = (EmployeeTimeSchedule)session.get(EmployeeTimeScheduleImpl.class,
						Long.valueOf(employeeTimeScheduleId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (employeeTimeSchedule != null) {
					cacheResult(employeeTimeSchedule);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(EmployeeTimeScheduleModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeTimeScheduleImpl.class, employeeTimeScheduleId,
						_nullEmployeeTimeSchedule);
				}

				closeSession(session);
			}
		}

		return employeeTimeSchedule;
	}

	/**
	 * Returns all the employee time schedules where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the matching employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeTimeSchedule> findByEmployee(long employeeId)
		throws SystemException {
		return findByEmployee(employeeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the employee time schedules where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of employee time schedules
	 * @param end the upper bound of the range of employee time schedules (not inclusive)
	 * @return the range of matching employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeTimeSchedule> findByEmployee(long employeeId,
		int start, int end) throws SystemException {
		return findByEmployee(employeeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee time schedules where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of employee time schedules
	 * @param end the upper bound of the range of employee time schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeTimeSchedule> findByEmployee(long employeeId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId, start, end, orderByComparator };
		}

		List<EmployeeTimeSchedule> list = (List<EmployeeTimeSchedule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EmployeeTimeSchedule employeeTimeSchedule : list) {
				if ((employeeId != employeeTimeSchedule.getEmployeeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_EMPLOYEETIMESCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				list = (List<EmployeeTimeSchedule>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first employee time schedule in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee time schedule
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule findByEmployee_First(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = fetchByEmployee_First(employeeId,
				orderByComparator);

		if (employeeTimeSchedule != null) {
			return employeeTimeSchedule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeTimeScheduleException(msg.toString());
	}

	/**
	 * Returns the first employee time schedule in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule fetchByEmployee_First(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EmployeeTimeSchedule> list = findByEmployee(employeeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last employee time schedule in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee time schedule
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule findByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = fetchByEmployee_Last(employeeId,
				orderByComparator);

		if (employeeTimeSchedule != null) {
			return employeeTimeSchedule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeTimeScheduleException(msg.toString());
	}

	/**
	 * Returns the last employee time schedule in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule fetchByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByEmployee(employeeId);

		List<EmployeeTimeSchedule> list = findByEmployee(employeeId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the employee time schedules before and after the current employee time schedule in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeTimeScheduleId the primary key of the current employee time schedule
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next employee time schedule
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule[] findByEmployee_PrevAndNext(
		long employeeTimeScheduleId, long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = findByPrimaryKey(employeeTimeScheduleId);

		Session session = null;

		try {
			session = openSession();

			EmployeeTimeSchedule[] array = new EmployeeTimeScheduleImpl[3];

			array[0] = getByEmployee_PrevAndNext(session, employeeTimeSchedule,
					employeeId, orderByComparator, true);

			array[1] = employeeTimeSchedule;

			array[2] = getByEmployee_PrevAndNext(session, employeeTimeSchedule,
					employeeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmployeeTimeSchedule getByEmployee_PrevAndNext(Session session,
		EmployeeTimeSchedule employeeTimeSchedule, long employeeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMPLOYEETIMESCHEDULE_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(employeeTimeSchedule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmployeeTimeSchedule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the employee time schedule where employeeId = &#63; and day = &#63; or throws a {@link info.diit.portal.NoSuchEmployeeTimeScheduleException} if it could not be found.
	 *
	 * @param employeeId the employee ID
	 * @param day the day
	 * @return the matching employee time schedule
	 * @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule findByEmployeeDay(long employeeId, int day)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = fetchByEmployeeDay(employeeId,
				day);

		if (employeeTimeSchedule == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("employeeId=");
			msg.append(employeeId);

			msg.append(", day=");
			msg.append(day);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchEmployeeTimeScheduleException(msg.toString());
		}

		return employeeTimeSchedule;
	}

	/**
	 * Returns the employee time schedule where employeeId = &#63; and day = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param employeeId the employee ID
	 * @param day the day
	 * @return the matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule fetchByEmployeeDay(long employeeId, int day)
		throws SystemException {
		return fetchByEmployeeDay(employeeId, day, true);
	}

	/**
	 * Returns the employee time schedule where employeeId = &#63; and day = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param employeeId the employee ID
	 * @param day the day
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule fetchByEmployeeDay(long employeeId, int day,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId, day };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
					finderArgs, this);
		}

		if (result instanceof EmployeeTimeSchedule) {
			EmployeeTimeSchedule employeeTimeSchedule = (EmployeeTimeSchedule)result;

			if ((employeeId != employeeTimeSchedule.getEmployeeId()) ||
					(day != employeeTimeSchedule.getDay())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_EMPLOYEETIMESCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEDAY_EMPLOYEEID_2);

			query.append(_FINDER_COLUMN_EMPLOYEEDAY_DAY_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				qPos.add(day);

				List<EmployeeTimeSchedule> list = q.list();

				result = list;

				EmployeeTimeSchedule employeeTimeSchedule = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
						finderArgs, list);
				}
				else {
					employeeTimeSchedule = list.get(0);

					cacheResult(employeeTimeSchedule);

					if ((employeeTimeSchedule.getEmployeeId() != employeeId) ||
							(employeeTimeSchedule.getDay() != day)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
							finderArgs, employeeTimeSchedule);
					}
				}

				return employeeTimeSchedule;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEEDAY,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (EmployeeTimeSchedule)result;
			}
		}
	}

	/**
	 * Returns all the employee time schedules.
	 *
	 * @return the employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeTimeSchedule> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee time schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee time schedules
	 * @param end the upper bound of the range of employee time schedules (not inclusive)
	 * @return the range of employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeTimeSchedule> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee time schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee time schedules
	 * @param end the upper bound of the range of employee time schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeTimeSchedule> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmployeeTimeSchedule> list = (List<EmployeeTimeSchedule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EMPLOYEETIMESCHEDULE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMPLOYEETIMESCHEDULE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<EmployeeTimeSchedule>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<EmployeeTimeSchedule>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the employee time schedules where employeeId = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployee(long employeeId) throws SystemException {
		for (EmployeeTimeSchedule employeeTimeSchedule : findByEmployee(
				employeeId)) {
			remove(employeeTimeSchedule);
		}
	}

	/**
	 * Removes the employee time schedule where employeeId = &#63; and day = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @param day the day
	 * @return the employee time schedule that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeTimeSchedule removeByEmployeeDay(long employeeId, int day)
		throws NoSuchEmployeeTimeScheduleException, SystemException {
		EmployeeTimeSchedule employeeTimeSchedule = findByEmployeeDay(employeeId,
				day);

		return remove(employeeTimeSchedule);
	}

	/**
	 * Removes all the employee time schedules from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (EmployeeTimeSchedule employeeTimeSchedule : findAll()) {
			remove(employeeTimeSchedule);
		}
	}

	/**
	 * Returns the number of employee time schedules where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the number of matching employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployee(long employeeId) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EMPLOYEETIMESCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee time schedules where employeeId = &#63; and day = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param day the day
	 * @return the number of matching employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployeeDay(long employeeId, int day)
		throws SystemException {
		Object[] finderArgs = new Object[] { employeeId, day };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEEDAY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EMPLOYEETIMESCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEDAY_EMPLOYEEID_2);

			query.append(_FINDER_COLUMN_EMPLOYEEDAY_DAY_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				qPos.add(day);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEEDAY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee time schedules.
	 *
	 * @return the number of employee time schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMPLOYEETIMESCHEDULE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the employee time schedule persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.EmployeeTimeSchedule")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EmployeeTimeSchedule>> listenersList = new ArrayList<ModelListener<EmployeeTimeSchedule>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EmployeeTimeSchedule>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EmployeeTimeScheduleImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EMPLOYEETIMESCHEDULE = "SELECT employeeTimeSchedule FROM EmployeeTimeSchedule employeeTimeSchedule";
	private static final String _SQL_SELECT_EMPLOYEETIMESCHEDULE_WHERE = "SELECT employeeTimeSchedule FROM EmployeeTimeSchedule employeeTimeSchedule WHERE ";
	private static final String _SQL_COUNT_EMPLOYEETIMESCHEDULE = "SELECT COUNT(employeeTimeSchedule) FROM EmployeeTimeSchedule employeeTimeSchedule";
	private static final String _SQL_COUNT_EMPLOYEETIMESCHEDULE_WHERE = "SELECT COUNT(employeeTimeSchedule) FROM EmployeeTimeSchedule employeeTimeSchedule WHERE ";
	private static final String _FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2 = "employeeTimeSchedule.employeeId = ?";
	private static final String _FINDER_COLUMN_EMPLOYEEDAY_EMPLOYEEID_2 = "employeeTimeSchedule.employeeId = ? AND ";
	private static final String _FINDER_COLUMN_EMPLOYEEDAY_DAY_2 = "employeeTimeSchedule.day = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "employeeTimeSchedule.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmployeeTimeSchedule exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EmployeeTimeSchedule exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EmployeeTimeSchedulePersistenceImpl.class);
	private static EmployeeTimeSchedule _nullEmployeeTimeSchedule = new EmployeeTimeScheduleImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EmployeeTimeSchedule> toCacheModel() {
				return _nullEmployeeTimeScheduleCacheModel;
			}
		};

	private static CacheModel<EmployeeTimeSchedule> _nullEmployeeTimeScheduleCacheModel =
		new CacheModel<EmployeeTimeSchedule>() {
			public EmployeeTimeSchedule toEntityModel() {
				return _nullEmployeeTimeSchedule;
			}
		};
}
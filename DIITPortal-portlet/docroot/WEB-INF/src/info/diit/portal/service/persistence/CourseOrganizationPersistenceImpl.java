/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchCourseOrganizationException;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.impl.CourseOrganizationImpl;
import info.diit.portal.model.impl.CourseOrganizationModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the course organization service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseOrganizationPersistence
 * @see CourseOrganizationUtil
 * @generated
 */
public class CourseOrganizationPersistenceImpl extends BasePersistenceImpl<CourseOrganization>
	implements CourseOrganizationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CourseOrganizationUtil} to access the course organization persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CourseOrganizationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSE = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBycourse",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE =
		new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycourse",
			new String[] { Long.class.getName() },
			CourseOrganizationModelImpl.COURSEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSE = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycourse",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorganization",
			new String[] { Long.class.getName() },
			CourseOrganizationModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBycompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycompany",
			new String[] { Long.class.getName() },
			CourseOrganizationModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED,
			CourseOrganizationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the course organization in the entity cache if it is enabled.
	 *
	 * @param courseOrganization the course organization
	 */
	public void cacheResult(CourseOrganization courseOrganization) {
		EntityCacheUtil.putResult(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationImpl.class, courseOrganization.getPrimaryKey(),
			courseOrganization);

		courseOrganization.resetOriginalValues();
	}

	/**
	 * Caches the course organizations in the entity cache if it is enabled.
	 *
	 * @param courseOrganizations the course organizations
	 */
	public void cacheResult(List<CourseOrganization> courseOrganizations) {
		for (CourseOrganization courseOrganization : courseOrganizations) {
			if (EntityCacheUtil.getResult(
						CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
						CourseOrganizationImpl.class,
						courseOrganization.getPrimaryKey()) == null) {
				cacheResult(courseOrganization);
			}
			else {
				courseOrganization.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all course organizations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CourseOrganizationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CourseOrganizationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the course organization.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CourseOrganization courseOrganization) {
		EntityCacheUtil.removeResult(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationImpl.class, courseOrganization.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CourseOrganization> courseOrganizations) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CourseOrganization courseOrganization : courseOrganizations) {
			EntityCacheUtil.removeResult(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
				CourseOrganizationImpl.class, courseOrganization.getPrimaryKey());
		}
	}

	/**
	 * Creates a new course organization with the primary key. Does not add the course organization to the database.
	 *
	 * @param courseOrganizationId the primary key for the new course organization
	 * @return the new course organization
	 */
	public CourseOrganization create(long courseOrganizationId) {
		CourseOrganization courseOrganization = new CourseOrganizationImpl();

		courseOrganization.setNew(true);
		courseOrganization.setPrimaryKey(courseOrganizationId);

		return courseOrganization;
	}

	/**
	 * Removes the course organization with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courseOrganizationId the primary key of the course organization
	 * @return the course organization that was removed
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization remove(long courseOrganizationId)
		throws NoSuchCourseOrganizationException, SystemException {
		return remove(Long.valueOf(courseOrganizationId));
	}

	/**
	 * Removes the course organization with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the course organization
	 * @return the course organization that was removed
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseOrganization remove(Serializable primaryKey)
		throws NoSuchCourseOrganizationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CourseOrganization courseOrganization = (CourseOrganization)session.get(CourseOrganizationImpl.class,
					primaryKey);

			if (courseOrganization == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCourseOrganizationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(courseOrganization);
		}
		catch (NoSuchCourseOrganizationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CourseOrganization removeImpl(
		CourseOrganization courseOrganization) throws SystemException {
		courseOrganization = toUnwrappedModel(courseOrganization);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, courseOrganization);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(courseOrganization);

		return courseOrganization;
	}

	@Override
	public CourseOrganization updateImpl(
		info.diit.portal.model.CourseOrganization courseOrganization,
		boolean merge) throws SystemException {
		courseOrganization = toUnwrappedModel(courseOrganization);

		boolean isNew = courseOrganization.isNew();

		CourseOrganizationModelImpl courseOrganizationModelImpl = (CourseOrganizationModelImpl)courseOrganization;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, courseOrganization, merge);

			courseOrganization.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CourseOrganizationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((courseOrganizationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseOrganizationModelImpl.getOriginalCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE,
					args);

				args = new Object[] {
						Long.valueOf(courseOrganizationModelImpl.getCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE,
					args);
			}

			if ((courseOrganizationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseOrganizationModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(courseOrganizationModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((courseOrganizationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseOrganizationModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(courseOrganizationModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}
		}

		EntityCacheUtil.putResult(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
			CourseOrganizationImpl.class, courseOrganization.getPrimaryKey(),
			courseOrganization);

		return courseOrganization;
	}

	protected CourseOrganization toUnwrappedModel(
		CourseOrganization courseOrganization) {
		if (courseOrganization instanceof CourseOrganizationImpl) {
			return courseOrganization;
		}

		CourseOrganizationImpl courseOrganizationImpl = new CourseOrganizationImpl();

		courseOrganizationImpl.setNew(courseOrganization.isNew());
		courseOrganizationImpl.setPrimaryKey(courseOrganization.getPrimaryKey());

		courseOrganizationImpl.setCourseOrganizationId(courseOrganization.getCourseOrganizationId());
		courseOrganizationImpl.setCompanyId(courseOrganization.getCompanyId());
		courseOrganizationImpl.setOrganizationId(courseOrganization.getOrganizationId());
		courseOrganizationImpl.setUserId(courseOrganization.getUserId());
		courseOrganizationImpl.setUserName(courseOrganization.getUserName());
		courseOrganizationImpl.setCreateDate(courseOrganization.getCreateDate());
		courseOrganizationImpl.setModifiedDate(courseOrganization.getModifiedDate());
		courseOrganizationImpl.setCourseId(courseOrganization.getCourseId());

		return courseOrganizationImpl;
	}

	/**
	 * Returns the course organization with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the course organization
	 * @return the course organization
	 * @throws com.liferay.portal.NoSuchModelException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseOrganization findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course organization with the primary key or throws a {@link info.diit.portal.NoSuchCourseOrganizationException} if it could not be found.
	 *
	 * @param courseOrganizationId the primary key of the course organization
	 * @return the course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findByPrimaryKey(long courseOrganizationId)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchByPrimaryKey(courseOrganizationId);

		if (courseOrganization == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					courseOrganizationId);
			}

			throw new NoSuchCourseOrganizationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				courseOrganizationId);
		}

		return courseOrganization;
	}

	/**
	 * Returns the course organization with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the course organization
	 * @return the course organization, or <code>null</code> if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseOrganization fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course organization with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courseOrganizationId the primary key of the course organization
	 * @return the course organization, or <code>null</code> if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchByPrimaryKey(long courseOrganizationId)
		throws SystemException {
		CourseOrganization courseOrganization = (CourseOrganization)EntityCacheUtil.getResult(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
				CourseOrganizationImpl.class, courseOrganizationId);

		if (courseOrganization == _nullCourseOrganization) {
			return null;
		}

		if (courseOrganization == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				courseOrganization = (CourseOrganization)session.get(CourseOrganizationImpl.class,
						Long.valueOf(courseOrganizationId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (courseOrganization != null) {
					cacheResult(courseOrganization);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CourseOrganizationModelImpl.ENTITY_CACHE_ENABLED,
						CourseOrganizationImpl.class, courseOrganizationId,
						_nullCourseOrganization);
				}

				closeSession(session);
			}
		}

		return courseOrganization;
	}

	/**
	 * Returns all the course organizations where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findBycourse(long courseId)
		throws SystemException {
		return findBycourse(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course organizations where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @return the range of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findBycourse(long courseId, int start,
		int end) throws SystemException {
		return findBycourse(courseId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course organizations where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findBycourse(long courseId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE;
			finderArgs = new Object[] { courseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSE;
			finderArgs = new Object[] { courseId, start, end, orderByComparator };
		}

		List<CourseOrganization> list = (List<CourseOrganization>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseOrganization courseOrganization : list) {
				if ((courseId != courseOrganization.getCourseId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSEORGANIZATION_WHERE);

			query.append(_FINDER_COLUMN_COURSE_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseOrganizationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<CourseOrganization>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course organization in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findBycourse_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchBycourse_First(courseId,
				orderByComparator);

		if (courseOrganization != null) {
			return courseOrganization;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseOrganizationException(msg.toString());
	}

	/**
	 * Returns the first course organization in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchBycourse_First(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseOrganization> list = findBycourse(courseId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course organization in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findBycourse_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchBycourse_Last(courseId,
				orderByComparator);

		if (courseOrganization != null) {
			return courseOrganization;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseOrganizationException(msg.toString());
	}

	/**
	 * Returns the last course organization in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchBycourse_Last(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycourse(courseId);

		List<CourseOrganization> list = findBycourse(courseId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course organizations before and after the current course organization in the ordered set where courseId = &#63;.
	 *
	 * @param courseOrganizationId the primary key of the current course organization
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization[] findBycourse_PrevAndNext(
		long courseOrganizationId, long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = findByPrimaryKey(courseOrganizationId);

		Session session = null;

		try {
			session = openSession();

			CourseOrganization[] array = new CourseOrganizationImpl[3];

			array[0] = getBycourse_PrevAndNext(session, courseOrganization,
					courseId, orderByComparator, true);

			array[1] = courseOrganization;

			array[2] = getBycourse_PrevAndNext(session, courseOrganization,
					courseId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseOrganization getBycourse_PrevAndNext(Session session,
		CourseOrganization courseOrganization, long courseId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSEORGANIZATION_WHERE);

		query.append(_FINDER_COLUMN_COURSE_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseOrganizationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseOrganization);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseOrganization> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course organizations where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course organizations where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @return the range of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findByorganization(long organizationId,
		int start, int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course organizations where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findByorganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<CourseOrganization> list = (List<CourseOrganization>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseOrganization courseOrganization : list) {
				if ((organizationId != courseOrganization.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSEORGANIZATION_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseOrganizationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<CourseOrganization>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course organization in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchByorganization_First(organizationId,
				orderByComparator);

		if (courseOrganization != null) {
			return courseOrganization;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseOrganizationException(msg.toString());
	}

	/**
	 * Returns the first course organization in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseOrganization> list = findByorganization(organizationId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course organization in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (courseOrganization != null) {
			return courseOrganization;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseOrganizationException(msg.toString());
	}

	/**
	 * Returns the last course organization in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<CourseOrganization> list = findByorganization(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course organizations before and after the current course organization in the ordered set where organizationId = &#63;.
	 *
	 * @param courseOrganizationId the primary key of the current course organization
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization[] findByorganization_PrevAndNext(
		long courseOrganizationId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = findByPrimaryKey(courseOrganizationId);

		Session session = null;

		try {
			session = openSession();

			CourseOrganization[] array = new CourseOrganizationImpl[3];

			array[0] = getByorganization_PrevAndNext(session,
					courseOrganization, organizationId, orderByComparator, true);

			array[1] = courseOrganization;

			array[2] = getByorganization_PrevAndNext(session,
					courseOrganization, organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseOrganization getByorganization_PrevAndNext(
		Session session, CourseOrganization courseOrganization,
		long organizationId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSEORGANIZATION_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseOrganizationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseOrganization);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseOrganization> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course organizations where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findBycompany(long companyId)
		throws SystemException {
		return findBycompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the course organizations where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @return the range of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findBycompany(long companyId, int start,
		int end) throws SystemException {
		return findBycompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course organizations where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findBycompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<CourseOrganization> list = (List<CourseOrganization>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseOrganization courseOrganization : list) {
				if ((companyId != courseOrganization.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSEORGANIZATION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseOrganizationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<CourseOrganization>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course organization in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findBycompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchBycompany_First(companyId,
				orderByComparator);

		if (courseOrganization != null) {
			return courseOrganization;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseOrganizationException(msg.toString());
	}

	/**
	 * Returns the first course organization in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchBycompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseOrganization> list = findBycompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course organization in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization findBycompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = fetchBycompany_Last(companyId,
				orderByComparator);

		if (courseOrganization != null) {
			return courseOrganization;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseOrganizationException(msg.toString());
	}

	/**
	 * Returns the last course organization in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization fetchBycompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycompany(companyId);

		List<CourseOrganization> list = findBycompany(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course organizations before and after the current course organization in the ordered set where companyId = &#63;.
	 *
	 * @param courseOrganizationId the primary key of the current course organization
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course organization
	 * @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseOrganization[] findBycompany_PrevAndNext(
		long courseOrganizationId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseOrganizationException, SystemException {
		CourseOrganization courseOrganization = findByPrimaryKey(courseOrganizationId);

		Session session = null;

		try {
			session = openSession();

			CourseOrganization[] array = new CourseOrganizationImpl[3];

			array[0] = getBycompany_PrevAndNext(session, courseOrganization,
					companyId, orderByComparator, true);

			array[1] = courseOrganization;

			array[2] = getBycompany_PrevAndNext(session, courseOrganization,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseOrganization getBycompany_PrevAndNext(Session session,
		CourseOrganization courseOrganization, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSEORGANIZATION_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseOrganizationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseOrganization);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseOrganization> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course organizations.
	 *
	 * @return the course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course organizations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @return the range of course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the course organizations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course organizations
	 * @param end the upper bound of the range of course organizations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseOrganization> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CourseOrganization> list = (List<CourseOrganization>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COURSEORGANIZATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COURSEORGANIZATION.concat(CourseOrganizationModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CourseOrganization>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CourseOrganization>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the course organizations where courseId = &#63; from the database.
	 *
	 * @param courseId the course ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycourse(long courseId) throws SystemException {
		for (CourseOrganization courseOrganization : findBycourse(courseId)) {
			remove(courseOrganization);
		}
	}

	/**
	 * Removes all the course organizations where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (CourseOrganization courseOrganization : findByorganization(
				organizationId)) {
			remove(courseOrganization);
		}
	}

	/**
	 * Removes all the course organizations where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycompany(long companyId) throws SystemException {
		for (CourseOrganization courseOrganization : findBycompany(companyId)) {
			remove(courseOrganization);
		}
	}

	/**
	 * Removes all the course organizations from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CourseOrganization courseOrganization : findAll()) {
			remove(courseOrganization);
		}
	}

	/**
	 * Returns the number of course organizations where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the number of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycourse(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSEORGANIZATION_WHERE);

			query.append(_FINDER_COLUMN_COURSE_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course organizations where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSEORGANIZATION_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course organizations where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSEORGANIZATION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course organizations.
	 *
	 * @return the number of course organizations
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COURSEORGANIZATION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the course organization persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.CourseOrganization")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CourseOrganization>> listenersList = new ArrayList<ModelListener<CourseOrganization>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CourseOrganization>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CourseOrganizationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COURSEORGANIZATION = "SELECT courseOrganization FROM CourseOrganization courseOrganization";
	private static final String _SQL_SELECT_COURSEORGANIZATION_WHERE = "SELECT courseOrganization FROM CourseOrganization courseOrganization WHERE ";
	private static final String _SQL_COUNT_COURSEORGANIZATION = "SELECT COUNT(courseOrganization) FROM CourseOrganization courseOrganization";
	private static final String _SQL_COUNT_COURSEORGANIZATION_WHERE = "SELECT COUNT(courseOrganization) FROM CourseOrganization courseOrganization WHERE ";
	private static final String _FINDER_COLUMN_COURSE_COURSEID_2 = "courseOrganization.courseId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "courseOrganization.organizationId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "courseOrganization.companyId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "courseOrganization.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CourseOrganization exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CourseOrganization exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CourseOrganizationPersistenceImpl.class);
	private static CourseOrganization _nullCourseOrganization = new CourseOrganizationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CourseOrganization> toCacheModel() {
				return _nullCourseOrganizationCacheModel;
			}
		};

	private static CacheModel<CourseOrganization> _nullCourseOrganizationCacheModel =
		new CacheModel<CourseOrganization>() {
			public CourseOrganization toEntityModel() {
				return _nullCourseOrganization;
			}
		};
}
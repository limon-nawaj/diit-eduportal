/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchBatchException;
import info.diit.portal.model.Batch;
import info.diit.portal.service.base.BatchLocalServiceBaseImpl;
import info.diit.portal.service.persistence.BatchUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the batch local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.BatchLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.BatchLocalServiceBaseImpl
 * @see info.diit.portal.service.BatchLocalServiceUtil
 */
public class BatchLocalServiceImpl extends BatchLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.BatchLocalServiceUtil} to access the batch local service.
	 */
	public Batch findBatchByComanyOrg(long companyId, long organizationId, String batchName) throws SystemException {
		Batch batch = null;
		try {
			batch = BatchUtil.findByBatchByCompanyOrg(organizationId, companyId, batchName);
		} catch (NoSuchBatchException e) {
		}
		return batch;		
	}
	
	public List<Batch> findBatchesByComOrg (long companyId, long orgId) throws SystemException{
		List<Batch> batches = BatchUtil.findByBatchesByComOrg(orgId, companyId);
		return batches;		
	}
	
	public List<Batch> findBatchesByOrgCourseId (long organizationId, long courseId) throws SystemException{
		List<Batch> batches = BatchUtil.findByBatchesByOrgCourse(organizationId, courseId);
		return batches;
	}
	public List<Batch> findAllBatchByCompanyId (long companyId) throws SystemException{
		List<Batch> batchList = (List<Batch>)BatchUtil.findByCompanyId(companyId);
		return batchList;
	}
	public List<Batch> findBatchByCourseId(long courseId) throws SystemException{
		return BatchUtil.findByCourseId(courseId);
	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchTaskDesignationException;
import info.diit.portal.model.TaskDesignation;
import info.diit.portal.model.impl.TaskDesignationImpl;
import info.diit.portal.model.impl.TaskDesignationModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the task designation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see TaskDesignationPersistence
 * @see TaskDesignationUtil
 * @generated
 */
public class TaskDesignationPersistenceImpl extends BasePersistenceImpl<TaskDesignation>
	implements TaskDesignationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TaskDesignationUtil} to access the task designation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TaskDesignationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DESIGNATION =
		new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByDesignation",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESIGNATION =
		new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDesignation",
			new String[] { Long.class.getName() },
			TaskDesignationModelImpl.DESIGNATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DESIGNATION = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDesignation",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TASK = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByTask",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTask",
			new String[] { Long.class.getName() },
			TaskDesignationModelImpl.TASKID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TASK = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTask",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_DESIGNATIONTASK = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByDesignationTask",
			new String[] { Long.class.getName(), Long.class.getName() },
			TaskDesignationModelImpl.DESIGNATIONID_COLUMN_BITMASK |
			TaskDesignationModelImpl.TASKID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DESIGNATIONTASK = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDesignationTask",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			TaskDesignationModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED,
			TaskDesignationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the task designation in the entity cache if it is enabled.
	 *
	 * @param taskDesignation the task designation
	 */
	public void cacheResult(TaskDesignation taskDesignation) {
		EntityCacheUtil.putResult(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationImpl.class, taskDesignation.getPrimaryKey(),
			taskDesignation);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
			new Object[] {
				Long.valueOf(taskDesignation.getDesignationId()),
				Long.valueOf(taskDesignation.getTaskId())
			}, taskDesignation);

		taskDesignation.resetOriginalValues();
	}

	/**
	 * Caches the task designations in the entity cache if it is enabled.
	 *
	 * @param taskDesignations the task designations
	 */
	public void cacheResult(List<TaskDesignation> taskDesignations) {
		for (TaskDesignation taskDesignation : taskDesignations) {
			if (EntityCacheUtil.getResult(
						TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
						TaskDesignationImpl.class,
						taskDesignation.getPrimaryKey()) == null) {
				cacheResult(taskDesignation);
			}
			else {
				taskDesignation.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all task designations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TaskDesignationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TaskDesignationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the task designation.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TaskDesignation taskDesignation) {
		EntityCacheUtil.removeResult(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationImpl.class, taskDesignation.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(taskDesignation);
	}

	@Override
	public void clearCache(List<TaskDesignation> taskDesignations) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TaskDesignation taskDesignation : taskDesignations) {
			EntityCacheUtil.removeResult(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
				TaskDesignationImpl.class, taskDesignation.getPrimaryKey());

			clearUniqueFindersCache(taskDesignation);
		}
	}

	protected void clearUniqueFindersCache(TaskDesignation taskDesignation) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
			new Object[] {
				Long.valueOf(taskDesignation.getDesignationId()),
				Long.valueOf(taskDesignation.getTaskId())
			});
	}

	/**
	 * Creates a new task designation with the primary key. Does not add the task designation to the database.
	 *
	 * @param taskDesignationId the primary key for the new task designation
	 * @return the new task designation
	 */
	public TaskDesignation create(long taskDesignationId) {
		TaskDesignation taskDesignation = new TaskDesignationImpl();

		taskDesignation.setNew(true);
		taskDesignation.setPrimaryKey(taskDesignationId);

		return taskDesignation;
	}

	/**
	 * Removes the task designation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param taskDesignationId the primary key of the task designation
	 * @return the task designation that was removed
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation remove(long taskDesignationId)
		throws NoSuchTaskDesignationException, SystemException {
		return remove(Long.valueOf(taskDesignationId));
	}

	/**
	 * Removes the task designation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the task designation
	 * @return the task designation that was removed
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaskDesignation remove(Serializable primaryKey)
		throws NoSuchTaskDesignationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TaskDesignation taskDesignation = (TaskDesignation)session.get(TaskDesignationImpl.class,
					primaryKey);

			if (taskDesignation == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTaskDesignationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(taskDesignation);
		}
		catch (NoSuchTaskDesignationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TaskDesignation removeImpl(TaskDesignation taskDesignation)
		throws SystemException {
		taskDesignation = toUnwrappedModel(taskDesignation);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, taskDesignation);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(taskDesignation);

		return taskDesignation;
	}

	@Override
	public TaskDesignation updateImpl(
		info.diit.portal.model.TaskDesignation taskDesignation, boolean merge)
		throws SystemException {
		taskDesignation = toUnwrappedModel(taskDesignation);

		boolean isNew = taskDesignation.isNew();

		TaskDesignationModelImpl taskDesignationModelImpl = (TaskDesignationModelImpl)taskDesignation;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, taskDesignation, merge);

			taskDesignation.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TaskDesignationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((taskDesignationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESIGNATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getOriginalDesignationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DESIGNATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESIGNATION,
					args);

				args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getDesignationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DESIGNATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESIGNATION,
					args);
			}

			if ((taskDesignationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getOriginalTaskId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TASK, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK,
					args);

				args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getTaskId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TASK, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK,
					args);
			}

			if ((taskDesignationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}
		}

		EntityCacheUtil.putResult(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
			TaskDesignationImpl.class, taskDesignation.getPrimaryKey(),
			taskDesignation);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
				new Object[] {
					Long.valueOf(taskDesignation.getDesignationId()),
					Long.valueOf(taskDesignation.getTaskId())
				}, taskDesignation);
		}
		else {
			if ((taskDesignationModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DESIGNATIONTASK.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(taskDesignationModelImpl.getOriginalDesignationId()),
						Long.valueOf(taskDesignationModelImpl.getOriginalTaskId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DESIGNATIONTASK,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
					new Object[] {
						Long.valueOf(taskDesignation.getDesignationId()),
						Long.valueOf(taskDesignation.getTaskId())
					}, taskDesignation);
			}
		}

		return taskDesignation;
	}

	protected TaskDesignation toUnwrappedModel(TaskDesignation taskDesignation) {
		if (taskDesignation instanceof TaskDesignationImpl) {
			return taskDesignation;
		}

		TaskDesignationImpl taskDesignationImpl = new TaskDesignationImpl();

		taskDesignationImpl.setNew(taskDesignation.isNew());
		taskDesignationImpl.setPrimaryKey(taskDesignation.getPrimaryKey());

		taskDesignationImpl.setTaskDesignationId(taskDesignation.getTaskDesignationId());
		taskDesignationImpl.setCompanyId(taskDesignation.getCompanyId());
		taskDesignationImpl.setUserId(taskDesignation.getUserId());
		taskDesignationImpl.setUserName(taskDesignation.getUserName());
		taskDesignationImpl.setCreateDate(taskDesignation.getCreateDate());
		taskDesignationImpl.setModifiedDate(taskDesignation.getModifiedDate());
		taskDesignationImpl.setTaskId(taskDesignation.getTaskId());
		taskDesignationImpl.setDesignationId(taskDesignation.getDesignationId());

		return taskDesignationImpl;
	}

	/**
	 * Returns the task designation with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the task designation
	 * @return the task designation
	 * @throws com.liferay.portal.NoSuchModelException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaskDesignation findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the task designation with the primary key or throws a {@link info.diit.portal.NoSuchTaskDesignationException} if it could not be found.
	 *
	 * @param taskDesignationId the primary key of the task designation
	 * @return the task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByPrimaryKey(long taskDesignationId)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByPrimaryKey(taskDesignationId);

		if (taskDesignation == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + taskDesignationId);
			}

			throw new NoSuchTaskDesignationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				taskDesignationId);
		}

		return taskDesignation;
	}

	/**
	 * Returns the task designation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the task designation
	 * @return the task designation, or <code>null</code> if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaskDesignation fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the task designation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param taskDesignationId the primary key of the task designation
	 * @return the task designation, or <code>null</code> if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByPrimaryKey(long taskDesignationId)
		throws SystemException {
		TaskDesignation taskDesignation = (TaskDesignation)EntityCacheUtil.getResult(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
				TaskDesignationImpl.class, taskDesignationId);

		if (taskDesignation == _nullTaskDesignation) {
			return null;
		}

		if (taskDesignation == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				taskDesignation = (TaskDesignation)session.get(TaskDesignationImpl.class,
						Long.valueOf(taskDesignationId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (taskDesignation != null) {
					cacheResult(taskDesignation);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(TaskDesignationModelImpl.ENTITY_CACHE_ENABLED,
						TaskDesignationImpl.class, taskDesignationId,
						_nullTaskDesignation);
				}

				closeSession(session);
			}
		}

		return taskDesignation;
	}

	/**
	 * Returns all the task designations where designationId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @return the matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByDesignation(long designationId)
		throws SystemException {
		return findByDesignation(designationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the task designations where designationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param designationId the designation ID
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @return the range of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByDesignation(long designationId,
		int start, int end) throws SystemException {
		return findByDesignation(designationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the task designations where designationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param designationId the designation ID
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByDesignation(long designationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESIGNATION;
			finderArgs = new Object[] { designationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DESIGNATION;
			finderArgs = new Object[] {
					designationId,
					
					start, end, orderByComparator
				};
		}

		List<TaskDesignation> list = (List<TaskDesignation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TaskDesignation taskDesignation : list) {
				if ((designationId != taskDesignation.getDesignationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(designationId);

				list = (List<TaskDesignation>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first task designation in the ordered set where designationId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByDesignation_First(long designationId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByDesignation_First(designationId,
				orderByComparator);

		if (taskDesignation != null) {
			return taskDesignation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("designationId=");
		msg.append(designationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaskDesignationException(msg.toString());
	}

	/**
	 * Returns the first task designation in the ordered set where designationId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByDesignation_First(long designationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<TaskDesignation> list = findByDesignation(designationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last task designation in the ordered set where designationId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByDesignation_Last(long designationId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByDesignation_Last(designationId,
				orderByComparator);

		if (taskDesignation != null) {
			return taskDesignation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("designationId=");
		msg.append(designationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaskDesignationException(msg.toString());
	}

	/**
	 * Returns the last task designation in the ordered set where designationId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByDesignation_Last(long designationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByDesignation(designationId);

		List<TaskDesignation> list = findByDesignation(designationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the task designations before and after the current task designation in the ordered set where designationId = &#63;.
	 *
	 * @param taskDesignationId the primary key of the current task designation
	 * @param designationId the designation ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation[] findByDesignation_PrevAndNext(
		long taskDesignationId, long designationId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = findByPrimaryKey(taskDesignationId);

		Session session = null;

		try {
			session = openSession();

			TaskDesignation[] array = new TaskDesignationImpl[3];

			array[0] = getByDesignation_PrevAndNext(session, taskDesignation,
					designationId, orderByComparator, true);

			array[1] = taskDesignation;

			array[2] = getByDesignation_PrevAndNext(session, taskDesignation,
					designationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TaskDesignation getByDesignation_PrevAndNext(Session session,
		TaskDesignation taskDesignation, long designationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

		query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(designationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(taskDesignation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TaskDesignation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the task designations where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @return the matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByTask(long taskId)
		throws SystemException {
		return findByTask(taskId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the task designations where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @return the range of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByTask(long taskId, int start, int end)
		throws SystemException {
		return findByTask(taskId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the task designations where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByTask(long taskId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK;
			finderArgs = new Object[] { taskId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TASK;
			finderArgs = new Object[] { taskId, start, end, orderByComparator };
		}

		List<TaskDesignation> list = (List<TaskDesignation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TaskDesignation taskDesignation : list) {
				if ((taskId != taskDesignation.getTaskId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_TASK_TASKID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(taskId);

				list = (List<TaskDesignation>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first task designation in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByTask_First(long taskId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByTask_First(taskId,
				orderByComparator);

		if (taskDesignation != null) {
			return taskDesignation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("taskId=");
		msg.append(taskId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaskDesignationException(msg.toString());
	}

	/**
	 * Returns the first task designation in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByTask_First(long taskId,
		OrderByComparator orderByComparator) throws SystemException {
		List<TaskDesignation> list = findByTask(taskId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last task designation in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByTask_Last(long taskId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByTask_Last(taskId,
				orderByComparator);

		if (taskDesignation != null) {
			return taskDesignation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("taskId=");
		msg.append(taskId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaskDesignationException(msg.toString());
	}

	/**
	 * Returns the last task designation in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByTask_Last(long taskId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTask(taskId);

		List<TaskDesignation> list = findByTask(taskId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the task designations before and after the current task designation in the ordered set where taskId = &#63;.
	 *
	 * @param taskDesignationId the primary key of the current task designation
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation[] findByTask_PrevAndNext(long taskDesignationId,
		long taskId, OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = findByPrimaryKey(taskDesignationId);

		Session session = null;

		try {
			session = openSession();

			TaskDesignation[] array = new TaskDesignationImpl[3];

			array[0] = getByTask_PrevAndNext(session, taskDesignation, taskId,
					orderByComparator, true);

			array[1] = taskDesignation;

			array[2] = getByTask_PrevAndNext(session, taskDesignation, taskId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TaskDesignation getByTask_PrevAndNext(Session session,
		TaskDesignation taskDesignation, long taskId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

		query.append(_FINDER_COLUMN_TASK_TASKID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(taskId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(taskDesignation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TaskDesignation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the task designation where designationId = &#63; and taskId = &#63; or throws a {@link info.diit.portal.NoSuchTaskDesignationException} if it could not be found.
	 *
	 * @param designationId the designation ID
	 * @param taskId the task ID
	 * @return the matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByDesignationTask(long designationId, long taskId)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByDesignationTask(designationId,
				taskId);

		if (taskDesignation == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("designationId=");
			msg.append(designationId);

			msg.append(", taskId=");
			msg.append(taskId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTaskDesignationException(msg.toString());
		}

		return taskDesignation;
	}

	/**
	 * Returns the task designation where designationId = &#63; and taskId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param designationId the designation ID
	 * @param taskId the task ID
	 * @return the matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByDesignationTask(long designationId,
		long taskId) throws SystemException {
		return fetchByDesignationTask(designationId, taskId, true);
	}

	/**
	 * Returns the task designation where designationId = &#63; and taskId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param designationId the designation ID
	 * @param taskId the task ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByDesignationTask(long designationId,
		long taskId, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { designationId, taskId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
					finderArgs, this);
		}

		if (result instanceof TaskDesignation) {
			TaskDesignation taskDesignation = (TaskDesignation)result;

			if ((designationId != taskDesignation.getDesignationId()) ||
					(taskId != taskDesignation.getTaskId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_DESIGNATIONTASK_DESIGNATIONID_2);

			query.append(_FINDER_COLUMN_DESIGNATIONTASK_TASKID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(designationId);

				qPos.add(taskId);

				List<TaskDesignation> list = q.list();

				result = list;

				TaskDesignation taskDesignation = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
						finderArgs, list);
				}
				else {
					taskDesignation = list.get(0);

					cacheResult(taskDesignation);

					if ((taskDesignation.getDesignationId() != designationId) ||
							(taskDesignation.getTaskId() != taskId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
							finderArgs, taskDesignation);
					}
				}

				return taskDesignation;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DESIGNATIONTASK,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (TaskDesignation)result;
			}
		}
	}

	/**
	 * Returns all the task designations where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByCompany(long companyId)
		throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the task designations where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @return the range of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByCompany(long companyId, int start,
		int end) throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the task designations where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findByCompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<TaskDesignation> list = (List<TaskDesignation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TaskDesignation taskDesignation : list) {
				if ((companyId != taskDesignation.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<TaskDesignation>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first task designation in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByCompany_First(companyId,
				orderByComparator);

		if (taskDesignation != null) {
			return taskDesignation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaskDesignationException(msg.toString());
	}

	/**
	 * Returns the first task designation in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<TaskDesignation> list = findByCompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last task designation in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = fetchByCompany_Last(companyId,
				orderByComparator);

		if (taskDesignation != null) {
			return taskDesignation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaskDesignationException(msg.toString());
	}

	/**
	 * Returns the last task designation in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<TaskDesignation> list = findByCompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the task designations before and after the current task designation in the ordered set where companyId = &#63;.
	 *
	 * @param taskDesignationId the primary key of the current task designation
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task designation
	 * @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation[] findByCompany_PrevAndNext(long taskDesignationId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = findByPrimaryKey(taskDesignationId);

		Session session = null;

		try {
			session = openSession();

			TaskDesignation[] array = new TaskDesignationImpl[3];

			array[0] = getByCompany_PrevAndNext(session, taskDesignation,
					companyId, orderByComparator, true);

			array[1] = taskDesignation;

			array[2] = getByCompany_PrevAndNext(session, taskDesignation,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TaskDesignation getByCompany_PrevAndNext(Session session,
		TaskDesignation taskDesignation, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TASKDESIGNATION_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(taskDesignation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TaskDesignation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the task designations.
	 *
	 * @return the task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the task designations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @return the range of task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the task designations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of task designations
	 * @param end the upper bound of the range of task designations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of task designations
	 * @throws SystemException if a system exception occurred
	 */
	public List<TaskDesignation> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TaskDesignation> list = (List<TaskDesignation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TASKDESIGNATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TASKDESIGNATION;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<TaskDesignation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<TaskDesignation>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the task designations where designationId = &#63; from the database.
	 *
	 * @param designationId the designation ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByDesignation(long designationId)
		throws SystemException {
		for (TaskDesignation taskDesignation : findByDesignation(designationId)) {
			remove(taskDesignation);
		}
	}

	/**
	 * Removes all the task designations where taskId = &#63; from the database.
	 *
	 * @param taskId the task ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByTask(long taskId) throws SystemException {
		for (TaskDesignation taskDesignation : findByTask(taskId)) {
			remove(taskDesignation);
		}
	}

	/**
	 * Removes the task designation where designationId = &#63; and taskId = &#63; from the database.
	 *
	 * @param designationId the designation ID
	 * @param taskId the task ID
	 * @return the task designation that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public TaskDesignation removeByDesignationTask(long designationId,
		long taskId) throws NoSuchTaskDesignationException, SystemException {
		TaskDesignation taskDesignation = findByDesignationTask(designationId,
				taskId);

		return remove(taskDesignation);
	}

	/**
	 * Removes all the task designations where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (TaskDesignation taskDesignation : findByCompany(companyId)) {
			remove(taskDesignation);
		}
	}

	/**
	 * Removes all the task designations from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (TaskDesignation taskDesignation : findAll()) {
			remove(taskDesignation);
		}
	}

	/**
	 * Returns the number of task designations where designationId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @return the number of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public int countByDesignation(long designationId) throws SystemException {
		Object[] finderArgs = new Object[] { designationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_DESIGNATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_DESIGNATION_DESIGNATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(designationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DESIGNATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of task designations where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @return the number of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public int countByTask(long taskId) throws SystemException {
		Object[] finderArgs = new Object[] { taskId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TASK,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_TASK_TASKID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(taskId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TASK,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of task designations where designationId = &#63; and taskId = &#63;.
	 *
	 * @param designationId the designation ID
	 * @param taskId the task ID
	 * @return the number of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public int countByDesignationTask(long designationId, long taskId)
		throws SystemException {
		Object[] finderArgs = new Object[] { designationId, taskId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_DESIGNATIONTASK,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_DESIGNATIONTASK_DESIGNATIONID_2);

			query.append(_FINDER_COLUMN_DESIGNATIONTASK_TASKID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(designationId);

				qPos.add(taskId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DESIGNATIONTASK,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of task designations where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching task designations
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TASKDESIGNATION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of task designations.
	 *
	 * @return the number of task designations
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TASKDESIGNATION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the task designation persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.TaskDesignation")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TaskDesignation>> listenersList = new ArrayList<ModelListener<TaskDesignation>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TaskDesignation>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TaskDesignationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_TASKDESIGNATION = "SELECT taskDesignation FROM TaskDesignation taskDesignation";
	private static final String _SQL_SELECT_TASKDESIGNATION_WHERE = "SELECT taskDesignation FROM TaskDesignation taskDesignation WHERE ";
	private static final String _SQL_COUNT_TASKDESIGNATION = "SELECT COUNT(taskDesignation) FROM TaskDesignation taskDesignation";
	private static final String _SQL_COUNT_TASKDESIGNATION_WHERE = "SELECT COUNT(taskDesignation) FROM TaskDesignation taskDesignation WHERE ";
	private static final String _FINDER_COLUMN_DESIGNATION_DESIGNATIONID_2 = "taskDesignation.designationId = ?";
	private static final String _FINDER_COLUMN_TASK_TASKID_2 = "taskDesignation.taskId = ?";
	private static final String _FINDER_COLUMN_DESIGNATIONTASK_DESIGNATIONID_2 = "taskDesignation.designationId = ? AND ";
	private static final String _FINDER_COLUMN_DESIGNATIONTASK_TASKID_2 = "taskDesignation.taskId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "taskDesignation.companyId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "taskDesignation.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TaskDesignation exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TaskDesignation exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TaskDesignationPersistenceImpl.class);
	private static TaskDesignation _nullTaskDesignation = new TaskDesignationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TaskDesignation> toCacheModel() {
				return _nullTaskDesignationCacheModel;
			}
		};

	private static CacheModel<TaskDesignation> _nullTaskDesignationCacheModel = new CacheModel<TaskDesignation>() {
			public TaskDesignation toEntityModel() {
				return _nullTaskDesignation;
			}
		};
}
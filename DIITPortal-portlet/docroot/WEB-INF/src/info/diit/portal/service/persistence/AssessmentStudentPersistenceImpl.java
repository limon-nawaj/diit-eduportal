/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchAssessmentStudentException;
import info.diit.portal.model.AssessmentStudent;
import info.diit.portal.model.impl.AssessmentStudentImpl;
import info.diit.portal.model.impl.AssessmentStudentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the assessment student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AssessmentStudentPersistence
 * @see AssessmentStudentUtil
 * @generated
 */
public class AssessmentStudentPersistenceImpl extends BasePersistenceImpl<AssessmentStudent>
	implements AssessmentStudentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AssessmentStudentUtil} to access the assessment student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AssessmentStudentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			AssessmentStudentModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSESSMENTID =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByassessmentId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTID =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByassessmentId",
			new String[] { Long.class.getName() },
			AssessmentStudentModelImpl.ASSESSMENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ASSESSMENTID = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByassessmentId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSESSMENTSTUDENT =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByassessmentStudent",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTSTUDENT =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByassessmentStudent",
			new String[] { Long.class.getName(), Long.class.getName() },
			AssessmentStudentModelImpl.ASSESSMENTID_COLUMN_BITMASK |
			AssessmentStudentModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ASSESSMENTSTUDENT = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByassessmentStudent",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBycompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycompany",
			new String[] { Long.class.getName() },
			AssessmentStudentModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorganization",
			new String[] { Long.class.getName() },
			AssessmentStudentModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED,
			AssessmentStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the assessment student in the entity cache if it is enabled.
	 *
	 * @param assessmentStudent the assessment student
	 */
	public void cacheResult(AssessmentStudent assessmentStudent) {
		EntityCacheUtil.putResult(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentImpl.class, assessmentStudent.getPrimaryKey(),
			assessmentStudent);

		assessmentStudent.resetOriginalValues();
	}

	/**
	 * Caches the assessment students in the entity cache if it is enabled.
	 *
	 * @param assessmentStudents the assessment students
	 */
	public void cacheResult(List<AssessmentStudent> assessmentStudents) {
		for (AssessmentStudent assessmentStudent : assessmentStudents) {
			if (EntityCacheUtil.getResult(
						AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
						AssessmentStudentImpl.class,
						assessmentStudent.getPrimaryKey()) == null) {
				cacheResult(assessmentStudent);
			}
			else {
				assessmentStudent.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all assessment students.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AssessmentStudentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AssessmentStudentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the assessment student.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AssessmentStudent assessmentStudent) {
		EntityCacheUtil.removeResult(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentImpl.class, assessmentStudent.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AssessmentStudent> assessmentStudents) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AssessmentStudent assessmentStudent : assessmentStudents) {
			EntityCacheUtil.removeResult(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
				AssessmentStudentImpl.class, assessmentStudent.getPrimaryKey());
		}
	}

	/**
	 * Creates a new assessment student with the primary key. Does not add the assessment student to the database.
	 *
	 * @param assessmentStudentId the primary key for the new assessment student
	 * @return the new assessment student
	 */
	public AssessmentStudent create(long assessmentStudentId) {
		AssessmentStudent assessmentStudent = new AssessmentStudentImpl();

		assessmentStudent.setNew(true);
		assessmentStudent.setPrimaryKey(assessmentStudentId);

		return assessmentStudent;
	}

	/**
	 * Removes the assessment student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentStudentId the primary key of the assessment student
	 * @return the assessment student that was removed
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent remove(long assessmentStudentId)
		throws NoSuchAssessmentStudentException, SystemException {
		return remove(Long.valueOf(assessmentStudentId));
	}

	/**
	 * Removes the assessment student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the assessment student
	 * @return the assessment student that was removed
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AssessmentStudent remove(Serializable primaryKey)
		throws NoSuchAssessmentStudentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AssessmentStudent assessmentStudent = (AssessmentStudent)session.get(AssessmentStudentImpl.class,
					primaryKey);

			if (assessmentStudent == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAssessmentStudentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(assessmentStudent);
		}
		catch (NoSuchAssessmentStudentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AssessmentStudent removeImpl(AssessmentStudent assessmentStudent)
		throws SystemException {
		assessmentStudent = toUnwrappedModel(assessmentStudent);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, assessmentStudent);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(assessmentStudent);

		return assessmentStudent;
	}

	@Override
	public AssessmentStudent updateImpl(
		info.diit.portal.model.AssessmentStudent assessmentStudent,
		boolean merge) throws SystemException {
		assessmentStudent = toUnwrappedModel(assessmentStudent);

		boolean isNew = assessmentStudent.isNew();

		AssessmentStudentModelImpl assessmentStudentModelImpl = (AssessmentStudentModelImpl)assessmentStudent;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, assessmentStudent, merge);

			assessmentStudent.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AssessmentStudentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((assessmentStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getOriginalUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((assessmentStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getOriginalAssessmentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSESSMENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTID,
					args);

				args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getAssessmentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSESSMENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTID,
					args);
			}

			if ((assessmentStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTSTUDENT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getOriginalAssessmentId()),
						Long.valueOf(assessmentStudentModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSESSMENTSTUDENT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTSTUDENT,
					args);

				args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getAssessmentId()),
						Long.valueOf(assessmentStudentModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSESSMENTSTUDENT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTSTUDENT,
					args);
			}

			if ((assessmentStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((assessmentStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(assessmentStudentModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}
		}

		EntityCacheUtil.putResult(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentStudentImpl.class, assessmentStudent.getPrimaryKey(),
			assessmentStudent);

		return assessmentStudent;
	}

	protected AssessmentStudent toUnwrappedModel(
		AssessmentStudent assessmentStudent) {
		if (assessmentStudent instanceof AssessmentStudentImpl) {
			return assessmentStudent;
		}

		AssessmentStudentImpl assessmentStudentImpl = new AssessmentStudentImpl();

		assessmentStudentImpl.setNew(assessmentStudent.isNew());
		assessmentStudentImpl.setPrimaryKey(assessmentStudent.getPrimaryKey());

		assessmentStudentImpl.setAssessmentStudentId(assessmentStudent.getAssessmentStudentId());
		assessmentStudentImpl.setCompanyId(assessmentStudent.getCompanyId());
		assessmentStudentImpl.setOrganizationId(assessmentStudent.getOrganizationId());
		assessmentStudentImpl.setUserId(assessmentStudent.getUserId());
		assessmentStudentImpl.setUserName(assessmentStudent.getUserName());
		assessmentStudentImpl.setCreateDate(assessmentStudent.getCreateDate());
		assessmentStudentImpl.setModifiedDate(assessmentStudent.getModifiedDate());
		assessmentStudentImpl.setObtainMark(assessmentStudent.getObtainMark());
		assessmentStudentImpl.setAssessmentId(assessmentStudent.getAssessmentId());
		assessmentStudentImpl.setStudentId(assessmentStudent.getStudentId());
		assessmentStudentImpl.setStatus(assessmentStudent.getStatus());

		return assessmentStudentImpl;
	}

	/**
	 * Returns the assessment student with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the assessment student
	 * @return the assessment student
	 * @throws com.liferay.portal.NoSuchModelException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AssessmentStudent findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the assessment student with the primary key or throws a {@link info.diit.portal.NoSuchAssessmentStudentException} if it could not be found.
	 *
	 * @param assessmentStudentId the primary key of the assessment student
	 * @return the assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByPrimaryKey(long assessmentStudentId)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByPrimaryKey(assessmentStudentId);

		if (assessmentStudent == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					assessmentStudentId);
			}

			throw new NoSuchAssessmentStudentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				assessmentStudentId);
		}

		return assessmentStudent;
	}

	/**
	 * Returns the assessment student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the assessment student
	 * @return the assessment student, or <code>null</code> if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AssessmentStudent fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the assessment student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param assessmentStudentId the primary key of the assessment student
	 * @return the assessment student, or <code>null</code> if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByPrimaryKey(long assessmentStudentId)
		throws SystemException {
		AssessmentStudent assessmentStudent = (AssessmentStudent)EntityCacheUtil.getResult(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
				AssessmentStudentImpl.class, assessmentStudentId);

		if (assessmentStudent == _nullAssessmentStudent) {
			return null;
		}

		if (assessmentStudent == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				assessmentStudent = (AssessmentStudent)session.get(AssessmentStudentImpl.class,
						Long.valueOf(assessmentStudentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (assessmentStudent != null) {
					cacheResult(assessmentStudent);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AssessmentStudentModelImpl.ENTITY_CACHE_ENABLED,
						AssessmentStudentImpl.class, assessmentStudentId,
						_nullAssessmentStudent);
				}

				closeSession(session);
			}
		}

		return assessmentStudent;
	}

	/**
	 * Returns all the assessment students where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByuserId(long userId)
		throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment students where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @return the range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment students where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByuserId(long userId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<AssessmentStudent> list = (List<AssessmentStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentStudent assessmentStudent : list) {
				if ((userId != assessmentStudent.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<AssessmentStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment student in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByuserId_First(userId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the first assessment student in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AssessmentStudent> list = findByuserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment student in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByuserId_Last(userId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the last assessment student in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		List<AssessmentStudent> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment students before and after the current assessment student in the ordered set where userId = &#63;.
	 *
	 * @param assessmentStudentId the primary key of the current assessment student
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent[] findByuserId_PrevAndNext(
		long assessmentStudentId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = findByPrimaryKey(assessmentStudentId);

		Session session = null;

		try {
			session = openSession();

			AssessmentStudent[] array = new AssessmentStudentImpl[3];

			array[0] = getByuserId_PrevAndNext(session, assessmentStudent,
					userId, orderByComparator, true);

			array[1] = assessmentStudent;

			array[2] = getByuserId_PrevAndNext(session, assessmentStudent,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentStudent getByuserId_PrevAndNext(Session session,
		AssessmentStudent assessmentStudent, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment students where assessmentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @return the matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByassessmentId(long assessmentId)
		throws SystemException {
		return findByassessmentId(assessmentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment students where assessmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assessmentId the assessment ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @return the range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByassessmentId(long assessmentId,
		int start, int end) throws SystemException {
		return findByassessmentId(assessmentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment students where assessmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assessmentId the assessment ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByassessmentId(long assessmentId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTID;
			finderArgs = new Object[] { assessmentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSESSMENTID;
			finderArgs = new Object[] {
					assessmentId,
					
					start, end, orderByComparator
				};
		}

		List<AssessmentStudent> list = (List<AssessmentStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentStudent assessmentStudent : list) {
				if ((assessmentId != assessmentStudent.getAssessmentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ASSESSMENTID_ASSESSMENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assessmentId);

				list = (List<AssessmentStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment student in the ordered set where assessmentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByassessmentId_First(long assessmentId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByassessmentId_First(assessmentId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assessmentId=");
		msg.append(assessmentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the first assessment student in the ordered set where assessmentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByassessmentId_First(long assessmentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AssessmentStudent> list = findByassessmentId(assessmentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment student in the ordered set where assessmentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByassessmentId_Last(long assessmentId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByassessmentId_Last(assessmentId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assessmentId=");
		msg.append(assessmentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the last assessment student in the ordered set where assessmentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByassessmentId_Last(long assessmentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByassessmentId(assessmentId);

		List<AssessmentStudent> list = findByassessmentId(assessmentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment students before and after the current assessment student in the ordered set where assessmentId = &#63;.
	 *
	 * @param assessmentStudentId the primary key of the current assessment student
	 * @param assessmentId the assessment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent[] findByassessmentId_PrevAndNext(
		long assessmentStudentId, long assessmentId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = findByPrimaryKey(assessmentStudentId);

		Session session = null;

		try {
			session = openSession();

			AssessmentStudent[] array = new AssessmentStudentImpl[3];

			array[0] = getByassessmentId_PrevAndNext(session,
					assessmentStudent, assessmentId, orderByComparator, true);

			array[1] = assessmentStudent;

			array[2] = getByassessmentId_PrevAndNext(session,
					assessmentStudent, assessmentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentStudent getByassessmentId_PrevAndNext(Session session,
		AssessmentStudent assessmentStudent, long assessmentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_ASSESSMENTID_ASSESSMENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assessmentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment students where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @return the matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByassessmentStudent(long assessmentId,
		long studentId) throws SystemException {
		return findByassessmentStudent(assessmentId, studentId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment students where assessmentId = &#63; and studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @return the range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByassessmentStudent(long assessmentId,
		long studentId, int start, int end) throws SystemException {
		return findByassessmentStudent(assessmentId, studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment students where assessmentId = &#63; and studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByassessmentStudent(long assessmentId,
		long studentId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSESSMENTSTUDENT;
			finderArgs = new Object[] { assessmentId, studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSESSMENTSTUDENT;
			finderArgs = new Object[] {
					assessmentId, studentId,
					
					start, end, orderByComparator
				};
		}

		List<AssessmentStudent> list = (List<AssessmentStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentStudent assessmentStudent : list) {
				if ((assessmentId != assessmentStudent.getAssessmentId()) ||
						(studentId != assessmentStudent.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ASSESSMENTSTUDENT_ASSESSMENTID_2);

			query.append(_FINDER_COLUMN_ASSESSMENTSTUDENT_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assessmentId);

				qPos.add(studentId);

				list = (List<AssessmentStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByassessmentStudent_First(long assessmentId,
		long studentId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByassessmentStudent_First(assessmentId,
				studentId, orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assessmentId=");
		msg.append(assessmentId);

		msg.append(", studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the first assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByassessmentStudent_First(long assessmentId,
		long studentId, OrderByComparator orderByComparator)
		throws SystemException {
		List<AssessmentStudent> list = findByassessmentStudent(assessmentId,
				studentId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByassessmentStudent_Last(long assessmentId,
		long studentId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByassessmentStudent_Last(assessmentId,
				studentId, orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assessmentId=");
		msg.append(assessmentId);

		msg.append(", studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the last assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByassessmentStudent_Last(long assessmentId,
		long studentId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByassessmentStudent(assessmentId, studentId);

		List<AssessmentStudent> list = findByassessmentStudent(assessmentId,
				studentId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment students before and after the current assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentStudentId the primary key of the current assessment student
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent[] findByassessmentStudent_PrevAndNext(
		long assessmentStudentId, long assessmentId, long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = findByPrimaryKey(assessmentStudentId);

		Session session = null;

		try {
			session = openSession();

			AssessmentStudent[] array = new AssessmentStudentImpl[3];

			array[0] = getByassessmentStudent_PrevAndNext(session,
					assessmentStudent, assessmentId, studentId,
					orderByComparator, true);

			array[1] = assessmentStudent;

			array[2] = getByassessmentStudent_PrevAndNext(session,
					assessmentStudent, assessmentId, studentId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentStudent getByassessmentStudent_PrevAndNext(
		Session session, AssessmentStudent assessmentStudent,
		long assessmentId, long studentId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_ASSESSMENTSTUDENT_ASSESSMENTID_2);

		query.append(_FINDER_COLUMN_ASSESSMENTSTUDENT_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assessmentId);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment students where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findBycompany(long companyId)
		throws SystemException {
		return findBycompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the assessment students where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @return the range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findBycompany(long companyId, int start,
		int end) throws SystemException {
		return findBycompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment students where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findBycompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<AssessmentStudent> list = (List<AssessmentStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentStudent assessmentStudent : list) {
				if ((companyId != assessmentStudent.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<AssessmentStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment student in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findBycompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchBycompany_First(companyId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the first assessment student in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchBycompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AssessmentStudent> list = findBycompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment student in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findBycompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchBycompany_Last(companyId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the last assessment student in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchBycompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycompany(companyId);

		List<AssessmentStudent> list = findBycompany(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment students before and after the current assessment student in the ordered set where companyId = &#63;.
	 *
	 * @param assessmentStudentId the primary key of the current assessment student
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent[] findBycompany_PrevAndNext(
		long assessmentStudentId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = findByPrimaryKey(assessmentStudentId);

		Session session = null;

		try {
			session = openSession();

			AssessmentStudent[] array = new AssessmentStudentImpl[3];

			array[0] = getBycompany_PrevAndNext(session, assessmentStudent,
					companyId, orderByComparator, true);

			array[1] = assessmentStudent;

			array[2] = getBycompany_PrevAndNext(session, assessmentStudent,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentStudent getBycompany_PrevAndNext(Session session,
		AssessmentStudent assessmentStudent, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment students where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment students where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @return the range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByorganization(long organizationId,
		int start, int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment students where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findByorganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<AssessmentStudent> list = (List<AssessmentStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentStudent assessmentStudent : list) {
				if ((organizationId != assessmentStudent.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<AssessmentStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment student in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByorganization_First(organizationId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the first assessment student in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AssessmentStudent> list = findByorganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment student in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (assessmentStudent != null) {
			return assessmentStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentStudentException(msg.toString());
	}

	/**
	 * Returns the last assessment student in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<AssessmentStudent> list = findByorganization(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment students before and after the current assessment student in the ordered set where organizationId = &#63;.
	 *
	 * @param assessmentStudentId the primary key of the current assessment student
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment student
	 * @throws info.diit.portal.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentStudent[] findByorganization_PrevAndNext(
		long assessmentStudentId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentStudentException, SystemException {
		AssessmentStudent assessmentStudent = findByPrimaryKey(assessmentStudentId);

		Session session = null;

		try {
			session = openSession();

			AssessmentStudent[] array = new AssessmentStudentImpl[3];

			array[0] = getByorganization_PrevAndNext(session,
					assessmentStudent, organizationId, orderByComparator, true);

			array[1] = assessmentStudent;

			array[2] = getByorganization_PrevAndNext(session,
					assessmentStudent, organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentStudent getByorganization_PrevAndNext(Session session,
		AssessmentStudent assessmentStudent, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentStudentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment students.
	 *
	 * @return the assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @return the range of assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessment students
	 * @param end the upper bound of the range of assessment students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentStudent> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AssessmentStudent> list = (List<AssessmentStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ASSESSMENTSTUDENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ASSESSMENTSTUDENT.concat(AssessmentStudentModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AssessmentStudent>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AssessmentStudent>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the assessment students where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByuserId(long userId) throws SystemException {
		for (AssessmentStudent assessmentStudent : findByuserId(userId)) {
			remove(assessmentStudent);
		}
	}

	/**
	 * Removes all the assessment students where assessmentId = &#63; from the database.
	 *
	 * @param assessmentId the assessment ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByassessmentId(long assessmentId)
		throws SystemException {
		for (AssessmentStudent assessmentStudent : findByassessmentId(
				assessmentId)) {
			remove(assessmentStudent);
		}
	}

	/**
	 * Removes all the assessment students where assessmentId = &#63; and studentId = &#63; from the database.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByassessmentStudent(long assessmentId, long studentId)
		throws SystemException {
		for (AssessmentStudent assessmentStudent : findByassessmentStudent(
				assessmentId, studentId)) {
			remove(assessmentStudent);
		}
	}

	/**
	 * Removes all the assessment students where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycompany(long companyId) throws SystemException {
		for (AssessmentStudent assessmentStudent : findBycompany(companyId)) {
			remove(assessmentStudent);
		}
	}

	/**
	 * Removes all the assessment students where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (AssessmentStudent assessmentStudent : findByorganization(
				organizationId)) {
			remove(assessmentStudent);
		}
	}

	/**
	 * Removes all the assessment students from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AssessmentStudent assessmentStudent : findAll()) {
			remove(assessmentStudent);
		}
	}

	/**
	 * Returns the number of assessment students where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByuserId(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment students where assessmentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @return the number of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByassessmentId(long assessmentId) throws SystemException {
		Object[] finderArgs = new Object[] { assessmentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ASSESSMENTID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ASSESSMENTID_ASSESSMENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assessmentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ASSESSMENTID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment students where assessmentId = &#63; and studentId = &#63;.
	 *
	 * @param assessmentId the assessment ID
	 * @param studentId the student ID
	 * @return the number of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByassessmentStudent(long assessmentId, long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { assessmentId, studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ASSESSMENTSTUDENT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ASSESSMENTSTUDENT_ASSESSMENTID_2);

			query.append(_FINDER_COLUMN_ASSESSMENTSTUDENT_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assessmentId);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ASSESSMENTSTUDENT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment students where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment students where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENTSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment students.
	 *
	 * @return the number of assessment students
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ASSESSMENTSTUDENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the assessment student persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.AssessmentStudent")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AssessmentStudent>> listenersList = new ArrayList<ModelListener<AssessmentStudent>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AssessmentStudent>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AssessmentStudentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ASSESSMENTSTUDENT = "SELECT assessmentStudent FROM AssessmentStudent assessmentStudent";
	private static final String _SQL_SELECT_ASSESSMENTSTUDENT_WHERE = "SELECT assessmentStudent FROM AssessmentStudent assessmentStudent WHERE ";
	private static final String _SQL_COUNT_ASSESSMENTSTUDENT = "SELECT COUNT(assessmentStudent) FROM AssessmentStudent assessmentStudent";
	private static final String _SQL_COUNT_ASSESSMENTSTUDENT_WHERE = "SELECT COUNT(assessmentStudent) FROM AssessmentStudent assessmentStudent WHERE ";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "assessmentStudent.userId = ?";
	private static final String _FINDER_COLUMN_ASSESSMENTID_ASSESSMENTID_2 = "assessmentStudent.assessmentId = ?";
	private static final String _FINDER_COLUMN_ASSESSMENTSTUDENT_ASSESSMENTID_2 = "assessmentStudent.assessmentId = ? AND ";
	private static final String _FINDER_COLUMN_ASSESSMENTSTUDENT_STUDENTID_2 = "assessmentStudent.studentId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "assessmentStudent.companyId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "assessmentStudent.organizationId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "assessmentStudent.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AssessmentStudent exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AssessmentStudent exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AssessmentStudentPersistenceImpl.class);
	private static AssessmentStudent _nullAssessmentStudent = new AssessmentStudentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AssessmentStudent> toCacheModel() {
				return _nullAssessmentStudentCacheModel;
			}
		};

	private static CacheModel<AssessmentStudent> _nullAssessmentStudentCacheModel =
		new CacheModel<AssessmentStudent>() {
			public AssessmentStudent toEntityModel() {
				return _nullAssessmentStudent;
			}
		};
}
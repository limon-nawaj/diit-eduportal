/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.model.PersonEmail;
import info.diit.portal.service.base.PersonEmailLocalServiceBaseImpl;
import info.diit.portal.service.persistence.PersonEmailUtil;

import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the person email local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.PersonEmailLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.PersonEmailLocalServiceBaseImpl
 * @see info.diit.portal.service.PersonEmailLocalServiceUtil
 */
public class PersonEmailLocalServiceImpl extends PersonEmailLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.PersonEmailLocalServiceUtil} to access the person email local service.
	 */
	public List<PersonEmail> findByPersonEmailList(long studentId) throws SystemException{
		return PersonEmailUtil.findByStudentPersonEmailList(studentId);
	}
	public PersonEmail getPersonEmailCheck(String email) throws SystemException{
		Criterion criterion = RestrictionsFactoryUtil.eq("personEmail", email);
		DynamicQuery query = DynamicQueryFactoryUtil.forClass(PersonEmail.class).add(criterion);
		List<PersonEmail> personEmails = PersonEmailUtil.findWithDynamicQuery(query);
		if(personEmails.size()>0){
			PersonEmail personEmail=personEmails.get(0);
			return personEmail;
		}
		
		return null;
	}
}
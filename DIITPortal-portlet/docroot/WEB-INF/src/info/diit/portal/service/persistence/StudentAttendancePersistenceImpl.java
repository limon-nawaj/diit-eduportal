/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchStudentAttendanceException;
import info.diit.portal.model.StudentAttendance;
import info.diit.portal.model.impl.StudentAttendanceImpl;
import info.diit.portal.model.impl.StudentAttendanceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the student attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StudentAttendancePersistence
 * @see StudentAttendanceUtil
 * @generated
 */
public class StudentAttendancePersistenceImpl extends BasePersistenceImpl<StudentAttendance>
	implements StudentAttendancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StudentAttendanceUtil} to access the student attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StudentAttendanceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENT = new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED,
			StudentAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStudent",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENT =
		new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED,
			StudentAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStudent",
			new String[] { Long.class.getName() },
			StudentAttendanceModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENT = new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStudent",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ATTENDANCE =
		new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED,
			StudentAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAttendance",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE =
		new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED,
			StudentAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAttendance",
			new String[] { Long.class.getName() },
			StudentAttendanceModelImpl.ATTENDANCEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ATTENDANCE = new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAttendance",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED,
			StudentAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED,
			StudentAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the student attendance in the entity cache if it is enabled.
	 *
	 * @param studentAttendance the student attendance
	 */
	public void cacheResult(StudentAttendance studentAttendance) {
		EntityCacheUtil.putResult(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceImpl.class, studentAttendance.getPrimaryKey(),
			studentAttendance);

		studentAttendance.resetOriginalValues();
	}

	/**
	 * Caches the student attendances in the entity cache if it is enabled.
	 *
	 * @param studentAttendances the student attendances
	 */
	public void cacheResult(List<StudentAttendance> studentAttendances) {
		for (StudentAttendance studentAttendance : studentAttendances) {
			if (EntityCacheUtil.getResult(
						StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
						StudentAttendanceImpl.class,
						studentAttendance.getPrimaryKey()) == null) {
				cacheResult(studentAttendance);
			}
			else {
				studentAttendance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all student attendances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StudentAttendanceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StudentAttendanceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student attendance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(StudentAttendance studentAttendance) {
		EntityCacheUtil.removeResult(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceImpl.class, studentAttendance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<StudentAttendance> studentAttendances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (StudentAttendance studentAttendance : studentAttendances) {
			EntityCacheUtil.removeResult(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
				StudentAttendanceImpl.class, studentAttendance.getPrimaryKey());
		}
	}

	/**
	 * Creates a new student attendance with the primary key. Does not add the student attendance to the database.
	 *
	 * @param attendanceTopicId the primary key for the new student attendance
	 * @return the new student attendance
	 */
	public StudentAttendance create(long attendanceTopicId) {
		StudentAttendance studentAttendance = new StudentAttendanceImpl();

		studentAttendance.setNew(true);
		studentAttendance.setPrimaryKey(attendanceTopicId);

		return studentAttendance;
	}

	/**
	 * Removes the student attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendanceTopicId the primary key of the student attendance
	 * @return the student attendance that was removed
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance remove(long attendanceTopicId)
		throws NoSuchStudentAttendanceException, SystemException {
		return remove(Long.valueOf(attendanceTopicId));
	}

	/**
	 * Removes the student attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student attendance
	 * @return the student attendance that was removed
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentAttendance remove(Serializable primaryKey)
		throws NoSuchStudentAttendanceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			StudentAttendance studentAttendance = (StudentAttendance)session.get(StudentAttendanceImpl.class,
					primaryKey);

			if (studentAttendance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(studentAttendance);
		}
		catch (NoSuchStudentAttendanceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected StudentAttendance removeImpl(StudentAttendance studentAttendance)
		throws SystemException {
		studentAttendance = toUnwrappedModel(studentAttendance);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, studentAttendance);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(studentAttendance);

		return studentAttendance;
	}

	@Override
	public StudentAttendance updateImpl(
		info.diit.portal.model.StudentAttendance studentAttendance,
		boolean merge) throws SystemException {
		studentAttendance = toUnwrappedModel(studentAttendance);

		boolean isNew = studentAttendance.isNew();

		StudentAttendanceModelImpl studentAttendanceModelImpl = (StudentAttendanceModelImpl)studentAttendance;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, studentAttendance, merge);

			studentAttendance.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !StudentAttendanceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((studentAttendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentAttendanceModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENT,
					args);

				args = new Object[] {
						Long.valueOf(studentAttendanceModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENT,
					args);
			}

			if ((studentAttendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentAttendanceModelImpl.getOriginalAttendanceId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE,
					args);

				args = new Object[] {
						Long.valueOf(studentAttendanceModelImpl.getAttendanceId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE,
					args);
			}
		}

		EntityCacheUtil.putResult(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			StudentAttendanceImpl.class, studentAttendance.getPrimaryKey(),
			studentAttendance);

		return studentAttendance;
	}

	protected StudentAttendance toUnwrappedModel(
		StudentAttendance studentAttendance) {
		if (studentAttendance instanceof StudentAttendanceImpl) {
			return studentAttendance;
		}

		StudentAttendanceImpl studentAttendanceImpl = new StudentAttendanceImpl();

		studentAttendanceImpl.setNew(studentAttendance.isNew());
		studentAttendanceImpl.setPrimaryKey(studentAttendance.getPrimaryKey());

		studentAttendanceImpl.setAttendanceTopicId(studentAttendance.getAttendanceTopicId());
		studentAttendanceImpl.setCompanyId(studentAttendance.getCompanyId());
		studentAttendanceImpl.setOrganizationId(studentAttendance.getOrganizationId());
		studentAttendanceImpl.setUserId(studentAttendance.getUserId());
		studentAttendanceImpl.setUserName(studentAttendance.getUserName());
		studentAttendanceImpl.setCreateDate(studentAttendance.getCreateDate());
		studentAttendanceImpl.setModifiedDate(studentAttendance.getModifiedDate());
		studentAttendanceImpl.setAttendanceId(studentAttendance.getAttendanceId());
		studentAttendanceImpl.setStudentId(studentAttendance.getStudentId());
		studentAttendanceImpl.setStatus(studentAttendance.getStatus());
		studentAttendanceImpl.setAttendanceDate(studentAttendance.getAttendanceDate());

		return studentAttendanceImpl;
	}

	/**
	 * Returns the student attendance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the student attendance
	 * @return the student attendance
	 * @throws com.liferay.portal.NoSuchModelException if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentAttendance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student attendance with the primary key or throws a {@link info.diit.portal.NoSuchStudentAttendanceException} if it could not be found.
	 *
	 * @param attendanceTopicId the primary key of the student attendance
	 * @return the student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance findByPrimaryKey(long attendanceTopicId)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = fetchByPrimaryKey(attendanceTopicId);

		if (studentAttendance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + attendanceTopicId);
			}

			throw new NoSuchStudentAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				attendanceTopicId);
		}

		return studentAttendance;
	}

	/**
	 * Returns the student attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student attendance
	 * @return the student attendance, or <code>null</code> if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentAttendance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param attendanceTopicId the primary key of the student attendance
	 * @return the student attendance, or <code>null</code> if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance fetchByPrimaryKey(long attendanceTopicId)
		throws SystemException {
		StudentAttendance studentAttendance = (StudentAttendance)EntityCacheUtil.getResult(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
				StudentAttendanceImpl.class, attendanceTopicId);

		if (studentAttendance == _nullStudentAttendance) {
			return null;
		}

		if (studentAttendance == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				studentAttendance = (StudentAttendance)session.get(StudentAttendanceImpl.class,
						Long.valueOf(attendanceTopicId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (studentAttendance != null) {
					cacheResult(studentAttendance);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StudentAttendanceModelImpl.ENTITY_CACHE_ENABLED,
						StudentAttendanceImpl.class, attendanceTopicId,
						_nullStudentAttendance);
				}

				closeSession(session);
			}
		}

		return studentAttendance;
	}

	/**
	 * Returns all the student attendances where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findByStudent(long studentId)
		throws SystemException {
		return findByStudent(studentId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the student attendances where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of student attendances
	 * @param end the upper bound of the range of student attendances (not inclusive)
	 * @return the range of matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findByStudent(long studentId, int start,
		int end) throws SystemException {
		return findByStudent(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the student attendances where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of student attendances
	 * @param end the upper bound of the range of student attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findByStudent(long studentId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENT;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENT;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<StudentAttendance> list = (List<StudentAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (StudentAttendance studentAttendance : list) {
				if ((studentId != studentAttendance.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_STUDENT_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<StudentAttendance>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student attendance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance findByStudent_First(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = fetchByStudent_First(studentId,
				orderByComparator);

		if (studentAttendance != null) {
			return studentAttendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentAttendanceException(msg.toString());
	}

	/**
	 * Returns the first student attendance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student attendance, or <code>null</code> if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance fetchByStudent_First(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<StudentAttendance> list = findByStudent(studentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student attendance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance findByStudent_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = fetchByStudent_Last(studentId,
				orderByComparator);

		if (studentAttendance != null) {
			return studentAttendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentAttendanceException(msg.toString());
	}

	/**
	 * Returns the last student attendance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student attendance, or <code>null</code> if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance fetchByStudent_Last(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudent(studentId);

		List<StudentAttendance> list = findByStudent(studentId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the student attendances before and after the current student attendance in the ordered set where studentId = &#63;.
	 *
	 * @param attendanceTopicId the primary key of the current student attendance
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance[] findByStudent_PrevAndNext(
		long attendanceTopicId, long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = findByPrimaryKey(attendanceTopicId);

		Session session = null;

		try {
			session = openSession();

			StudentAttendance[] array = new StudentAttendanceImpl[3];

			array[0] = getByStudent_PrevAndNext(session, studentAttendance,
					studentId, orderByComparator, true);

			array[1] = studentAttendance;

			array[2] = getByStudent_PrevAndNext(session, studentAttendance,
					studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected StudentAttendance getByStudent_PrevAndNext(Session session,
		StudentAttendance studentAttendance, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_STUDENT_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(studentAttendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<StudentAttendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the student attendances where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @return the matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findByAttendance(long attendanceId)
		throws SystemException {
		return findByAttendance(attendanceId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student attendances where attendanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param attendanceId the attendance ID
	 * @param start the lower bound of the range of student attendances
	 * @param end the upper bound of the range of student attendances (not inclusive)
	 * @return the range of matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findByAttendance(long attendanceId,
		int start, int end) throws SystemException {
		return findByAttendance(attendanceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the student attendances where attendanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param attendanceId the attendance ID
	 * @param start the lower bound of the range of student attendances
	 * @param end the upper bound of the range of student attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findByAttendance(long attendanceId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE;
			finderArgs = new Object[] { attendanceId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ATTENDANCE;
			finderArgs = new Object[] {
					attendanceId,
					
					start, end, orderByComparator
				};
		}

		List<StudentAttendance> list = (List<StudentAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (StudentAttendance studentAttendance : list) {
				if ((attendanceId != studentAttendance.getAttendanceId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(attendanceId);

				list = (List<StudentAttendance>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student attendance in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance findByAttendance_First(long attendanceId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = fetchByAttendance_First(attendanceId,
				orderByComparator);

		if (studentAttendance != null) {
			return studentAttendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("attendanceId=");
		msg.append(attendanceId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentAttendanceException(msg.toString());
	}

	/**
	 * Returns the first student attendance in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student attendance, or <code>null</code> if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance fetchByAttendance_First(long attendanceId,
		OrderByComparator orderByComparator) throws SystemException {
		List<StudentAttendance> list = findByAttendance(attendanceId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student attendance in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance findByAttendance_Last(long attendanceId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = fetchByAttendance_Last(attendanceId,
				orderByComparator);

		if (studentAttendance != null) {
			return studentAttendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("attendanceId=");
		msg.append(attendanceId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentAttendanceException(msg.toString());
	}

	/**
	 * Returns the last student attendance in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student attendance, or <code>null</code> if a matching student attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance fetchByAttendance_Last(long attendanceId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByAttendance(attendanceId);

		List<StudentAttendance> list = findByAttendance(attendanceId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the student attendances before and after the current student attendance in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceTopicId the primary key of the current student attendance
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student attendance
	 * @throws info.diit.portal.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentAttendance[] findByAttendance_PrevAndNext(
		long attendanceTopicId, long attendanceId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentAttendanceException, SystemException {
		StudentAttendance studentAttendance = findByPrimaryKey(attendanceTopicId);

		Session session = null;

		try {
			session = openSession();

			StudentAttendance[] array = new StudentAttendanceImpl[3];

			array[0] = getByAttendance_PrevAndNext(session, studentAttendance,
					attendanceId, orderByComparator, true);

			array[1] = studentAttendance;

			array[2] = getByAttendance_PrevAndNext(session, studentAttendance,
					attendanceId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected StudentAttendance getByAttendance_PrevAndNext(Session session,
		StudentAttendance studentAttendance, long attendanceId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(attendanceId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(studentAttendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<StudentAttendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the student attendances.
	 *
	 * @return the student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student attendances
	 * @param end the upper bound of the range of student attendances (not inclusive)
	 * @return the range of student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the student attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student attendances
	 * @param end the upper bound of the range of student attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentAttendance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<StudentAttendance> list = (List<StudentAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENTATTENDANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENTATTENDANCE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<StudentAttendance>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<StudentAttendance>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the student attendances where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudent(long studentId) throws SystemException {
		for (StudentAttendance studentAttendance : findByStudent(studentId)) {
			remove(studentAttendance);
		}
	}

	/**
	 * Removes all the student attendances where attendanceId = &#63; from the database.
	 *
	 * @param attendanceId the attendance ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByAttendance(long attendanceId) throws SystemException {
		for (StudentAttendance studentAttendance : findByAttendance(
				attendanceId)) {
			remove(studentAttendance);
		}
	}

	/**
	 * Removes all the student attendances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (StudentAttendance studentAttendance : findAll()) {
			remove(studentAttendance);
		}
	}

	/**
	 * Returns the number of student attendances where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudent(long studentId) throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_STUDENT_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student attendances where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @return the number of matching student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByAttendance(long attendanceId) throws SystemException {
		Object[] finderArgs = new Object[] { attendanceId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(attendanceId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student attendances.
	 *
	 * @return the number of student attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENTATTENDANCE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the student attendance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.StudentAttendance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<StudentAttendance>> listenersList = new ArrayList<ModelListener<StudentAttendance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<StudentAttendance>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StudentAttendanceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STUDENTATTENDANCE = "SELECT studentAttendance FROM StudentAttendance studentAttendance";
	private static final String _SQL_SELECT_STUDENTATTENDANCE_WHERE = "SELECT studentAttendance FROM StudentAttendance studentAttendance WHERE ";
	private static final String _SQL_COUNT_STUDENTATTENDANCE = "SELECT COUNT(studentAttendance) FROM StudentAttendance studentAttendance";
	private static final String _SQL_COUNT_STUDENTATTENDANCE_WHERE = "SELECT COUNT(studentAttendance) FROM StudentAttendance studentAttendance WHERE ";
	private static final String _FINDER_COLUMN_STUDENT_STUDENTID_2 = "studentAttendance.studentId = ?";
	private static final String _FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2 = "studentAttendance.attendanceId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "studentAttendance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No StudentAttendance exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No StudentAttendance exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StudentAttendancePersistenceImpl.class);
	private static StudentAttendance _nullStudentAttendance = new StudentAttendanceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<StudentAttendance> toCacheModel() {
				return _nullStudentAttendanceCacheModel;
			}
		};

	private static CacheModel<StudentAttendance> _nullStudentAttendanceCacheModel =
		new CacheModel<StudentAttendance>() {
			public StudentAttendance toEntityModel() {
				return _nullStudentAttendance;
			}
		};
}
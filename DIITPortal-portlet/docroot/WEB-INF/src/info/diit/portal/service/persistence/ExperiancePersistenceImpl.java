/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchExperianceException;
import info.diit.portal.model.Experiance;
import info.diit.portal.model.impl.ExperianceImpl;
import info.diit.portal.model.impl.ExperianceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the experiance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ExperiancePersistence
 * @see ExperianceUtil
 * @generated
 */
public class ExperiancePersistenceImpl extends BasePersistenceImpl<Experiance>
	implements ExperiancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ExperianceUtil} to access the experiance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ExperianceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST =
		new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, ExperianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentExperianceList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST =
		new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, ExperianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentExperianceList",
			new String[] { Long.class.getName() },
			ExperianceModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTEXPERIANCELIST = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentExperianceList",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, ExperianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, ExperianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the experiance in the entity cache if it is enabled.
	 *
	 * @param experiance the experiance
	 */
	public void cacheResult(Experiance experiance) {
		EntityCacheUtil.putResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceImpl.class, experiance.getPrimaryKey(), experiance);

		experiance.resetOriginalValues();
	}

	/**
	 * Caches the experiances in the entity cache if it is enabled.
	 *
	 * @param experiances the experiances
	 */
	public void cacheResult(List<Experiance> experiances) {
		for (Experiance experiance : experiances) {
			if (EntityCacheUtil.getResult(
						ExperianceModelImpl.ENTITY_CACHE_ENABLED,
						ExperianceImpl.class, experiance.getPrimaryKey()) == null) {
				cacheResult(experiance);
			}
			else {
				experiance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all experiances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ExperianceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ExperianceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the experiance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Experiance experiance) {
		EntityCacheUtil.removeResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceImpl.class, experiance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Experiance> experiances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Experiance experiance : experiances) {
			EntityCacheUtil.removeResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
				ExperianceImpl.class, experiance.getPrimaryKey());
		}
	}

	/**
	 * Creates a new experiance with the primary key. Does not add the experiance to the database.
	 *
	 * @param experianceId the primary key for the new experiance
	 * @return the new experiance
	 */
	public Experiance create(long experianceId) {
		Experiance experiance = new ExperianceImpl();

		experiance.setNew(true);
		experiance.setPrimaryKey(experianceId);

		return experiance;
	}

	/**
	 * Removes the experiance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param experianceId the primary key of the experiance
	 * @return the experiance that was removed
	 * @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance remove(long experianceId)
		throws NoSuchExperianceException, SystemException {
		return remove(Long.valueOf(experianceId));
	}

	/**
	 * Removes the experiance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the experiance
	 * @return the experiance that was removed
	 * @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Experiance remove(Serializable primaryKey)
		throws NoSuchExperianceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Experiance experiance = (Experiance)session.get(ExperianceImpl.class,
					primaryKey);

			if (experiance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchExperianceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(experiance);
		}
		catch (NoSuchExperianceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Experiance removeImpl(Experiance experiance)
		throws SystemException {
		experiance = toUnwrappedModel(experiance);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, experiance);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(experiance);

		return experiance;
	}

	@Override
	public Experiance updateImpl(info.diit.portal.model.Experiance experiance,
		boolean merge) throws SystemException {
		experiance = toUnwrappedModel(experiance);

		boolean isNew = experiance.isNew();

		ExperianceModelImpl experianceModelImpl = (ExperianceModelImpl)experiance;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, experiance, merge);

			experiance.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ExperianceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((experianceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(experianceModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTEXPERIANCELIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST,
					args);

				args = new Object[] {
						Long.valueOf(experianceModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTEXPERIANCELIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST,
					args);
			}
		}

		EntityCacheUtil.putResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceImpl.class, experiance.getPrimaryKey(), experiance);

		return experiance;
	}

	protected Experiance toUnwrappedModel(Experiance experiance) {
		if (experiance instanceof ExperianceImpl) {
			return experiance;
		}

		ExperianceImpl experianceImpl = new ExperianceImpl();

		experianceImpl.setNew(experiance.isNew());
		experianceImpl.setPrimaryKey(experiance.getPrimaryKey());

		experianceImpl.setExperianceId(experiance.getExperianceId());
		experianceImpl.setCompanyId(experiance.getCompanyId());
		experianceImpl.setUserId(experiance.getUserId());
		experianceImpl.setUserName(experiance.getUserName());
		experianceImpl.setCreateDate(experiance.getCreateDate());
		experianceImpl.setModifiedDate(experiance.getModifiedDate());
		experianceImpl.setOrganizationId(experiance.getOrganizationId());
		experianceImpl.setOrganization(experiance.getOrganization());
		experianceImpl.setDesignation(experiance.getDesignation());
		experianceImpl.setStartDate(experiance.getStartDate());
		experianceImpl.setEndDate(experiance.getEndDate());
		experianceImpl.setCurrentStatus(experiance.getCurrentStatus());
		experianceImpl.setStudentId(experiance.getStudentId());

		return experianceImpl;
	}

	/**
	 * Returns the experiance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the experiance
	 * @return the experiance
	 * @throws com.liferay.portal.NoSuchModelException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Experiance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the experiance with the primary key or throws a {@link info.diit.portal.NoSuchExperianceException} if it could not be found.
	 *
	 * @param experianceId the primary key of the experiance
	 * @return the experiance
	 * @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance findByPrimaryKey(long experianceId)
		throws NoSuchExperianceException, SystemException {
		Experiance experiance = fetchByPrimaryKey(experianceId);

		if (experiance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + experianceId);
			}

			throw new NoSuchExperianceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				experianceId);
		}

		return experiance;
	}

	/**
	 * Returns the experiance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the experiance
	 * @return the experiance, or <code>null</code> if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Experiance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the experiance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param experianceId the primary key of the experiance
	 * @return the experiance, or <code>null</code> if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance fetchByPrimaryKey(long experianceId)
		throws SystemException {
		Experiance experiance = (Experiance)EntityCacheUtil.getResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
				ExperianceImpl.class, experianceId);

		if (experiance == _nullExperiance) {
			return null;
		}

		if (experiance == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				experiance = (Experiance)session.get(ExperianceImpl.class,
						Long.valueOf(experianceId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (experiance != null) {
					cacheResult(experiance);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
						ExperianceImpl.class, experianceId, _nullExperiance);
				}

				closeSession(session);
			}
		}

		return experiance;
	}

	/**
	 * Returns all the experiances where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findByStudentExperianceList(long studentId)
		throws SystemException {
		return findByStudentExperianceList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the experiances where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of experiances
	 * @param end the upper bound of the range of experiances (not inclusive)
	 * @return the range of matching experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findByStudentExperianceList(long studentId,
		int start, int end) throws SystemException {
		return findByStudentExperianceList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the experiances where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of experiances
	 * @param end the upper bound of the range of experiances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findByStudentExperianceList(long studentId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTEXPERIANCELIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<Experiance> list = (List<Experiance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Experiance experiance : list) {
				if ((studentId != experiance.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_EXPERIANCE_WHERE);

			query.append(_FINDER_COLUMN_STUDENTEXPERIANCELIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<Experiance>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first experiance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching experiance
	 * @throws info.diit.portal.NoSuchExperianceException if a matching experiance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance findByStudentExperianceList_First(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchExperianceException, SystemException {
		Experiance experiance = fetchByStudentExperianceList_First(studentId,
				orderByComparator);

		if (experiance != null) {
			return experiance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExperianceException(msg.toString());
	}

	/**
	 * Returns the first experiance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching experiance, or <code>null</code> if a matching experiance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance fetchByStudentExperianceList_First(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Experiance> list = findByStudentExperianceList(studentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last experiance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching experiance
	 * @throws info.diit.portal.NoSuchExperianceException if a matching experiance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance findByStudentExperianceList_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchExperianceException, SystemException {
		Experiance experiance = fetchByStudentExperianceList_Last(studentId,
				orderByComparator);

		if (experiance != null) {
			return experiance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchExperianceException(msg.toString());
	}

	/**
	 * Returns the last experiance in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching experiance, or <code>null</code> if a matching experiance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance fetchByStudentExperianceList_Last(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentExperianceList(studentId);

		List<Experiance> list = findByStudentExperianceList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the experiances before and after the current experiance in the ordered set where studentId = &#63;.
	 *
	 * @param experianceId the primary key of the current experiance
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next experiance
	 * @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance[] findByStudentExperianceList_PrevAndNext(
		long experianceId, long studentId, OrderByComparator orderByComparator)
		throws NoSuchExperianceException, SystemException {
		Experiance experiance = findByPrimaryKey(experianceId);

		Session session = null;

		try {
			session = openSession();

			Experiance[] array = new ExperianceImpl[3];

			array[0] = getByStudentExperianceList_PrevAndNext(session,
					experiance, studentId, orderByComparator, true);

			array[1] = experiance;

			array[2] = getByStudentExperianceList_PrevAndNext(session,
					experiance, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Experiance getByStudentExperianceList_PrevAndNext(
		Session session, Experiance experiance, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EXPERIANCE_WHERE);

		query.append(_FINDER_COLUMN_STUDENTEXPERIANCELIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(experiance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Experiance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the experiances.
	 *
	 * @return the experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the experiances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of experiances
	 * @param end the upper bound of the range of experiances (not inclusive)
	 * @return the range of experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the experiances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of experiances
	 * @param end the upper bound of the range of experiances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Experiance> list = (List<Experiance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EXPERIANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EXPERIANCE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Experiance>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Experiance>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the experiances where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentExperianceList(long studentId)
		throws SystemException {
		for (Experiance experiance : findByStudentExperianceList(studentId)) {
			remove(experiance);
		}
	}

	/**
	 * Removes all the experiances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Experiance experiance : findAll()) {
			remove(experiance);
		}
	}

	/**
	 * Returns the number of experiances where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching experiances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentExperianceList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTEXPERIANCELIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXPERIANCE_WHERE);

			query.append(_FINDER_COLUMN_STUDENTEXPERIANCELIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTEXPERIANCELIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of experiances.
	 *
	 * @return the number of experiances
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EXPERIANCE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the experiance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.Experiance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Experiance>> listenersList = new ArrayList<ModelListener<Experiance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Experiance>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ExperianceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EXPERIANCE = "SELECT experiance FROM Experiance experiance";
	private static final String _SQL_SELECT_EXPERIANCE_WHERE = "SELECT experiance FROM Experiance experiance WHERE ";
	private static final String _SQL_COUNT_EXPERIANCE = "SELECT COUNT(experiance) FROM Experiance experiance";
	private static final String _SQL_COUNT_EXPERIANCE_WHERE = "SELECT COUNT(experiance) FROM Experiance experiance WHERE ";
	private static final String _FINDER_COLUMN_STUDENTEXPERIANCELIST_STUDENTID_2 =
		"experiance.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "experiance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Experiance exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Experiance exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ExperiancePersistenceImpl.class);
	private static Experiance _nullExperiance = new ExperianceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Experiance> toCacheModel() {
				return _nullExperianceCacheModel;
			}
		};

	private static CacheModel<Experiance> _nullExperianceCacheModel = new CacheModel<Experiance>() {
			public Experiance toEntityModel() {
				return _nullExperiance;
			}
		};
}
package info.diit.portal.scriptlet;

import info.diit.portal.service.StudentLocalServiceUtil;
import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

public class StudentPhotoScriptlet extends JRDefaultScriptlet{
	
	@Override
	public void afterDetailEval() throws JRScriptletException {
		super.afterDetailEval();
		
		Object studentId = getFieldValue("studentId");
		
		if(studentId != null)
		{
			String imageLocation = StudentLocalServiceUtil.getStudentPhotoLocation(Long.parseLong(studentId.toString()));
			
			setVariableValue("imageLocation", imageLocation);
		}
		
	}

}

package info.diit.portal.assessment;

import info.diit.portal.model.AssessmentType;
import info.diit.portal.model.impl.AssessmentTypeImpl;
import info.diit.portal.service.AssessmentTypeLocalServiceUtil;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class AssessmentTypeApp extends Application implements PortletRequestListener{

	private Window window;
	private final static String ASSESSMENT_TEPE = "type";
	
	private ThemeDisplay themeDisplay;
	
	private GridLayout mainGridLayout;
	private BeanItemContainer<AssessmentType> typeDataSource;
	private AssessmentType assessmentType;
	private Table typeTable;
	private TextField typeField;
	
    public void init() {
        window = new Window();
        typeDataSource = new BeanItemContainer<AssessmentType>(AssessmentType.class);
        setMainWindow(window);
        window.addComponent(assessmentType());
        loadAssessmentType();
    }
    
    public GridLayout assessmentType(){
    	mainGridLayout = new GridLayout(6,17);
    	mainGridLayout.setWidth("100%");
    	mainGridLayout.setSpacing(true);
    	typeField = new TextField("Assessment Type");
    	typeField.setWidth("100%");
    	mainGridLayout.addComponent(typeField, 0, 0, 1, 0);
    	
    	HorizontalLayout rowButtonLayout = new HorizontalLayout();
    	rowButtonLayout.setSpacing(true);
    	
    	Button saveButton = new Button("Save");
    	saveButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				if (assessmentType==null) {
					assessmentType = new AssessmentTypeImpl();
					assessmentType.setNew(true);
				}
				assessmentType.setCompanyId(themeDisplay.getCompanyId());
				assessmentType.setUserId(themeDisplay.getUser().getUserId());
				assessmentType.setUserName(themeDisplay.getUser().getScreenName());
				assessmentType.setType(typeField.getValue().toString());
				
				try {
					if (assessmentType.isNew()) {
						assessmentType.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
						assessmentType.setCreateDate(new Date());
						AssessmentTypeLocalServiceUtil.addAssessmentType(assessmentType);
						window.showNotification("Type saved successfully!");
					}else{
						assessmentType.setModifiedDate(new Date());
						AssessmentTypeLocalServiceUtil.updateAssessmentType(assessmentType);
						window.showNotification("Type update successfully!");
					}
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (PortalException e) {
					e.printStackTrace();
				}
				clearType();
				loadAssessmentType();
			}
		});
    	rowButtonLayout.addComponent(saveButton);
    	
    	Button resetButton = new Button("Reset");
    	resetButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				clearType();
			}
		});
    	rowButtonLayout.addComponent(resetButton);
    	
    	mainGridLayout.addComponent(rowButtonLayout, 0, 1);
    	
    	typeTable = new Table("Assessment Type", typeDataSource);
    	typeTable.setWidth("100%");
    	typeTable.setHeight("100%");
    	typeTable.setSelectable(true);
    	typeTable.setColumnHeader(ASSESSMENT_TEPE, "Type");
    	
    	typeTable.setVisibleColumns(new String[]{ASSESSMENT_TEPE});
    	
    	typeTable.setColumnAlignment(ASSESSMENT_TEPE, Table.ALIGN_CENTER);
    	
    	mainGridLayout.addComponent(typeTable, 2, 0, 5, 15);
    	
    	HorizontalLayout tableRowButtonLayout = new HorizontalLayout();
    	tableRowButtonLayout.setSpacing(true);
    	tableRowButtonLayout.setWidth("100%");
    	
    	Label spacer = new Label();
    	tableRowButtonLayout.addComponent(spacer);
    	
    	Button editButton = new Button("Edit");
    	editButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				assessmentType = (AssessmentType) typeTable.getValue();
				if (assessmentType!=null) {
					assessmentType.setNew(false);
					showType(assessmentType);
				}
			}
		});
    	
    	tableRowButtonLayout.addComponent(editButton);
    	
    	Button deleteButton = new Button("Delete");
    	deleteButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				assessmentType = (AssessmentType) typeTable.getValue();
				if (assessmentType!=null) {
					try {
						AssessmentTypeLocalServiceUtil.deleteAssessmentType(assessmentType);
						window.showNotification("Assessment type deleted successfully!");
						loadAssessmentType();
						clearType();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
    	tableRowButtonLayout.addComponent(deleteButton);
    	
    	tableRowButtonLayout.setExpandRatio(spacer, 1);
    	
    	mainGridLayout.addComponent(tableRowButtonLayout, 2, 16, 5, 16);
    	
    	return mainGridLayout;
    }
    
    public void loadAssessmentType(){
    	if (typeDataSource!=null) {
			typeDataSource.removeAllItems();
			try {
				List<AssessmentType> typeList = AssessmentTypeLocalServiceUtil.findByOrganization(themeDisplay.getLayout().getGroup().getOrganizationId());
				for (int i = 0; i < typeList.size(); i++) {
					AssessmentType assessmentType = typeList.get(i);
					assessmentType.getType();
					typeDataSource.addBean(assessmentType);
				}
				typeTable.refreshRowCache();
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
    }
    
    public void showType(AssessmentType assessmentType){
    	typeField.setValue(assessmentType.getType());
    }
    
    public void clearType(){
    	typeField.setValue("");
    	assessmentType = null;
    	typeTable.refreshRowCache();
    	typeTable.setValue(false);
    }

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}


}

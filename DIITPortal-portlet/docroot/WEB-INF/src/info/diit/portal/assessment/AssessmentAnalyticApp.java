package info.diit.portal.assessment;

import info.diit.portal.NoSuchBatchSubjectException;
import info.diit.portal.dto.AnalyticsDto;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.model.Assessment;
import info.diit.portal.model.AssessmentStudent;
import info.diit.portal.model.AssessmentType;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.Subject;
import info.diit.portal.service.AssessmentLocalServiceUtil;
import info.diit.portal.service.AssessmentStudentLocalServiceUtil;
import info.diit.portal.service.AssessmentTypeLocalServiceUtil;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class AssessmentAnalyticApp extends Application implements PortletRequestListener{

	private Window window;
	private ThemeDisplay themeDisplay;
	private GridLayout analyticsGridLayout;
	
	private BeanItemContainer<AnalyticsDto> analyticsContainer;
	
	private final static String TEACHER = "teacher";
	private final static String SUBJECT_NAME = "subjectName";
	private final static String ASSESSMENT_TYPE = "type";
	private final static String ASSESSMENT_COUNT = "count";
	private final static String PASS_RATE = "passRate";
	private final static String AVERAGE_OBTAIN_MARK	= "averageObtainMark";
	private final static String ATTENDANCE_RATE = "attendanceRate";
	
	private List<CourseDto> courseList;
	private ComboBox courseComboBox;
	private ComboBox campusComboBox;
	private ComboBox batchComboBox;
	private ComboBox subjectComboBox;
	private List<BatchDto> batchDtoList;
	private Table analyticsTable;
	
	private List<OrganizationDto> campusList;
	private List<SubjectDto> subjectList;
	
	DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");
	DecimalFormat intDecimalFormat= new DecimalFormat("#");
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
        
        loadOrganization();
        
        window.addComponent(initAnalyticsLayout());
    }

    public GridLayout initAnalyticsLayout(){
    	analyticsGridLayout = new GridLayout(8, 8);
    	analyticsGridLayout.setSpacing(true);
    	
    	campusComboBox = new ComboBox("Campus");
    	campusComboBox.setWidth("100%");
    	campusComboBox.setImmediate(true);
    	if (campusList!=null) {
    		for (OrganizationDto campusDto : campusList) {
    			campusComboBox.addItem(campusDto);
    		}
		}
    	
    	courseComboBox = new ComboBox("Course");
    	courseComboBox.setWidth("100%");
    	courseComboBox.setImmediate(true);
    	
    	campusComboBox.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				OrganizationDto campusDto = (OrganizationDto) campusComboBox.getValue();
				loadCourse(campusDto);
				
				if (courseList!=null) {
					for (CourseDto courseDto : courseList) {
						courseComboBox.addItem(courseDto);
					}
				}
			}
		});
    	
    	batchComboBox = new ComboBox("Batch");
    	batchComboBox.setWidth("100%");
    	batchComboBox.setImmediate(true);
    	
    	courseComboBox.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				OrganizationDto campusDto = (OrganizationDto) campusComboBox.getValue();
				CourseDto courseDto = (CourseDto) courseComboBox.getValue();
				loadBatch(campusDto, courseDto);
				if (batchDtoList!=null) {
					for (BatchDto batchDto : batchDtoList) {
						batchComboBox.addItem(batchDto);
					}
				}
			}
		});
    	
    	batchComboBox.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				BatchDto batchDto = (BatchDto) batchComboBox.getValue();
				loadAnalyticsTableByBatch(batchDto);
				loadSubject(batchDto);
				
				if (subjectList!=null) {
					for (SubjectDto subjectDto : subjectList) {
						subjectComboBox.addItem(subjectDto);
					}
				}else{
					subjectComboBox.removeAllItems();
					subjectList.clear();
				}
			}
		});
    	
    	subjectComboBox = new ComboBox("Subject");
    	subjectComboBox.setWidth("100%");
    	subjectComboBox.setImmediate(true);
    	
    	
    	
    	
    	analyticsContainer = new BeanItemContainer<AnalyticsDto>(AnalyticsDto.class);
    	
    	analyticsTable = new Table("", analyticsContainer){
    		
    		protected String formatPropertyValue(Object rowId, Object colId,
    				Property property) {
    			if (property.getType()==Double.class) {
					return twoDecimalFormat.format(property.getValue());
				}
    			return super.formatPropertyValue(rowId, colId, property);
    		}
    	};
    	analyticsTable.setImmediate(true);
    	analyticsTable.setColumnHeader(TEACHER, "Teacher");
    	analyticsTable.setColumnHeader(SUBJECT_NAME, "Subject");
    	analyticsTable.setColumnHeader(ASSESSMENT_TYPE, "Assessment Type");
    	analyticsTable.setColumnHeader(ASSESSMENT_COUNT, "Type Count");
    	analyticsTable.setColumnHeader(PASS_RATE, "Pass Rate");
    	analyticsTable.setColumnHeader(AVERAGE_OBTAIN_MARK, "Average Obtain Mark");
    	analyticsTable.setColumnHeader(ATTENDANCE_RATE, "Attendance Rate");
    	
    	analyticsTable.setVisibleColumns(new String[]{TEACHER, SUBJECT_NAME, ASSESSMENT_TYPE, ASSESSMENT_COUNT, PASS_RATE, AVERAGE_OBTAIN_MARK, ATTENDANCE_RATE});
    	
    	analyticsTable.setColumnAlignment(SUBJECT_NAME,Table.ALIGN_RIGHT);
    	analyticsTable.setColumnAlignment(ASSESSMENT_TYPE,Table.ALIGN_RIGHT);
    	analyticsTable.setColumnAlignment(ASSESSMENT_COUNT,Table.ALIGN_RIGHT);
    	analyticsTable.setColumnAlignment(PASS_RATE,Table.ALIGN_RIGHT);
    	analyticsTable.setColumnAlignment(AVERAGE_OBTAIN_MARK,Table.ALIGN_RIGHT);
    	analyticsTable.setColumnAlignment(ATTENDANCE_RATE,Table.ALIGN_RIGHT);
    	
    	analyticsTable.setFooterVisible(true);
    	
    	subjectComboBox.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
				if (subjectDto!=null) {
					loadAnalyticsTableBySubject(subjectDto);
					analyticsTable.setVisibleColumns(new String[]{TEACHER, ASSESSMENT_TYPE, ASSESSMENT_COUNT, PASS_RATE, AVERAGE_OBTAIN_MARK, ATTENDANCE_RATE});
				}else{
					BatchDto batchDto = (BatchDto) batchComboBox.getValue();
					loadAnalyticsTableByBatch(batchDto);
					analyticsTable.setVisibleColumns(new String[]{TEACHER, SUBJECT_NAME, ASSESSMENT_TYPE, ASSESSMENT_COUNT, PASS_RATE, AVERAGE_OBTAIN_MARK, ATTENDANCE_RATE});
				}
				
			}
		});
    	
    	HorizontalLayout comboBoxRow = new HorizontalLayout();
    	comboBoxRow.setWidth("100%");
    	comboBoxRow.setSpacing(true);
    	comboBoxRow.addComponent(campusComboBox);
    	comboBoxRow.addComponent(courseComboBox);
    	comboBoxRow.addComponent(batchComboBox);
    	comboBoxRow.addComponent(subjectComboBox);
    	
    	analyticsGridLayout.addComponent(comboBoxRow, 0, 0, 7, 0);
    	analyticsGridLayout.addComponent(analyticsTable, 0, 1, 7, 7);
    	
    	return analyticsGridLayout;
    }
    
    private List<OrganizationDto> loadOrganization(){
    	if (campusList==null) {
    		campusList = new ArrayList<OrganizationDto>();
		}else{
			campusList.clear();
		}
    	try {
			List<Organization> organizationList = OrganizationLocalServiceUtil.search(themeDisplay.getCompanyId(), 
					OrganizationConstants.ANY_PARENT_ORGANIZATION_ID, null, OrganizationConstants.TYPE_REGULAR_ORGANIZATION, null, null, null, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for (Organization organization : organizationList) {
				OrganizationDto campusDto = new OrganizationDto();
				campusDto.setOrganizationId(organization.getOrganizationId());
				campusDto.setOrganizationName(organization.getName());
				campusList.add(campusDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    	
    	return campusList;
    }
    
    private void loadCourse(OrganizationDto campusDto){
    	if (courseList==null) {
			courseList = new ArrayList<CourseDto>();
		}else{
			courseComboBox.removeAllItems();
			courseList.clear();
		}
    	try {
    		if (campusDto!=null) {
    			List<CourseOrganization> courseOrganizationList = CourseOrganizationLocalServiceUtil.findByOrganization(campusDto.getOrganizationId());
    			for (CourseOrganization courseOrganization : courseOrganizationList) {
    				CourseDto courseDto = new CourseDto();
    				courseDto.setCourseId(courseOrganization.getCourseId());
    				Course course = CourseLocalServiceUtil.fetchCourse(courseOrganization.getCourseId());
    				courseDto.setCourseName(course.getCourseName());
    				courseList.add(courseDto);
    			}
			}else{
				courseComboBox.removeAllItems();
				courseList.clear();
				clearFooter();
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }

	private void loadBatch(OrganizationDto campusDto, CourseDto courseDto){
		if (batchDtoList==null) {
			batchDtoList = new ArrayList<BatchDto>();
		}else{
			batchComboBox.removeAllItems();
			batchDtoList.clear();
		}
		try {
			if (campusDto!=null & courseDto!=null) {
				List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(campusDto.getOrganizationId(), courseDto.getCourseId());
				for (Batch batch : batchList) {
					BatchDto batchDto = new BatchDto();
					batchDto.setBatchId(batch.getBatchId());
					batchDto.setBatchName(batch.getBatchName());
					batchDtoList.add(batchDto);
				}
			}else{
				clearFooter();
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	
	private void loadSubject(BatchDto batchDto){
		if (subjectList==null) {
			subjectList = new ArrayList<SubjectDto>();
		}else{
			subjectComboBox.removeAllItems();
			subjectList.clear();
		}
		
		try {
			if (batchDto!=null) {
				List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.findSubjectByBatch(batchDto.getBatchId());
				for (BatchSubject batchSubject : batchSubjectList) {
					SubjectDto subjectDto = new SubjectDto();
					Subject subject = SubjectLocalServiceUtil.fetchSubject(batchSubject.getSubjectId());
					subjectDto.setSubjectId(subject.getSubjectId());
					subjectDto.setSubjcetName(subject.getSubjectName());
					subjectList.add(subjectDto);
				}
			}else{
				clearFooter();
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadAnalyticsTableByBatch(BatchDto batchDto){
		try {
			analyticsContainer.removeAllItems();
			analyticsTable.removeAllItems();
			if (batchDto!=null) {
				List<Assessment> assessmentList = AssessmentLocalServiceUtil.findByBatchStatus(batchDto.getBatchId(), 1);
				if (assessmentList.size()>0) {
					calculateAssessment(assessmentList);
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadAnalyticsTableBySubject(SubjectDto subjectDto){
		try {
			analyticsContainer.removeAllItems();
			analyticsTable.removeAllItems();
			List<Assessment> assessmentList = AssessmentLocalServiceUtil.findBySubjectStatus(subjectDto.getSubjectId(), 1);
			if (assessmentList.size()>0) {
				calculateAssessment(assessmentList);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void calculateAssessment(List<Assessment> assessmentList){
		Set<Long> subjectId = new HashSet();
		Set<Long> assessmentType = new HashSet();
		for (Assessment assessment : assessmentList) {
			subjectId.add(assessment.getSubjectId());
			assessmentType.add(assessment.getAssessmentTypeId());
		}
		try {
			for (Long type : assessmentType) {
				for (Long subject : subjectId) {
					List<Assessment> assessList = AssessmentLocalServiceUtil.findBySubjectAssessmentType(subject, type);
					
					if (assessList!=null && assessList.size()>0) {
						double obtainMark = 0;
						int presentStudent = 0;
						int absentStudent = 0;
						double averageMark = 0;
						double pass = 0;
						
						for (int i=0; i<1; i++) {
							
							Assessment assessment = assessList.get(i);
							AnalyticsDto analyticsDto = new AnalyticsDto();
							
							Subject sub = SubjectLocalServiceUtil.fetchSubject(assessment.getSubjectId());
							BatchSubject batchSubject;
							try {
								batchSubject = BatchSubjectLocalServiceUtil.findBySubject(sub.getSubjectId());
								User user = UserLocalServiceUtil.fetchUser(batchSubject.getTeacherId());
								analyticsDto.setTeacher(user.getFullName());
							} catch (NoSuchBatchSubjectException e) {
								e.printStackTrace();
							}
							
							
							
							analyticsDto.setSubjectName(sub.getSubjectName());
							AssessmentType assessType = AssessmentTypeLocalServiceUtil.fetchAssessmentType(assessment.getAssessmentTypeId());
							analyticsDto.setType(assessType.getType());
							analyticsDto.setCount(assessList.size());
							
							for (int j=0; j<assessList.size(); j++) {
								Assessment assess = assessList.get(j);
								double totalMark = assess.getTotalMark();
								double fourtyPercent = totalMark*0.4;
								List<AssessmentStudent> assessmentStudentList = AssessmentStudentLocalServiceUtil.findByAssessmentId(assess.getAssessmentId());
								for (AssessmentStudent assessmentStudent : assessmentStudentList) {
									if (totalMark>0) {
										obtainMark = obtainMark+assessmentStudent.getObtainMark()*100/totalMark;
									}
									
									if (assessmentStudent.getStatus()==1) {
										presentStudent++;
									}else{
										absentStudent++;
									}
									if (assessmentStudent.getObtainMark()>=fourtyPercent) {
										pass++;
									}
								}
							}
							double passRate = 0;
							if (presentStudent>0) {
								averageMark = obtainMark/presentStudent;
								passRate = pass*100/presentStudent;
							}
							double totalStudent = presentStudent+absentStudent;
							double presentRate = 0;
							if (totalStudent>0) {
								presentRate = presentStudent*100/totalStudent;
							}
							
							analyticsDto.setAverageObtainMark(averageMark);
							analyticsDto.setAttendanceRate(presentRate);
							analyticsDto.setPassRate(passRate);
							
							analyticsContainer.addBean(analyticsDto);
						}
						
					}
				}
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		loadFooter();
	}
	
	private void loadFooter(){
		double totalObtainMark = 0;
		double totalType = 0;
		double totalPassRate = 0;
		double totalAttendanceRate = 0;
		for (int i = 0; i < analyticsContainer.size(); i++) {
			AnalyticsDto analyticsDto = analyticsContainer.getIdByIndex(i);
			totalObtainMark = totalObtainMark+analyticsDto.getAverageObtainMark();
			totalType = totalType+analyticsDto.getCount();
			totalPassRate = totalPassRate+analyticsDto.getPassRate();
			totalAttendanceRate = totalAttendanceRate+analyticsDto.getAttendanceRate();
		}
		
		
		if (analyticsContainer.size()>0) {
			totalObtainMark = totalObtainMark/analyticsContainer.size();
			totalType = totalType/analyticsContainer.size();
			totalPassRate = totalPassRate/analyticsContainer.size();
			totalAttendanceRate = totalAttendanceRate/analyticsContainer.size();
		}
		
		analyticsTable.setColumnFooter(ASSESSMENT_TYPE, "Total");
		analyticsTable.setColumnFooter(ASSESSMENT_COUNT, twoDecimalFormat.format(totalType));
		analyticsTable.setColumnFooter(PASS_RATE, twoDecimalFormat.format(totalPassRate));
		analyticsTable.setColumnFooter(AVERAGE_OBTAIN_MARK, twoDecimalFormat.format(totalObtainMark));
		analyticsTable.setColumnFooter(ATTENDANCE_RATE, twoDecimalFormat.format(totalAttendanceRate));
		
		analyticsTable.refreshRowCache();
	}
	
	private void clearFooter(){
		analyticsTable.setColumnFooter(ASSESSMENT_TYPE, null);
		analyticsTable.setColumnFooter(ASSESSMENT_COUNT, null);
		analyticsTable.setColumnFooter(PASS_RATE, null);
		analyticsTable.setColumnFooter(AVERAGE_OBTAIN_MARK, null);
		analyticsTable.setColumnFooter(ATTENDANCE_RATE, null);
	}
    
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

package info.diit.portal.assessment;

import info.diit.portal.NoSuchAssessmentException;
import info.diit.portal.dto.AssessmentDto;
import info.diit.portal.dto.AssessmentTypeDto;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.StudentDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.model.Assessment;
import info.diit.portal.model.AssessmentStudent;
import info.diit.portal.model.AssessmentType;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseSubject;
import info.diit.portal.model.Student;
import info.diit.portal.model.Subject;
import info.diit.portal.model.impl.AssessmentImpl;
import info.diit.portal.model.impl.AssessmentStudentImpl;
import info.diit.portal.service.AssessmentLocalServiceUtil;
import info.diit.portal.service.AssessmentStudentLocalServiceUtil;
import info.diit.portal.service.AssessmentTypeLocalServiceUtil;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseSubjectLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class AssessmentManageApp extends Application implements PortletRequestListener{

	private final static String STUDENT_ID = "studentId";
	private final static String STUDENT_NAME = "studentName";
	private final static String OBTAIN_MARK = "obtainMark";
	
	private static ThemeDisplay themeDisplay;
	private TabSheet tabSheet;
	private GridLayout createAssessmentLayout;
	private static Window window;
	private List batchIdList;
	private List subjectIdList;
	private List<BatchSubject> batchSubjectList;
	private Assessment assessment;
	private Table studentTable;
	private List studentIdList;
	private AssessmentStudent assessmentStudent;
	
	private static BeanItemContainer<StudentDto> studentContainer;
	
	private GridLayout assessmentListLayout;
	private BeanItemContainer<AssessmentDto> assessmentContainer;
	private static Table assessmentListTable;
	
	
	private List<AssessmentTypeDto> assessmentTypeList;
	private List<BatchDto> batchDtoList;
	private List<SubjectDto> subjectDtoList;
	
	private final static String BATCH_NAME = "batchName";
	private final static String SUBJECT_NAME = "subjectName";
	private final static String ASSESSMENT_DATE = "assessmentDate";
	private final static String RESULT_DATE = "resultDate";
	private final static String STATUS = "status";
	private final static String TOTAL_MARK = "totalMark";
	
	private final static String PENDING = "Pending";
	private final static String COMPLETED = "Completed";
	private final static String DATE_FORMAT = "dd/MM/yyyy";
	private final static String ABSENT = "Absent";
	private Button printButton;

	DecimalFormat decimalFormat= new DecimalFormat("#");
	
	public void init() {
		window = new Window();

		setMainWindow(window);
		loadAssessmentType();
		getBatchSubject();
		loadBatch();
		
		studentContainer = new BeanItemContainer<StudentDto>(StudentDto.class);
		assessmentContainer = new BeanItemContainer<AssessmentDto>(AssessmentDto.class);
		
		loadAssessmentList();
				
		window.addComponent(tabSheet());
	}

	public TabSheet tabSheet(){
		tabSheet = new TabSheet();
		tabSheet.addTab(createAssessmentLayout(), "Assessment");
		tabSheet.addTab(assessmentListLayout(), "Assessment List");
		return tabSheet;
	}
	
	private static NumericField totalMarkField;
	private static DateField assessmentDateField;
	private DateField resultDateField;
	private static ComboBox assessmentTypeComboBox;
	private static ComboBox batchComboBox;
	private static ComboBox subjectComboBox;
	private Label studentStatus;
	
	
	public GridLayout createAssessmentLayout(){
		createAssessmentLayout = new GridLayout(9, 8);
		createAssessmentLayout.setWidth("100%");
		createAssessmentLayout.setSpacing(true);
		
		assessmentDateField = new DateField("Assessment Data");
		assessmentDateField.setWidth("100%");
		assessmentDateField.setDateFormat(DATE_FORMAT);
		createAssessmentLayout.addComponent(assessmentDateField, 0, 0, 2, 0);
		
		resultDateField = new DateField("Result Date");
		resultDateField.setWidth("100%");
		resultDateField.setDateFormat(DATE_FORMAT);
		
		
				
		createAssessmentLayout.addComponent(resultDateField, 5, 0, 7, 0);
		
		assessmentTypeComboBox = new ComboBox("Assessment Type");
		assessmentTypeComboBox.setWidth("100%");
		assessmentTypeComboBox.setImmediate(true);
		assessmentTypeComboBox.setNullSelectionAllowed(false);
		
		if (assessmentTypeList!=null) {
			for (AssessmentTypeDto assessmentTypeDto : assessmentTypeList) {
				assessmentTypeComboBox.addItem(assessmentTypeDto);
			}
		}
		
		createAssessmentLayout.addComponent(assessmentTypeComboBox, 0, 1, 2, 1);
		
		totalMarkField = new NumericField("Total Mark");
        totalMarkField.setFormat(decimalFormat);
        
        
		totalMarkField.setWidth("100%");
		totalMarkField.setNumberType(NumericFieldType.INTEGER);
				
		createAssessmentLayout.addComponent(totalMarkField, 5, 1, 7, 1);
		
		batchComboBox = new ComboBox("Batch");
		batchComboBox.setImmediate(true);
		batchComboBox.setReadOnly(true);
		batchComboBox.setWidth("100%");
		batchComboBox.setNullSelectionAllowed(false);
		createAssessmentLayout.addComponent(batchComboBox, 0, 2, 2, 2);
		
		
		totalMarkField.addListener(new BlurListener() {
			
			public void blur(BlurEvent event) {
				double totalMark = Double.parseDouble(totalMarkField.getValue().toString());
		        if (totalMark>0) {
					batchComboBox.setReadOnly(false);
				}
			}
		});
        
        
		if (batchDtoList!=null) {
			for (BatchDto batchDto : batchDtoList) {
				batchComboBox.addItem(batchDto);
			}
		}
		
		
		
		subjectComboBox = new ComboBox("Subject");
		subjectComboBox.setWidth("100%");
		subjectComboBox.setImmediate(true);
		subjectComboBox.setNullSelectionAllowed(false);
		
		studentTable = new Table("",studentContainer)
		{
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if (property.getType()==Double.class) {
					return decimalFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				/*if (subjectIdList==null) {
					subjectIdList = new ArrayList();
				}else{
					subjectIdList.clear();
				}*/
				
				if (subjectDtoList==null) {
					subjectDtoList = new ArrayList<SubjectDto>();
				}else{
					subjectComboBox.removeAllItems();
					subjectDtoList.clear();
				}
				
				if (batch!=null) {
					try {
						List<BatchSubject> batchSubject = BatchSubjectLocalServiceUtil.findByBatch(batch.getBatchId(), themeDisplay.getUser().getUserId());
						for (BatchSubject batchSub : batchSubject) {
							/*subjectIdList.add(batchSub.getSubjectId());
							loadSubject();*/
							
							Subject subject = SubjectLocalServiceUtil.fetchSubject(batchSub.getSubjectId());
							SubjectDto subjectDto = new SubjectDto();
							subjectDto.setSubjectId(subject.getSubjectId());
							subjectDto.setSubjcetName(subject.getSubjectName());
							subjectDtoList.add(subjectDto);
						}
						
						if (subjectDtoList!=null) {
							for (SubjectDto subjectDto : subjectDtoList) {
								subjectComboBox.addItem(subjectDto);
							}
						}
						
						findStudent(batch.getBatchId());
						loadStudent();
						
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}else{
					studentTable.removeAllItems();
				}
			}
		});
		
		createAssessmentLayout.addComponent(subjectComboBox, 5, 2, 7, 2);
		
		studentTable.setWidth("100%");
		studentTable.setHeight("100%");
		studentTable.setSelectable(true);
		studentTable.setColumnHeader(STUDENT_ID, "Student Id");
		studentTable.setColumnHeader(STUDENT_NAME, "Student Name");
		studentTable.setColumnHeader(OBTAIN_MARK, "Obtain Mark");
		
		studentTable.setVisibleColumns(new String[]{STUDENT_ID, STUDENT_NAME, OBTAIN_MARK});
		
		
		studentTable.setColumnAlignment(STUDENT_ID, Table.ALIGN_CENTER);
		studentTable.setColumnAlignment(STUDENT_NAME, Table.ALIGN_CENTER);
		studentTable.setColumnAlignment(OBTAIN_MARK, Table.ALIGN_CENTER);
		studentTable.setColumnExpandRatio(STUDENT_NAME, 1);
		
		createAssessmentLayout.addComponent(studentTable, 0, 3, 8, 5);
		
		studentStatus = new Label();
		
		studentTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(STUDENT_ID)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(STUDENT_NAME)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(OBTAIN_MARK)) {
					NumericField field = new NumericField();
					field.setNullSettingAllowed(true);
					field.setNumberType(NumericFieldType.INTEGER);
					field.setWidth("100%");
					field.setNullRepresentation("");
					
					field.setReadOnly(false);
					field.addListener(new BlurListener() {
						
						public void blur(BlurEvent event) {
							loadAllStudentStatus();
						}
					});
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		studentTable.setEditable(true);
		
		
		
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				if (assessment==null) {
					assessment = new AssessmentImpl();
					assessment.setNew(true);
				}
				
				if (assessmentStudent==null) {
					assessmentStudent = new AssessmentStudentImpl();
					assessmentStudent.setNew(true);
				}
				
				assessment.setCompanyId(themeDisplay.getCompanyId());
				
					try {
						assessment.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
					} catch (PortalException e1) {
						e1.printStackTrace();
					} catch (SystemException e1) {
						e1.printStackTrace();
					}
				
				assessment.setUserId(themeDisplay.getUser().getUserId());
				assessment.setUserName(themeDisplay.getUser().getScreenName());
				
				Date assessmentDate = (Date)assessmentDateField.getValue();
				if (assessmentDate!=null) {
					assessment.setAssessmentDate(assessmentDate);
				}
				
				Date resultDate = (Date)resultDateField.getValue();
				if (resultDate!=null) {
					assessment.setResultDate(resultDate);
				}
				
				
				if (assessmentDate!=null && resultDate!=null) {
					if (resultDate.before(assessmentDate)) {
						resultDateField.setValue(null);
						window.showNotification("Please select valid result date", Window.Notification.TYPE_ERROR_MESSAGE);
						return;
					}
				}
				
				AssessmentTypeDto assessmentTypeDto = (AssessmentTypeDto) assessmentTypeComboBox.getValue();
				if (assessmentTypeDto!=null) {
					assessment.setAssessmentTypeId(assessmentTypeDto.getAssessmentTypeId());
				}
				
				double totalMark = Double.parseDouble(totalMarkField.getValue().toString());
				if (totalMark>0) {
					assessment.setTotalMark(totalMark);
				}
				
				BatchDto batchDto = (BatchDto) batchComboBox.getValue();
				if (batchDto!=null) {
					assessment.setBatchId(batchDto.getBatchId());
				}
				
				SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
				if (subjectDto!=null) {
					assessment.setSubjectId(subjectDto.getSubjectId());
				}
				
				if (checkAssessmentStatus()==true) {
					assessment.setStatus(1);
				}else{
					assessment.setStatus(0);
				}
				
				try {
					if (assessment.isNew()) {
						
						if (checkMark(Double.parseDouble(totalMarkField.getValue().toString()))==false) {
							assessment.setCreateDate(new Date());
							AssessmentLocalServiceUtil.addAssessment(assessment);
							
							if (studentContainer!=null) {
								for (int i = 0; i < studentContainer.size(); i++) {
									StudentDto student = studentContainer.getIdByIndex(i);
									assessmentStudent.setAssessmentId(assessment.getAssessmentId());
									
									assessmentStudent(student);
																	
									assessmentStudent.setCreateDate(new Date());
									AssessmentStudentLocalServiceUtil.addAssessmentStudent(assessmentStudent);
									window.showNotification("Assessment saved successfully!");
									loadAssessmentList();
								}
							}
						}else{
							window.showNotification("Student mark greater than total mark", Window.Notification.TYPE_ERROR_MESSAGE);
						}
					}else{
						if (checkMark(Double.parseDouble(totalMarkField.getValue().toString()))==false) {
							assessment.setModifiedDate(new Date());
							AssessmentLocalServiceUtil.updateAssessment(assessment);
							
							List<AssessmentStudent> assessmentStudentList = AssessmentStudentLocalServiceUtil.findByAssessmentId(assessment.getAssessmentId());
							for (AssessmentStudent assessmentStudent : assessmentStudentList) {
								AssessmentStudentLocalServiceUtil.deleteAssessmentStudent(assessmentStudent);
							}
							
							if (studentContainer!=null) {
								for (int i = 0; i < studentContainer.size(); i++) {
									StudentDto student = studentContainer.getIdByIndex(i);
									assessmentStudent.setAssessmentId(assessment.getAssessmentId());
									
									assessmentStudent(student);
									assessmentStudent.setModifiedDate(new Date());
									AssessmentStudentLocalServiceUtil.addAssessmentStudent(assessmentStudent);
									window.showNotification("Assessment updated successfully");
									loadAssessmentList();
								}
							}
						}else{
							window.showNotification("Student mark greater than total mark", Window.Notification.TYPE_ERROR_MESSAGE);
						}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		Button resetButton = new Button("Reset");
		
		HorizontalLayout rowLableLayout = new HorizontalLayout();
		rowLableLayout.setSpacing(true);
				
		resetButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		final Embedded pdfContents = new Embedded();
        pdfContents.setSizeFull();
        pdfContents.setType(Embedded.TYPE_BROWSER);
		
		printButton = new Button("Print");
		printButton.setImmediate(true);
		printButton.setEnabled(false);
		printButton.addListener(new ClickListener() {
		
			public void buttonClick(ClickEvent event) {
				displayPopup();
			}
		});
				
		HorizontalLayout rowButtonLayout = new HorizontalLayout();
		rowButtonLayout.setWidth("100%");
		rowButtonLayout.setHeight("100%");
		rowButtonLayout.setSpacing(true);
		
		rowButtonLayout.addComponent(studentStatus);
		rowButtonLayout.addComponent(saveButton);
		rowButtonLayout.addComponent(resetButton);
		rowButtonLayout.addComponent(printButton);
		rowButtonLayout.setExpandRatio(studentStatus, 1);
		
		createAssessmentLayout.addComponent(rowButtonLayout, 0, 6, 8, 6);
		
		return createAssessmentLayout;
	}
	
	public void loadAllStudentStatus(){
		if (studentContainer!=null) {
			double avgMark = 0;
			int pass = 0;
			int present = 0;
			double totalMark = Double.parseDouble(totalMarkField.getValue().toString());
			double fourtyPercent = totalMark*0.4;
			for (int i = 0; i < studentContainer.size(); i++) {
				StudentDto studentDto = studentContainer.getIdByIndex(i);
				if (studentDto.getObtainMark()!=null && !studentDto.getObtainMark().equals("")) {
					double obtainedMark = studentDto.getObtainMark();
					if (obtainedMark<=totalMark) {
						avgMark = avgMark+obtainedMark;
						present++;
						
						if (totalMark>0) {
							if (obtainedMark>=fourtyPercent) {
								pass++;
							}
						}
					}
				}
			}
			
			double averageMark = avgMark/present;
			if (pass>0) {
				double rate = pass*100/present;
				DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");
				studentStatus.setCaption("Averager Mark:"+twoDecimalFormat.format(averageMark)+"  Pass Rate:"+twoDecimalFormat.format(rate)+"%");
			}
		}
	}
	
	public void clear(){
		assessmentDateField.setValue(null);
		resultDateField.setValue(null);
		assessmentTypeComboBox.setValue(null);
		totalMarkField.setValue("");
		batchComboBox.setValue(null);
		batchComboBox.setReadOnly(true);
		subjectComboBox.setValue(null);
		studentStatus.setCaption("");
		assessment = null;
	}
	
	public Boolean checkMark(double totalMark){
		boolean status = true;
		if (studentContainer!=null) {
			for (int i = 0; i < studentContainer.size(); i++) {
				StudentDto student = studentContainer.getIdByIndex(i);
				if (student.getObtainMark()!=null && !student.getObtainMark().equals("") && student.getObtainMark()>totalMark) {
					status = true;
					break;
				}else{
					status = false;
				}
			}
		}
		return status;
	}
	
	public Boolean checkAssessmentStatus(){
		boolean status = false;
		if (studentContainer!=null) {
			for (int i = 0; i < studentContainer.size(); i++) {
				StudentDto student = studentContainer.getIdByIndex(i);
				if (student.getObtainMark()!=null) {
					status = true;
				}
			}
		}
		return status;
	}
	
	public void assessmentStudent(StudentDto student){
		long studentId = student.getStudentId();
		if (studentId>0) {
			assessmentStudent.setStudentId(studentId);
		}
		
		if (student.getObtainMark()!=null && !student.getObtainMark().equals("")) {
			assessmentStudent.setStatus(1);
			assessmentStudent.setObtainMark(student.getObtainMark());
		} else{
			assessmentStudent.setStatus(0);
			assessmentStudent.setObtainMark(0);
		}
		
		assessmentStudent.setCompanyId(themeDisplay.getCompanyId());
		
		try {
			assessmentStudent.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		assessmentStudent.setUserId(themeDisplay.getUser().getUserId());
		assessmentStudent.setUserName(themeDisplay.getUser().getScreenName());
	}
	
	private List<AssessmentTypeDto> loadAssessmentType(){
		
		try {
			List<AssessmentType> assessmentTypes = AssessmentTypeLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			
			if (assessmentTypeList==null) {
				assessmentTypeList = new ArrayList<AssessmentTypeDto>();
			}else{
				assessmentTypeList.clear();
			}
			
			for (AssessmentType assessmentType : assessmentTypes) {
				AssessmentTypeDto assessmentTypeDto = new AssessmentTypeDto();
				assessmentTypeDto.setAssessmentTypeId(assessmentType.getAssessmentTypeId());
				assessmentTypeDto.setAssessmentType(assessmentType.getType());
				assessmentTypeList.add(assessmentTypeDto);
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		return assessmentTypeList;
	}
	
	private void getBatchSubject(){
		batchIdList = new ArrayList();
		try {
			batchSubjectList = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
			
			for (int i = 0; i < batchSubjectList.size(); i++) {
				BatchSubject batch = batchSubjectList.get(i);
				batchIdList.add(batch.getBatchId());
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private List<BatchDto> loadBatch(){
		try {
			List<Batch> batchList = BatchLocalServiceUtil.findAllBatchByCompanyId(themeDisplay.getCompanyId());
			if (batchDtoList==null) {
				batchDtoList = new ArrayList<BatchDto>();
			}else{
				batchDtoList.clear();
			}
			
			for (Batch batch : batchList) {
				if (checkUserBatch(batch.getBatchId())) {
					BatchDto batchDto = new BatchDto();
					batchDto.setBatchId(batch.getBatchId());
					batchDto.setBatchName(batch.getBatchName());
					batchDtoList.add(batchDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		return batchDtoList;
	}
	
	private Boolean checkUserBatch(long batchId){
		boolean status = true;
		if (batchIdList!=null) {
			for (int i = 0; i < batchIdList.size(); i++) {
				Object id = batchIdList.get(i);
				if (id.equals(batchId)) {
					status = true;
					break;
				}else{
					status = false;
				}
			}
		}
		return status;
	}
	
	public List findStudent(long batchId){
		studentIdList = new ArrayList();
		try {
			List<BatchStudent> batchStudentList = BatchStudentLocalServiceUtil.findByBatch(batchId);
			for (BatchStudent batchStudent : batchStudentList) {
				studentIdList.add(batchStudent.getStudentId());
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return studentIdList;
	}
	
	private List<StudentDto> studentDtoList;
	
	public void loadStudent(){
		if (studentContainer!=null) {
			studentContainer.removeAllItems();
			studentDtoList = getStudent();
			for (int i = 0; i < studentDtoList.size(); i++) {
				StudentDto studentDto = studentDtoList.get(i);
				studentDto.getStudentId();
				studentDto.getName();
				studentDto.getObtainMark();
				studentContainer.addBean(studentDto);
			}
		}
	}
	
	public List<StudentDto> getStudent(){
		List<StudentDto> studentDtoList = new ArrayList<StudentDto>();
		try {
			List<Student> studentList = StudentLocalServiceUtil.getStudents(0, StudentLocalServiceUtil.getStudentsCount());
			for (int i = 0; i < studentList.size(); i++) {
				Student student = studentList.get(i);
				if (checkStudent(student.getStudentId())) {
					StudentDto studentDto = new StudentDto();
					studentDto.setStudentId(student.getStudentId());
					studentDto.setName(student.getName());
					if (assessment!=null) {
						List<AssessmentStudent> assessmentStudents = AssessmentStudentLocalServiceUtil.findByAssessmentId(assessment.getAssessmentId());
						for (AssessmentStudent assessmentStudent : assessmentStudents) {
							if (assessmentStudent.getStudentId()==student.getStudentId()) {
								if (assessmentStudent.getStatus()==1) {
									studentDto.setObtainMark((int)Math.round(assessmentStudent.getObtainMark()));
								}else{
									studentDto.setObtainMark(null);
								}
							}
						}
					}
					studentDtoList.add(studentDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		return studentDtoList;
	}
	
	public Boolean checkStudent(long studentId){
		boolean status = true;
		for (int i = 0; i < studentIdList.size(); i++) {
			Object id = studentIdList.get(i);
			if (id.equals(studentId)) {
				status = true;
				break;
			}else{
				status = false;
			}
		}
		return status;
	}
	
	public GridLayout assessmentListLayout(){
		assessmentListLayout = new GridLayout(2, 2);
		assessmentListLayout.setWidth("100%");
		
		final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		
		assessmentListTable = new Table("Assessment List", assessmentContainer){
			
			protected String formatPropertyValue(Object rowId, Object colId, Property property){
				if (property.getType()==Date.class) {
					return dateFormat.format(property.getValue());
				}
				
                if (property.getType()==Double.class) {
                   return decimalFormat.format(property.getValue());
                }
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		
		assessmentListTable.setWidth("100%");
		assessmentListTable.setImmediate(true);
		assessmentListTable.setSelectable(true);
		assessmentListTable.setColumnHeader(BATCH_NAME, "Batch Name");
		assessmentListTable.setColumnHeader(SUBJECT_NAME, "Subject Name");
		assessmentListTable.setColumnHeader(ASSESSMENT_DATE, "Assessment Date");
		assessmentListTable.setColumnHeader(RESULT_DATE, "Result Date");
		assessmentListTable.setColumnHeader(TOTAL_MARK, "Total Mark");
		assessmentListTable.setColumnHeader(STATUS, "Status");
		
		assessmentListTable.setVisibleColumns(new String[]{BATCH_NAME, SUBJECT_NAME, ASSESSMENT_DATE, RESULT_DATE, TOTAL_MARK, STATUS});
		
		assessmentListLayout.addComponent(assessmentListTable, 0, 0, 1, 0);
		
		Label spacer = new Label();
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				AssessmentDto assessmentDto = (AssessmentDto) assessmentListTable.getValue();
				if (assessmentDto!=null) {
					assessmentDto.getAssessment().setNew(false);
					tabSheet.setSelectedTab(createAssessmentLayout);
					showAssessment(assessmentDto.getAssessmentId());
					loadStudent();
					loadAllStudentStatus();
					if (assessmentDto.getStatus()==COMPLETED) {
						printButton.setEnabled(true);
					}
					
				}
			}
		});
		
		Button viewButton = new Button("View");
		viewButton.setImmediate(true);
		
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				AssessmentDto assessmentDto = (AssessmentDto) assessmentListTable.getValue();
				if (assessmentDto!=null) {
					try {
						Assessment assessment = AssessmentLocalServiceUtil.fetchAssessment(assessmentDto.getAssessmentId());
						if (assessment.getStatus()==0) {
							AssessmentLocalServiceUtil.deleteAssessment(assessment);
							List<AssessmentStudent> assessmentStudentList = AssessmentStudentLocalServiceUtil.findByAssessmentId(assessment.getAssessmentId());
							for (AssessmentStudent assessmentStudent : assessmentStudentList) {
								AssessmentStudentLocalServiceUtil.deleteAssessmentStudent(assessmentStudent);
							}
							loadAssessmentList();
							clear();
						}else{
							window.showNotification("This assessment has been completed! You cannot delete it.", Window.Notification.TYPE_ERROR_MESSAGE);
						}
						
					} catch (SystemException e) {
						e.printStackTrace();
					}
					
				}
			}
		});
		
		
		/*printButton.setEnabled(false);
		AssessmentDto assessmentDto = (AssessmentDto) assessmentListTable.getValue();
		if (assessmentDto!=null) {
			if (assessmentDto.getStatus()==COMPLETED) {
				printButton.setEnabled(true);
			}
		}*/
		
		final Embedded pdfContents = new Embedded();
        pdfContents.setSizeFull();
        pdfContents.setType(Embedded.TYPE_BROWSER);
		
//		printButton.setImmediate(true);
		viewButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				AssessmentDto assessmentDto = (AssessmentDto) assessmentListTable.getValue();
				if (assessmentDto!=null) {
					assessmentDto.getAssessment().setNew(false);
					
				}
			}
		});
		
		
		HorizontalLayout rowButtonLayout = new HorizontalLayout();
		rowButtonLayout.setWidth("100%");
		rowButtonLayout.setSpacing(true);
		
		rowButtonLayout.addComponent(spacer);
		rowButtonLayout.addComponent(editButton);
//		rowButtonLayout.addComponent(viewButton);
		rowButtonLayout.addComponent(deleteButton);
		rowButtonLayout.setExpandRatio(spacer, 1);
		
		assessmentListLayout.addComponent(rowButtonLayout, 0, 1, 1, 1);
		
		return assessmentListLayout;
	}
	
	public void showAssessment(long assessmentId){
		try {
			assessment = AssessmentLocalServiceUtil.findByPrimaryKey(assessmentId);
			assessmentDateField.setValue(assessment.getAssessmentDate());
			resultDateField.setValue(assessment.getResultDate());
									
			assessmentTypeComboBox.setValue(getAssessmentType(assessment.getAssessmentTypeId()));
			
			totalMarkField.setValue(assessment.getTotalMark());
			
			batchComboBox.setReadOnly(false);
			batchComboBox.setValue(getBatch(assessment.getBatchId()));
			subjectComboBox.setValue(getSubject(assessment.getSubjectId()));
						
		} catch (NoSuchAssessmentException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private AssessmentTypeDto getAssessmentType(long assessmentTypeId){
		if (assessmentTypeList!=null) {
			for (AssessmentTypeDto assessmentTypeDto : assessmentTypeList) {
				if (assessmentTypeDto.getAssessmentTypeId()==assessmentTypeId) {
					return assessmentTypeDto;
				}
			}
		}
		return null;
	}
	
	private BatchDto getBatch(long batchId){
		if (batchDtoList!=null) {
			for (BatchDto batchDto : batchDtoList) {
				if (batchDto.getBatchId()==batchId) {
					return batchDto;
				}
			}
		}
		return null;
	}
	
	private SubjectDto getSubject(long subjectId){
		if (subjectDtoList!=null) {
			for (SubjectDto subjectDto : subjectDtoList) {
				return subjectDto;
			}
		}
		return null;
	}
	
	public void loadAssessmentList(){
		if (assessmentContainer!=null) {
			assessmentContainer.removeAllItems();
			
			List<AssessmentDto> assessmentList = getUserAssessmentList();
			for (int i = 0; i < assessmentList.size(); i++) {
				AssessmentDto assessmentDto = assessmentList.get(i);
				assessmentDto.getAssessmentId();
				assessmentDto.getBatchName();
				assessmentDto.getSubjectName();
				assessmentDto.getAssessmentDate();
				assessmentDto.getResultDate();
				assessmentDto.getTotalMark();
				assessmentDto.getStatus();
				assessmentContainer.addBean(assessmentDto);
			}
		}
	}
	
	public List<AssessmentDto> getUserAssessmentList(){
		List<AssessmentDto> assessments = new ArrayList<AssessmentDto>();
		
		try {
			List<Assessment> assessmentList = AssessmentLocalServiceUtil.findByUser(themeDisplay.getUser().getUserId());
			for (int i = 0; i < assessmentList.size(); i++) {
				Assessment assessment = assessmentList.get(i);
				AssessmentDto assessmentDto = new AssessmentDto();
				
				assessmentDto.setAssessmentId(assessment.getAssessmentId());
				Object batchId = assessment.getBatchId();
				assessmentDto.setBatchId(assessment.getBatchId());
				
				List<Batch> batchList = BatchLocalServiceUtil.findAllBatchByCompanyId(themeDisplay.getCompanyId());
				for (int j = 0; j <batchList.size(); j++) {
					Batch batch = batchList.get(j);
					if (batchId.equals(batch.getBatchId())) {
						assessmentDto.setBatchName(batch.getBatchName());
					}
				}
				Object subjectId = assessment.getSubjectId();
				assessmentDto.setSubjectId(assessment.getSubjectId());
				
				List<Subject> subjectList = SubjectLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
				for (int j = 0; j < subjectList.size(); j++) {
					Subject subject = subjectList.get(j);
					if (subjectId.equals(subject.getSubjectId())) {
						assessmentDto.setSubjectName(subject.getSubjectName());
					}
				}
				
				assessmentDto.setAssessmentDate(assessment.getAssessmentDate());
				assessmentDto.setResultDate(assessment.getResultDate());
				assessmentDto.setTotalMark(assessment.getTotalMark());
				
				if (assessment.getStatus()==0) {
					assessmentDto.setStatus(PENDING);
				}else{
					assessmentDto.setStatus(COMPLETED);
				}
				
				assessmentDto.setAssessmentTypeId(assessment.getAssessmentTypeId());
				
				assessments.add(assessmentDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return assessments;
	}
	
	private Resource createPdf() {
        // Here we create a new StreamResource which downloads our StreamSource,
        // which is our pdf.
        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        // Set the right mime type
        resource.setMIMEType("application/pdf");
        return resource;
    }

    private void displayPopup() {
        Window win = new Window();
        win.getContent().setSizeFull();
        win.setResizable(true);
        win.setWidth("800");
        win.setHeight("600");
        win.center();
        Embedded e = new Embedded();
        e.setSizeFull();
        e.setType(Embedded.TYPE_BROWSER);

        // Here we create a new StreamResource which downloads our StreamSource,
        // which is our pdf.
        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        // Set the right mime type
        resource.setMIMEType("application/pdf");

        e.setSource(resource);
        win.addComponent(e);
        getMainWindow().addWindow(win);
    }

    /**
     * This class creates a PDF with the iText library. This class implements
     * the StreamSource interface which defines the getStream method.
     */
    public static class Pdf implements StreamSource {
        private final ByteArrayOutputStream os = new ByteArrayOutputStream();

        public Pdf() {
            Document document = null;

            try {
                document = new Document(PageSize.A4, 50, 50, 50, 50);
                PdfWriter.getInstance(document, os);
                document.setMarginMirroring(true);
                document.open();
                
                String companyName = themeDisplay.getCompany().getName();
                Paragraph companyParagraph = new Paragraph();
                Font companyFont = new Font();
                companyFont.setStyle(Font.BOLD);
                companyFont.setSize(16);
                companyParagraph.setFont(companyFont);
                companyParagraph.add(companyName);
                companyParagraph.setAlignment(Element.ALIGN_CENTER);
                document.add(companyParagraph);
                
                Paragraph markSheetParagraph = new Paragraph();
                Font markSheetFont = new Font();
                markSheetFont.setSize(8);
                markSheetParagraph.setFont(markSheetFont);
                markSheetParagraph.add("Assessment Mark Sheet");
                markSheetParagraph.setAlignment(Element.ALIGN_CENTER);
                markSheetParagraph.setSpacingAfter(10);
                document.add(markSheetParagraph);
                
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
                Date assessmentDate = (Date) assessmentDateField.getValue();
                BatchDto batchDto = (BatchDto) batchComboBox.getValue();
                
                PdfPTable detailTable = new PdfPTable(3);
                detailTable.setSpacingAfter(30);
                detailTable.setHorizontalAlignment(Element.ALIGN_CENTER);
                PdfPCell detailCell;
                
                SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
                if (subjectDto!=null) {
                	CourseSubject courseSubject = CourseSubjectLocalServiceUtil.findBySubject(subjectDto.getSubjectId());
                    Course course = CourseLocalServiceUtil.fetchCourse(courseSubject.getCourseId());
                    
                    detailCell = new PdfPCell(new Phrase("Course: "+course.getCourseName()));
                    detailCell.setBorder(PdfPCell.NO_BORDER);
                    detailTable.addCell(detailCell);
     
                    detailCell = new PdfPCell(new Phrase());
                    detailCell.setRotation(0);
                    detailCell.setBorder(PdfPCell.NO_BORDER);
                    detailTable.addCell(detailCell);
                    
                    detailCell = new PdfPCell(new Phrase("Subject: "+subjectDto.getSubjcetName()));
                    detailCell.setBorder(PdfPCell.NO_BORDER);
                    detailTable.addCell(detailCell);
				}
                
                detailCell = new PdfPCell(new Phrase("Batch: "+batchDto.getBatchName()));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                detailCell = new PdfPCell(new Phrase());
                detailCell.setRotation(0);
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                detailCell = new PdfPCell(new Phrase("Date: "+String.valueOf(simpleDateFormat.format(assessmentDate))));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                AssessmentTypeDto assessmentTypeDto = (AssessmentTypeDto) assessmentTypeComboBox.getValue();
                
                detailCell = new PdfPCell(new Phrase("Type: "+assessmentTypeDto.getAssessmentType()));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                detailCell = new PdfPCell(new Phrase());
                detailCell.setRotation(0);
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                String totalMark = String.valueOf(totalMarkField.getValue());
                
                detailCell = new PdfPCell(new Phrase("Total Mark: "+totalMark));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                document.add(detailTable);
                                
                PdfPTable table = new PdfPTable(3);
                PdfPCell cell;
                
                
                cell = new PdfPCell(new Phrase("Student Id"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Student Name"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Obtain Mark"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                
                if (studentContainer!=null) {
					for (int i = 0; i < studentContainer.size(); i++) {
						StudentDto studentDto = studentContainer.getIdByIndex(i);
						cell = new PdfPCell(new Phrase(String.valueOf(studentDto.getStudentId())));
						cell.setPadding(5);
						table.addCell(cell);
						cell = new PdfPCell(new Phrase(studentDto.getName()));
						cell.setPadding(5);
						table.addCell(cell);
						if (studentDto.getObtainMark()!=null) {
							cell = new PdfPCell(new Phrase(String.valueOf(studentDto.getObtainMark())));
							cell.setPadding(5);
						}else{
							cell = new PdfPCell(new Phrase(ABSENT));
							cell.setPadding(5);
						}
						
						table.addCell(cell);
					}
				}
                document.add(table);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (document != null) {
                    document.close();
                }
            }
        }
        
        public InputStream getStream() {
            // Here we return the pdf contents as a byte-array
            return new ByteArrayInputStream(os.toByteArray());
        }
    }
	
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

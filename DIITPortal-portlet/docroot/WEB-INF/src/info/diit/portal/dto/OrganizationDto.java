package info.diit.portal.dto;

import java.io.Serializable;

import com.liferay.portal.model.Organization;

public class OrganizationDto implements Serializable{

	private long organizationId;
	
	private String organizationName;

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public OrganizationDto(long organizationId, String organizationName) {
		
		this.organizationId = organizationId;
		this.organizationName = organizationName;
	}

	public OrganizationDto(Organization organization) {
		super();
		this.organizationId = organization.getOrganizationId();
		this.organizationName = organization.getName();
	}

	public OrganizationDto() {

		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getOrganizationName();
	}
	
	
}

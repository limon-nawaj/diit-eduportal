package info.diit.portal.dto;

import info.diit.portal.model.Task;
import info.diit.portal.model.impl.TaskImpl;

public class TaskDto {

	private long id;
	private String title;
	private String designation;
	private Task task;
	
	public TaskDto(){
		task = new TaskImpl();
		task.setNew(true);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}
}

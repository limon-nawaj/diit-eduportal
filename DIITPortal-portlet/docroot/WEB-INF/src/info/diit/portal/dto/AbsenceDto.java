package info.diit.portal.dto;

import java.util.Date;

public class AbsenceDto {
	private long id;
	private BatchDto batch;
	private long studentId;
	private String studentName;
	private CourseDto course;
	private Date lastPresent;
	private String phone1;
	private String phone2;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public BatchDto getBatch() {
		return batch;
	}
	public void setBatch(BatchDto batch) {
		this.batch = batch;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public CourseDto getCourse() {
		return course;
	}
	public void setCourse(CourseDto course) {
		this.course = course;
	}
	public Date getLastPresent() {
		return lastPresent;
	}
	public void setLastPresent(Date lastPresent) {
		this.lastPresent = lastPresent;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
}

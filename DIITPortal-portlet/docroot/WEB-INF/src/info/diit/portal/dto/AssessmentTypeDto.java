package info.diit.portal.dto;

public class AssessmentTypeDto {

	private long id;
	private String title;
	private Integer numberOfAssessment;
	private long assessmentTypeId;
	private String assessmentType;
	
	public long getAssessmentTypeId() {
		return assessmentTypeId;
	}
	public void setAssessmentTypeId(long assessmentTypeId) {
		this.assessmentTypeId = assessmentTypeId;
	}
	public String getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(String assessmentType) {
		this.assessmentType = assessmentType;
	}
	public String toString(){
		return getAssessmentType();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getNumberOfAssessment() {
		return numberOfAssessment;
	}
	public void setNumberOfAssessment(Integer numberOfAssessment) {
		this.numberOfAssessment = numberOfAssessment;
	}
	public int compareTo(AssessmentTypeDto obj) {
		// TODO Auto-generated method stub
		
		
			return obj.getAssessmentType().compareTo(getAssessmentType());
		
	}
}

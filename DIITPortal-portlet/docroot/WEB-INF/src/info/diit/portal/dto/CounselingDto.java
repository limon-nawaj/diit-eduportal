package info.diit.portal.dto;

import info.diit.portal.model.Counseling;
import info.diit.portal.model.CounselingCourseInterest;
import info.diit.portal.model.impl.CounselingImpl;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class CounselingDto implements Serializable{

	private Counseling counseling;
	private Set<CounselingCourseInterest> counselingCourseInterests;
	private CourseDto majorInterestCourse;
	
	public CounselingDto()
	{
		counseling = new CounselingImpl();
		counseling.setNew(true);
		
		counselingCourseInterests = new HashSet<CounselingCourseInterest>();
		majorInterestCourse = new CourseDto();
	}
	
	public Counseling getCounseling() {
		return counseling;
	}
	public void setCounseling(Counseling counseling) {
		this.counseling = counseling;
	}
	public CourseDto getMajorInterestCourse() {
		return majorInterestCourse;
	}
	public void setMajorInterestCourse(CourseDto majorInterestCourse) {
		this.majorInterestCourse = majorInterestCourse;
	}
	public Set<CounselingCourseInterest> getCounselingCourseInterests() {
		return counselingCourseInterests;
	}
	public void setCounselingCourseInterests(
			Set<CounselingCourseInterest> counselingCourseInterests) {
		this.counselingCourseInterests = counselingCourseInterests;
	}
	
}

package info.diit.portal.dto;

import com.vaadin.addon.calendar.event.BasicEvent;

public class EventDto extends BasicEvent {

	private long eventId;
	private TeacherDto teacher;
	private RoomDto room;
	private SubjectDto subject;
	
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public TeacherDto getTeacher() {
		return teacher;
	}
	public void setTeacher(TeacherDto teacher) {
		this.teacher = teacher;
		fireEventChange();
	}
	public RoomDto getRoom() {
		return room;
	}
	public void setRoom(RoomDto room) {
		this.room = room;
		fireEventChange();
	}
	public SubjectDto getSubject() {
		return subject;
	}
	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}
	
	
/*	@Override
	public void setEnd(Date end) {
        GregorianCalendar cal = new GregorianCalendar();
        
		cal.setTime(end);
		cal.setTimeZone(TimeZone.getTimeZone("GMT+6:00"));
		end = cal.getTime();
		this.end = end;
        fireEventChange();
    }
	
	@Override
	public Date getEnd(){
		
		return end;
	}*/
		
	/*@Override
	public String getCaption() {
		
		return formatter.format(getEnd())+"\n"+getTeacherName();
	}*/

}

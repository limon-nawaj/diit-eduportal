package info.diit.portal.dto;

import info.diit.portal.constant.CommunicationWith;
import info.diit.portal.constant.Media;
import info.diit.portal.constant.Subject;
import info.diit.portal.model.CommunicationRecord;
import info.diit.portal.model.impl.CommunicationRecordImpl;

import java.util.Date;

public class CommunicationDto {

	private long id;
	private BatchDto batch;
	private StudentDto student;
	private Media media;
	private Date date;
	private Subject subject;
	private CommunicationWith communicationWith;
	private String details;
	private CommunicationRecord communicationRecord;
	
	public CommunicationDto(){
		communicationRecord = new CommunicationRecordImpl();
		communicationRecord.setNew(true);
	}
	public CommunicationRecord getCommunicationRecord() {
		return communicationRecord;
	}
	public void setCommunicationRecord(CommunicationRecord communicationRecord) {
		this.communicationRecord = communicationRecord;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public BatchDto getBatch() {
		return batch;
	}
	public void setBatch(BatchDto batch) {
		this.batch = batch;
	}
	public StudentDto getStudent() {
		return student;
	}
	public void setStudent(StudentDto student) {
		this.student = student;
	}
	public Media getMedia() {
		return media;
	}
	public void setMedia(Media media) {
		this.media = media;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	public CommunicationWith getCommunicationWith() {
		return communicationWith;
	}
	public void setCommunicationWith(CommunicationWith communicationWith) {
		this.communicationWith = communicationWith;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
}

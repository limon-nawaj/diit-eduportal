package info.diit.portal.dto;

import java.io.Serializable;

public class SessionDto implements Serializable{
	private long sessionId;
	private String sessionName;
	
	public SessionDto(){
	}
	
	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	
	public String toString(){
		return getSessionName();
	}
}

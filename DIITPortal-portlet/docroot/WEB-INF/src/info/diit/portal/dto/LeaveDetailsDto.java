package info.diit.portal.dto;

import info.diit.portal.constant.Day;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LeaveDetailsDto {
	private int id;
	private Date leaveDate;
	private Day day;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
}

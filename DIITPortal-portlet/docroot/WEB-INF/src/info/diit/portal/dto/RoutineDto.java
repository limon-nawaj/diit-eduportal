package info.diit.portal.dto;

import java.util.Date;
import java.util.Set;


public class RoutineDto {
	long id;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	int	day;
	String dayName;
	Date	start;
	Date 	end;
	RoomDto room;
	/*
	long companyId;
	long organizationId;
	long subjectId;*/
	
	Set<BatchDto> batch;
	
	public Set<BatchDto> getBatch() {
		return batch;
	}
	public void setBatch(Set<BatchDto> batch) {
		this.batch = batch;
	}
	public String getDayName() {
		return dayName;
	}
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public RoomDto getRoom() {
		return room;
	}
	public void setRoom(RoomDto room) {
		this.room = room;
	}
	
}

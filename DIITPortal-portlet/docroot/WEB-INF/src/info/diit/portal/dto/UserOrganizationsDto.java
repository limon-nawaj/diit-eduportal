package info.diit.portal.dto;

import java.io.Serializable;

public class UserOrganizationsDto implements Serializable{
	private long orgId;
	private String orgName;
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	public String toString(){
		return getOrgName();
	}
}

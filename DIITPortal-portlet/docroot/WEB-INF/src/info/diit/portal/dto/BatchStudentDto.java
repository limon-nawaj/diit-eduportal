package info.diit.portal.dto;

import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.Student;

import java.io.Serializable;

public class BatchStudentDto implements Serializable {
	
	long 		companyId;
	long   		studentId;
	String 		name;
	boolean 	check;
	long 		batchId;
	long 		organizationId;	
	long		newBatchId;
	int			status;
	String		statusStr;
	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	long		batchStudentId;
	String		courseName;	
	String		batchName;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getBatchStudentId() {
		return batchStudentId;
	}

	public void setBatchStudentId(long batchStudentId) {
		this.batchStudentId = batchStudentId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getNewBatchId() {
		return newBatchId;
	}

	public void setNewBatchId(long newBatchId) {
		this.newBatchId = newBatchId;
	}

	public BatchStudentDto(){
		
	}
	
	public BatchStudentDto (Student student){
		this.studentId = student.getStudentId();
		this.name = student.getName();
	}
	
	public BatchStudentDto(Student student, BatchStudent batchStudent){
		this.studentId = student.getStudentId();
		this.name = student.getName();
		this.batchId = batchStudent.getBatchId();
		this.organizationId = batchStudent.getOrganizationId();
		this.status = batchStudent.getStatus();
		this.batchStudentId = batchStudent.getBatchStudentId();
	}
	
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

}

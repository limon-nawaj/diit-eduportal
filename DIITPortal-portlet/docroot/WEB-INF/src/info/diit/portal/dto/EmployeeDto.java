package info.diit.portal.dto;

import net.sourceforge.jtds.jdbc.DateTime;

public class EmployeeDto {

	private long id;
	private String name;
	private Double duration;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getDuration() {
		return duration;
	}
	public void setDuration(Double duration) {
		this.duration = duration;
	}
	@Override
	public String toString() {
		return getName();
	}
}

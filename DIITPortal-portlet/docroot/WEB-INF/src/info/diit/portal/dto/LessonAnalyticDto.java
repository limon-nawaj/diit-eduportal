package info.diit.portal.dto;

public class LessonAnalyticDto {

	private long id;
	private SubjectDto subject;
	private Integer totalTopic;
	private Integer completedTopic;
	private Integer remainingTopic;
	private double completedPercentage;
	private double remainingPercentage;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public SubjectDto getSubject() {
		return subject;
	}
	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}
	public Integer getTotalTopic() {
		return totalTopic;
	}
	public void setTotalTopic(Integer totalTopic) {
		this.totalTopic = totalTopic;
	}
	public Integer getCompletedTopic() {
		return completedTopic;
	}
	public void setCompletedTopic(Integer completedTopic) {
		this.completedTopic = completedTopic;
	}
	public Integer getRemainingTopic() {
		return remainingTopic;
	}
	public void setRemainingTopic(Integer remainingTopic) {
		this.remainingTopic = remainingTopic;
	}
	public double getCompletedPercentage() {
		return completedPercentage;
	}
	public void setCompletedPercentage(double completedPercentage) {
		this.completedPercentage = completedPercentage;
	}
	public double getRemainingPercentage() {
		return remainingPercentage;
	}
	public void setRemainingPercentage(double remainingPercentage) {
		this.remainingPercentage = remainingPercentage;
	}
}

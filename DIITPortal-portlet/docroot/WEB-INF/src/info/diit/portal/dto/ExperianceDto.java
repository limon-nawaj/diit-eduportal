package info.diit.portal.dto;

import info.diit.portal.model.Experiance;

import java.io.Serializable;
import java.util.Date;

public class ExperianceDto implements Serializable{
	
	private long experianceId;
	
	
	private String organization;
	private String designation;
	private Date startDate;
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	private Date endDate;
	//private int currentStatus;
	private boolean isNew=true;
	
	
	
	public ExperianceDto(Experiance experiance) {
		
		this.experianceId = experiance.getExperianceId();
		this.organization = experiance.getOrganization();
		this.designation = experiance.getDesignation();
		this.startDate = experiance.getStartDate();
		this.endDate = experiance.getEndDate();
		//this.currentStatus = experiance.getCurrentStatus();
	}
	public ExperianceDto() {
		organization="";
		designation="";
		startDate=new Date();
		endDate=new Date();
		//currentStatus=0;
		
	}
	public long getExperianceId() {
		return experianceId;
	}
	public void setExperianceId(long experianceId) {
		this.experianceId = experianceId;
	}
	
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}

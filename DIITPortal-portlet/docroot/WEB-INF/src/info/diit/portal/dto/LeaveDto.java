package info.diit.portal.dto;

import java.util.Date;

public class LeaveDto {
	
	long id;
	EmployeeDto employee;
	Date startDate;
	Date endDate;
	Double totalDay;
	String comments;
	String status;
	EmployeeDto complementedBy;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public EmployeeDto getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDto employee) {
		this.employee = employee;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getTotalDay() {
		return totalDay;
	}
	public void setTotalDay(Double totalDay) {
		this.totalDay = totalDay;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public EmployeeDto getComplementedBy() {
		return complementedBy;
	}
	public void setComplementedBy(EmployeeDto complementedBy) {
		this.complementedBy = complementedBy;
	}
}

package info.diit.portal.dto;

import java.io.Serializable;

public class TeacherDto implements Serializable{
	
	private long teacherId;
	private String teacherName;
	public long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(long teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String toString(){
		return getTeacherName();
	}
}

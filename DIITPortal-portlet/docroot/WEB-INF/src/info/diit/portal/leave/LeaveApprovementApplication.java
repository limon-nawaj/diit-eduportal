package info.diit.portal.leave;

import info.diit.portal.NoSuchEmployeeException;
import info.diit.portal.dto.EmployeeDto;
import info.diit.portal.dto.LeaveDto;
import info.diit.portal.helper.DayHelper;
import info.diit.portal.model.Employee;
import info.diit.portal.model.Leave;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.LeaveLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class LeaveApprovementApplication extends Application implements PortletRequestListener {

	private final static String COLUMN_EMPLOYEE 	= "employee";
	private final static String COLUMN_START_DATE 	= "startDate";
	private final static String COLUMN_END_DATE 	= "endDate";
	private final static String COLUMN_TOTAL_DATE 	= "totalDay";
	private final static String COLUMN_COMMENTS 	= "comments";
	private final static String COLUMN_STATUS	 	= "status";
	
	private final static String DATE_FORMAT			= "dd/MM/yyyy";

	private Window window;
	private Employee employee;
	private LeaveDao leaveDao;
	private User user;
	private ThemeDisplay themeDisplay;
	private long organizationId;
	private DayHelper dayHelper;
	
	private Window popUpWindow;
	
	private List<Employee> employeeList;
	
    public void init() {
    	leaveDao = new LeaveDao();
    	dayHelper = new DayHelper();
		user = themeDisplay.getUser();
		try {
        	organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
        	employee = EmployeeLocalServiceUtil.findByUserId(user.getUserId());
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		employeeList = leaveDao.countAssistantEmployee(employee.getEmployeeId());
		window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
		if (employeeList.size()>0) {
			window.addComponent(mainLayout());
			loadLeave();
		}
    }
    
    private DateField startDateField;
    private DateField endDateField;
    private BeanItemContainer<LeaveDto> leaveContainer;
    private Table leaveTable;
    private VerticalLayout mainLayout;
    
    private VerticalLayout mainLayout(){
    	mainLayout = new VerticalLayout();
    	mainLayout.setWidth("100%");
    	mainLayout.setSpacing(true);
    	
    	startDateField = new DateField("Start Date");
    	endDateField = new DateField("End Date");
    	
    	startDateField.setDateFormat(DATE_FORMAT);
    	endDateField.setDateFormat(DATE_FORMAT);
    	
    	startDateField.setImmediate(true);
    	endDateField.setImmediate(true);
    	
    	Date firstDayOfMonth = dayHelper.getFirstDateOfMonth(new Date());
    	startDateField.setValue(firstDayOfMonth);
		
		Date endDayOfMonth = dayHelper.getEndDateOfMonth(new Date());
		endDateField.setValue(endDayOfMonth);
    	
    	HorizontalLayout dateLayout = new HorizontalLayout();
    	dateLayout.setWidth("100%");
    	dateLayout.setSpacing(true);
    	
    	dateLayout.addComponent(startDateField);
    	dateLayout.addComponent(endDateField);
    	
    	startDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date startDate 	= (Date) startDateField.getValue();
				Date endDate 	= (Date) endDateField.getValue();
				if (endDate!=null) {
					if (startDate.before(endDate)) {
						loadLeave();
					}else{
						loadLeave();
						window.showNotification("Invalid date", Window.Notification.TYPE_ERROR_MESSAGE);
						leaveTable.removeAllItems();
					}
				}
				
			}
		});
		
		endDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date startDate 	= (Date) startDateField.getValue();
				Date endDate 	= (Date) endDateField.getValue();
				if (startDate!=null) {
					if (endDate.after(startDate)) {
						loadLeave();
					} else {
						loadLeave();
						window.showNotification("Invalid date", Window.Notification.TYPE_ERROR_MESSAGE);
						leaveTable.removeAllItems();
					}
				}
			}
		});
    	
    	leaveContainer = new BeanItemContainer<LeaveDto>(LeaveDto.class);
		leaveTable = new Table("List of Leave", leaveContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if (property.getType()==Date.class) {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
					return simpleDateFormat.format(property.getValue());
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		
		leaveTable.setWidth("100%");
		leaveTable.setHeight("70%");
		leaveTable.setSelectable(true);
		
		leaveTable.setColumnHeader(COLUMN_EMPLOYEE, "Employee");
		leaveTable.setColumnHeader(COLUMN_START_DATE, "Start Date");
		leaveTable.setColumnHeader(COLUMN_END_DATE, "End Date");
		leaveTable.setColumnHeader(COLUMN_TOTAL_DATE, "Total Date");
		leaveTable.setColumnHeader(COLUMN_COMMENTS, "Comments");
		leaveTable.setColumnHeader(COLUMN_STATUS, "Status");
		
		leaveTable.setVisibleColumns(new String[]{COLUMN_EMPLOYEE, COLUMN_START_DATE, COLUMN_END_DATE, COLUMN_TOTAL_DATE, COLUMN_COMMENTS, COLUMN_STATUS});
		
		Button approveButton = new Button("Approve");
		approveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				popUpWindow = new Window(null, layout);
				
				LeaveDto selectedLeave = (LeaveDto) leaveTable.getValue();
				layout.addComponent(commentsLayout(selectedLeave.getId()));
				popUpWindow.setWidth("40%");
				popUpWindow.center();
				popUpWindow.setModal(true);
				getMainWindow().addWindow(popUpWindow);
			}
		});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.addComponent(approveButton);
		
		mainLayout.addComponent(dateLayout);
		mainLayout.addComponent(leaveTable);
		mainLayout.addComponent(rowLayout);
    	
    	return mainLayout;
    }
    
    private VerticalLayout commentsLayout;
    private TextArea commentArea;
    private Button acceptButton;
    private Button declineButton;
    private Button updateButton;
    
    private Leave leave;
    
//    0 for panding, 1 for approve, 2 for decline
    private VerticalLayout commentsLayout(long leaveId){
    	commentsLayout 	= new VerticalLayout();
    	commentArea 	= new TextArea("Comment");
    	acceptButton 	= new Button("Accept");
    	declineButton 	= new Button("Decline");
    	updateButton 	= new Button("Update");
    	
    	acceptButton.setImmediate(true);
    	declineButton.setImmediate(true);
    	updateButton.setImmediate(true);
    	
    	commentsLayout.setWidth("100%");
    	commentArea.setWidth("100%");
    	
		/*updateButton.setReadOnly(true);
		acceptButton.setReadOnly(true);
		declineButton.setReadOnly(true);*/
    	
    	try {
			leave = LeaveLocalServiceUtil.fetchLeave(leaveId);
	    	
			/*if (leave.getApplicationStatus()==0) {
				updateButton.setReadOnly(true);
			}
			if (leave.getApplicationStatus()==1) {
				acceptButton.setReadOnly(true);
			}
			if (leave.getApplicationStatus()==2) {
				declineButton.setReadOnly(true);
			}*/
	    	
	    	String com = leave.getComments();
	    	if (com!=null) {
	    		commentArea.setValue(com);
			}
	    	
	    	acceptButton.addListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					leave.setApplicationStatus(1);
					updateComments(leave);
				}
			});
	    	
	    	declineButton.addListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					leave.setApplicationStatus(2);
					updateComments(leave);
					
				}
			});
	    	
	    	updateButton.addListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					updateComments(leave);
				}
			});
    	} catch (SystemException e) {
			e.printStackTrace();
		}
    	
    	HorizontalLayout buttonLayout = new HorizontalLayout();
    	buttonLayout.setWidth("100%");
    	buttonLayout.setSpacing(true);
    	
    	Label spacerLabel = new Label();
    	buttonLayout.addComponent(spacerLabel);
    	buttonLayout.addComponent(acceptButton);
    	buttonLayout.addComponent(declineButton);
    	buttonLayout.addComponent(updateButton);
    	buttonLayout.setExpandRatio(spacerLabel, 1);
    	
    	commentsLayout.addComponent(commentArea);
    	commentsLayout.addComponent(buttonLayout);
    	return commentsLayout;
    }
    
    private void updateComments(Leave leave){
    	String comments = (String) commentArea.getValue();
		if (comments!=null) {
			leave.setComments(comments);
		}
		
		try {
			Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
			leave.setComplementedBy(employee.getEmployeeId());
			LeaveLocalServiceUtil.updateLeave(leave);
			loadLeave();
			popUpWindow.setVisible(false);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NoSuchEmployeeException e) {
			e.printStackTrace();
		}
    }
    
    private void loadLeave(){
    	leaveContainer.removeAllItems();
    	if (employeeList!=null) {
    		Date startDate = (Date) startDateField.getValue();
    		startDate.setHours(0);
    		startDate.setMinutes(0);
    		startDate.setSeconds(0);
    		Date endDate = (Date) endDateField.getValue();
    		endDate.setHours(0);
    		endDate.setMinutes(0);
    		endDate.setSeconds(0);
    		
			for (Employee employee : employeeList) {
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Leave.class);
	    		Criterion criterion = RestrictionsFactoryUtil.between("startDate", startDate, endDate);
	    		dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("employee").eq(employee.getEmployeeId()));
	    		try {
					List<Leave> leaves = LeaveLocalServiceUtil.dynamicQuery(dynamicQuery);
					if (leaves!=null) {
						Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employee.getEmployeeId());
						EmployeeDto empDto = new EmployeeDto();
						empDto.setId(emp.getEmployeeId());
						empDto.setName(emp.getName());
						
						for (Leave leave : leaves) {
							LeaveDto leaveDto = new LeaveDto();
							
							leaveDto.setId(leave.getLeaveId());
							leaveDto.setEmployee(empDto);
							leaveDto.setStartDate(leave.getStartDate());
							leaveDto.setEndDate(leave.getEndDate());
							leaveDto.setTotalDay(leave.getNumberOfDay());
							leaveDto.setComments(leave.getComments());
							if (leave.getApplicationStatus()==0) {
								leaveDto.setStatus("Pending");
							}else if (leave.getApplicationStatus()==1) {
								leaveDto.setStatus("Accepted");
							}else if (leave.getApplicationStatus()==2) {
								leaveDto.setStatus("Decline");
							}else if (leave.getApplicationStatus()==3) {
								leaveDto.setStatus("Withdraw");
							}
							
							leaveContainer.addBean(leaveDto);
						}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		}
    }
    
	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
}

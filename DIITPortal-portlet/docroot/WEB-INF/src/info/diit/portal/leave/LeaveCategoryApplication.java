package info.diit.portal.leave;

import info.diit.portal.dto.LeaveCategoryDto;
import info.diit.portal.model.LeaveCategory;
import info.diit.portal.model.impl.LeaveCategoryImpl;
import info.diit.portal.service.LeaveCategoryLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class LeaveCategoryApplication extends Application implements PortletRequestListener {

	private final static String COLUMN_TITLE 		= "categoryTitle";
	private final static String COLUMN_TOTAL_LEAVE 	= "totalLeave";
	
	private ThemeDisplay themeDisplay;
	Window window;
	
	private long organizationId;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        try {
        	organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
        window.addComponent(mainLayout());
        loadCategory();
    }
    
    private TextField titleField;
    private NumericField totalLeaveField;
    private Button 	  saveButton;
    private BeanItemContainer<LeaveCategoryDto> categoryContainer;
    private Table categoryTable;
    
    private LeaveCategory leaveCategory;
    
    private LeaveDao leaveDao = new LeaveDao();
    
    private HorizontalLayout mainLayout(){
    	HorizontalLayout mainLayout = new HorizontalLayout();
    	mainLayout.setSpacing(true);
    	
    	VerticalLayout formLayout 	= new VerticalLayout();
    	formLayout.setSpacing(true);
    	
    	titleField 			= new TextField("Category Title");
    	titleField.setRequired(true);
    	totalLeaveField 	= new NumericField("Total Leave");
    	totalLeaveField.setNumberType(NumericFieldType.INTEGER);
    	totalLeaveField.setRequired(true);
    	saveButton 			= new Button("Save");
    	
    	formLayout.addComponent(titleField);
    	formLayout.addComponent(totalLeaveField);
    	formLayout.addComponent(saveButton);
    	
    	categoryContainer 	= new BeanItemContainer<LeaveCategoryDto>(LeaveCategoryDto.class);
    	categoryTable 		= new Table("Type of Leave", categoryContainer);
    	
    	mainLayout.setWidth("100%");
    	formLayout.setWidth("100%");
    	titleField.setWidth("70%");
    	totalLeaveField.setWidth("70%");
    	categoryTable.setWidth("100%");
    	
    	categoryTable.setColumnHeader(COLUMN_TITLE, "Title");
    	categoryTable.setColumnHeader(COLUMN_TOTAL_LEAVE, "Total Leave");
    	
    	categoryTable.setVisibleColumns(new String[]{COLUMN_TITLE, COLUMN_TOTAL_LEAVE});
    	
    	saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (leaveCategory == null) {
					leaveCategory = new LeaveCategoryImpl();
					leaveCategory.setNew(true);
				}
				
				try {
					leaveCategory.setCompanyId(themeDisplay.getCompanyId());
					leaveCategory.setOrganizationId(organizationId);
					leaveCategory.setUserId(themeDisplay.getUserId());
					leaveCategory.setUserName(themeDisplay.getUser().getFullName());
					
					String title = titleField.getValue().toString();
					if (title!=null && title!="") {
						leaveCategory.setTitle(title.trim());
					} else {
						window.showNotification("Title cannot be empty!", Window.Notification.TYPE_ERROR_MESSAGE);
						return;
					}
					
					Integer totalLeave = Integer.parseInt(totalLeaveField.getValue().toString());
					if (totalLeave!=null) {
						leaveCategory.setTotalLeave(totalLeave);
					} else {
						window.showNotification("Total leave cannot be empty!", Window.Notification.TYPE_ERROR_MESSAGE);
						return;
					}
					
					if (leaveCategory.isNew()) {
						LeaveCategoryLocalServiceUtil.addLeaveCategory(leaveCategory);
						window.showNotification("Saved successfully");
						loadCategory();
					} else {
						LeaveCategoryLocalServiceUtil.updateLeaveCategory(leaveCategory);
						window.showNotification("Updated successfully");
						loadCategory();
					}
					
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
    	
    	mainLayout.addComponent(formLayout);
    	mainLayout.addComponent(categoryTable);
    	return mainLayout;
    }
    
    private void loadCategory(){
    	categoryContainer.removeAllItems();
    	List<LeaveCategoryDto> categoryList = leaveDao.getCategory(organizationId);
    	if (categoryList!=null) {
			for (LeaveCategoryDto category : categoryList) {
				categoryContainer.addBean(category);
			}
		}
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

package info.diit.portal.accounts;

import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.FeesDto;
import info.diit.portal.dto.ScheduleDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchFee;
import info.diit.portal.model.BatchPaymentSchedule;
import info.diit.portal.model.FeeType;
import info.diit.portal.service.BatchFeeLocalServiceUtil;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchPaymentScheduleLocalServiceUtil;
import info.diit.portal.service.FeeTypeLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

public class AccountsDao {

	public static ArrayList<BatchDto> getBatchListByCourse(long courseId) throws SystemException
	{
		List<Batch> batchList = BatchLocalServiceUtil.findBatchByCourseId(courseId);
		ArrayList<BatchDto> batchDtoList = new ArrayList<BatchDto>(batchList.size());
		
		for(Batch batch:batchList)
		{
			BatchDto batchDto = new BatchDto();
			batchDto.setBatchId(batch.getBatchId());
			batchDto.setBatchName(batch.getBatchName());
			
			batchDtoList.add(batchDto);
		}
		
		return batchDtoList;
	}
	
	public static ArrayList<FeesDto> getBatchFeesList(long batchId) throws SystemException, PortalException
	{
		List<BatchFee> batchFeeList = BatchFeeLocalServiceUtil.getBatchFeesList(batchId);
		ArrayList<FeesDto> batchFees = new ArrayList<FeesDto>(batchFeeList.size());
		
		for(BatchFee batchFee:batchFeeList)
		{
			FeesDto feesDto = new FeesDto();
			feesDto.setAmount(Math.round(batchFee.getAmount()));
			feesDto.setDescription(feesDto.getDescription());
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(batchFee.getFeeTypeId());
			
			if(feeType!=null)
			{
				feesDto.setFeeType(feeType.getTypeName());
				feesDto.setId(feeType.getFeeTypeId());
			}
			
			batchFees.add(feesDto);
			
		}
		
		return batchFees;
		
	}
	
	public static ArrayList<ScheduleDto> getBatchPaymentSchedules(long batchId) throws SystemException, PortalException
	{
		ArrayList<ScheduleDto> batchPaymentScheduleDtos = new ArrayList<ScheduleDto>();
		
		List<BatchPaymentSchedule> batchPaymentSchedules = BatchPaymentScheduleLocalServiceUtil.findBatchPaymentScheduleByBatch(batchId);
		
		for(BatchPaymentSchedule batchPaymentSchedule:batchPaymentSchedules)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			scheduleDto.setAmount(Math.round(batchPaymentSchedule.getAmount()));
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(batchPaymentSchedule.getFeeTypeId());
			
			if(feeType!=null)
			{
				scheduleDto.setFeeType(feeType.getTypeName());
				scheduleDto.setId(feeType.getFeeTypeId());
			}
			
			scheduleDto.setScheduleDate(batchPaymentSchedule.getScheduleDate());
			
			batchPaymentScheduleDtos.add(scheduleDto);
		}
		
		return batchPaymentScheduleDtos;
	}
	
}

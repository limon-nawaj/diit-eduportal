package info.diit.portal.accounts;

import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.FeesDto;
import info.diit.portal.dto.ScheduleDto;
import info.diit.portal.model.BatchFee;
import info.diit.portal.model.BatchPaymentSchedule;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseFee;
import info.diit.portal.model.FeeType;
import info.diit.portal.model.impl.BatchFeeImpl;
import info.diit.portal.model.impl.BatchPaymentScheduleImpl;
import info.diit.portal.model.impl.CourseFeeImpl;
import info.diit.portal.model.impl.FeeTypeImpl;
import info.diit.portal.service.BatchFeeLocalServiceUtil;
import info.diit.portal.service.CourseFeeLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.FeeTypeLocalServiceUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

public class AccountsSetupApp extends Application implements PortletRequestListener{

	private ThemeDisplay 									themeDisplay;
	private Window 											window;
	
	private TabSheet 										accountsSetupTabSheet;
	
	//componets for fee types tab
	private TextField 										feeTypeNameField;
	private TextArea 										feeTypeDescriptionField;
	private Table 											feeTypesTable;
	
	private static final int 								TOTAL_SCHEDULE_ROWS_COUNT = 50;
	
	private static final String 							COLUMN_FEE_TYPE = "feeType";
	private static final String 							COLUMN_DESCRIPTION = "description";
	private static final String 							COLUMN_AMOUNT = "amount";
	private static final String 							COLUMN_SCHEDULE_DATE = "scheduleDate";
	
	private BeanItemContainer<FeesDto> 						feeTypesBeanItemContainer;
	
	//components for course fee setup
	private ListSelect 										courseListSelect;
	private Table 											courseFeeTable;
	private BeanItemContainer<FeesDto> 						courseFeeBeanItemContainer;
	
	
	//components for batch fee setup
	private ComboBox 										courseComboBox;
	private ComboBox 										batchComboBox;
	private CheckBox 										editBatchFeesCheckBox;
	private Table 											batchFeeTable;
	private BeanItemContainer<FeesDto> 						batchFeeBeanItemContainer;
	private Table 											batchPaymentScheduleTable;
	private BeanItemContainer<ScheduleDto> 					batchPaymentScheduleBeanItemContainer;
	
	
	//components for Student setup
	private NumericField 									studentIdField;
	private TextField 										nameField;
	private ComboBox 										studentBatchComboBox;
	private NumericField 									scholarshipField;
	private NumericField 									discountField;
	private Table 											studentFeeTable;
	private BeanItemContainer<FeesDto> 						studentFeeBeanItemContainer;
	private Table 											studentPaymentScheduleTable;
	private BeanItemContainer<ScheduleDto> 					studentPaymentScheduleBeanItemContainer;
	
	//data
	private List<CourseDto> 								courseList;
	
	public final static String 								DATE_FORMAT = "dd/MM/yyyy";

	public void init() {
		window 										= new Window();
		window										.setSizeFull();
		setMainWindow(window);
		
		loadCourseList();
		loadFeeTypesContainer();
		
		VerticalLayout verticalLayout 				= new VerticalLayout();
		verticalLayout								.setSizeFull();
		verticalLayout								.setSpacing(true);

		verticalLayout								.addComponent(initAccountsSetupTabSheet());
		
		window										.addComponent(verticalLayout);
		
	}

	private TabSheet initAccountsSetupTabSheet()
	{
		accountsSetupTabSheet = new TabSheet();
		accountsSetupTabSheet.setWidth("100%");
		
		accountsSetupTabSheet.addTab(initCourseFeeSetupLayout(),"Course");
		accountsSetupTabSheet.addTab(initBatchFeeSetupLayout(),"Batch");
		accountsSetupTabSheet.addTab(initStudentFeeSetupLayout(),"Student");
		accountsSetupTabSheet.addTab(initFeeTypesLayout(),"Fee Types");
		
		
		return accountsSetupTabSheet;

	}
	
	private GridLayout initFeeTypesLayout()
	{
		feeTypeNameField = new TextField("Fee Type");
		feeTypeNameField.setWidth("100%");
		feeTypeNameField.setRequired(true);
		
		feeTypeDescriptionField = new TextArea("Description");
		feeTypeDescriptionField.setWidth("100%");
		
		feeTypesTable = new Table("Fee Types",feeTypesBeanItemContainer);
		feeTypesTable.setWidth("100%");
		feeTypesTable.setPageLength(10);
		feeTypesTable.setColumnHeader(COLUMN_FEE_TYPE, "Name");
		feeTypesTable.setSelectable(true);
		feeTypesTable.setImmediate(true);
		
		feeTypesTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_DESCRIPTION});
		feeTypesTable.setColumnHeader(COLUMN_FEE_TYPE, "Name");
		feeTypesTable.setColumnHeader(COLUMN_DESCRIPTION, "Description");
		
		Button saveButton = new Button("Save");
		Button deleteButton = new Button("Delete");
		Button clearButton = new Button("Clear");
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		horizontalLayout.addComponent(deleteButton);
		horizontalLayout.addComponent(clearButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(3,5);
		gridLayout.setSpacing(true);
		gridLayout.setWidth("100%");
		
		gridLayout.addComponent(feeTypeNameField,0,0,0,0);
		gridLayout.addComponent(feeTypesTable,1,0,2,4);
		gridLayout.addComponent(feeTypeDescriptionField,0,1,0,2);
		gridLayout.addComponent(horizontalLayout,0,3,0,3);
		

		feeTypesTable.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				FeesDto accountsDto = (FeesDto) feeTypesTable.getValue();
				
				if(accountsDto!=null)
				{
					feeTypeNameField.setValue(accountsDto.getFeeType());
					feeTypeDescriptionField.setValue(accountsDto.getDescription());
				}
				else
				{
					feeTypeNameField.setValue("");
					feeTypeDescriptionField.setValue("");
				}
				
			}
		});
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				boolean isNew = true;
				FeesDto accountsDto = (FeesDto)feeTypesTable.getValue();
				
				FeeType feeType = null;
				
				if(accountsDto==null)
				{
					feeType = new FeeTypeImpl();
				}
				else
				{
					isNew = false;
					long feeTypeId = accountsDto.getId();
					try {
						feeType = FeeTypeLocalServiceUtil.fetchFeeType(feeTypeId);
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
				}
					
				if(feeTypeNameField.isValid() || !feeTypeNameField.getValue().toString().trim().equals(""))
				{
					feeType.setTypeName(feeTypeNameField.getValue().toString());
					
				}
				else
				{
					window.showNotification("Name cannot be blank!");
					return;
				}
				
				Object description = feeTypeDescriptionField.getValue();
				
				if(description!=null)
				{
					feeType.setDescription(description.toString());
				}
					
					
				if(!isFeeTypeDuplicate(feeType))
				{
					try
					{
						feeType = FeeTypeLocalServiceUtil.updateFeeType(feeType);
						
						if(isNew)
						{
							feeTypesBeanItemContainer.addBean(convertFeeTypeModel(feeType));
							
						}
						
						feeTypesTable.refreshRowCache();
						
						window.showNotification("Fee Type Saved!");
						clearFeeTypesForm();
					}
					catch (SystemException e)
					{
						window.showNotification("Cannot save the Fee Type for a System Error!");
						e.printStackTrace();
					}
						
				}
				else
				{
					window.showNotification("'"+feeType.getTypeName()+"' already exists!");
				}
				
			}
		});
		
		clearButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				clearFeeTypesForm();
				
			}
		});
		
		return gridLayout;
	}
	
	private GridLayout initCourseFeeSetupLayout()
	{
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setSpacing(true);
		gridLayout.setSizeFull();
		
		
		courseListSelect = new ListSelect("Courses");
		courseListSelect.setWidth("100%");
		courseListSelect.setHeight("100%");
		courseListSelect.setNullSelectionAllowed(false);
		courseListSelect.setImmediate(true);
		
		
		for(CourseDto courseDto:courseList)
		{
			courseListSelect.addItem(courseDto);
		}
		
		courseListSelect.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				CourseDto courseDto = (CourseDto) courseListSelect.getValue();
				
				if(courseDto!=null)
				{
					loadCourseFeeContainer();
				}
				
			}
		});
		
		courseFeeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
		courseFeeTable = new Table("Fees",courseFeeBeanItemContainer);
		courseFeeTable.setWidth("100%");
		courseFeeTable.setPageLength(6);
		courseFeeTable.setImmediate(true);
		courseFeeTable.setFooterVisible(true);
		
		courseFeeTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		courseFeeTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		courseFeeTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);

		courseFeeTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		courseFeeTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {

				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.addStyleName("align-right");
					field.setReadOnly(true);
					
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							courseFeeTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalCourseFee()));
							
						}
					});
					
					return field;
					
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		
		courseFeeTable.setEditable(true);
		
		//select the first value from the course list
		if(courseList!=null && courseList.size()>0)
		{
			CourseDto courseDto = courseList.get(0);
			courseListSelect.setValue(courseDto);
		}
		
		Button saveButton = new Button("Save");
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				CourseDto selectedCourse = (CourseDto)courseListSelect.getValue();
				
				if(selectedCourse!=null)
				{
					try {
						CourseFeeLocalServiceUtil.deleteAllCourseFees(selectedCourse.getCourseId());
						Collection<FeesDto> feesItems = courseFeeBeanItemContainer.getItemIds();

						Iterator<FeesDto> iterator = feesItems.iterator();						
						
						while(iterator.hasNext())
						{
							FeesDto accountsDto = iterator.next();
							
							if(accountsDto.getAmount()!=null)
							{
								CourseFee courseFee = new CourseFeeImpl();
								
								courseFee.setAmount(accountsDto.getAmount());
								courseFee.setCompanyId(themeDisplay.getCompanyId());
								courseFee.setCourseId(selectedCourse.getCourseId());
								courseFee.setCreateDate(new Date());
								courseFee.setFeeTypeId(accountsDto.getId());
								courseFee.setUserId(themeDisplay.getUserId());
								
								courseFee = CourseFeeLocalServiceUtil.addCourseFee(courseFee);
							}
						}
						window.showNotification("Course Fees Saved!");
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
				else
				{
					window.showNotification("No Course is selected !");
				}
			}
		});
		
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);
		
		gridLayout.addComponent(courseListSelect,0,0,0,0);
		gridLayout.addComponent(courseFeeTable,1,0,1,0);
		gridLayout.addComponent(horizontalLayout,1,1,1,1);
		
		loadCourseFeeContainer();
		
		return gridLayout;
	}

	private GridLayout initBatchFeeSetupLayout()
	{

		courseComboBox = new ComboBox("Course");
		courseComboBox.setImmediate(true);
		courseComboBox.setWidth("100%");
		
		for(CourseDto courseDto:courseList)
		{
			courseComboBox.addItem(courseDto);
		}

		
		batchComboBox = new ComboBox("Batch");
		batchComboBox.setImmediate(true);
		batchComboBox.setWidth("100%");
		
		
		editBatchFeesCheckBox = new CheckBox("Edit Batch Fees Information");
		editBatchFeesCheckBox.setWidth("100%");
		editBatchFeesCheckBox.setEnabled(false);
		editBatchFeesCheckBox.setImmediate(true);
		
		batchFeeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
		batchFeeTable = new Table("Total Fees",batchFeeBeanItemContainer);
		batchFeeTable.setWidth("100%");
		batchFeeTable.setPageLength(8);
		
		batchFeeTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		batchFeeTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		batchFeeTable.setFooterVisible(true);

		batchFeeTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		batchFeeTable.setColumnAlignment(COLUMN_FEE_TYPE, Table.ALIGN_RIGHT);
		batchFeeTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		
		batchFeeTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					field.addStyleName("align-right");
					
					return field;
				}
				else if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullSettingAllowed(true);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							batchFeeTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalBatchFee()));
							
						}
					});

					
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		batchPaymentScheduleBeanItemContainer = new BeanItemContainer<ScheduleDto>(ScheduleDto.class);
		batchPaymentScheduleTable = new Table("Payment Schedule",batchPaymentScheduleBeanItemContainer);
		batchPaymentScheduleTable.setEditable(true);
		batchPaymentScheduleTable.setWidth("100%");
		batchPaymentScheduleTable.setPageLength(10);
		
		batchPaymentScheduleTable.setColumnHeader(COLUMN_SCHEDULE_DATE, "Date");
		batchPaymentScheduleTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		batchPaymentScheduleTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		batchPaymentScheduleTable.setColumnExpandRatio(COLUMN_SCHEDULE_DATE, 1);
		batchPaymentScheduleTable.setColumnExpandRatio(COLUMN_FEE_TYPE, 1);
		batchPaymentScheduleTable.setColumnExpandRatio(COLUMN_AMOUNT, 1);
		
		batchPaymentScheduleTable.setVisibleColumns(new String[]{COLUMN_SCHEDULE_DATE,COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		batchPaymentScheduleTable.setFooterVisible(true);
		batchPaymentScheduleTable.setColumnFooter(COLUMN_FEE_TYPE, "Total");
		batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, "0");
		
		batchPaymentScheduleTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		batchPaymentScheduleTable.setColumnAlignment(COLUMN_FEE_TYPE, Table.ALIGN_RIGHT);
		
		batchPaymentScheduleTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if(propertyId.equals(COLUMN_SCHEDULE_DATE))
				{
					DateField field = new DateField();
					field.setWidth("100%");
					field.setResolution(DateField.RESOLUTION_DAY);
					field.setDateFormat(DATE_FORMAT);
					field.setImmediate(true);
					
					return field;

				}
				
				else if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					ComboBox field = new ComboBox();
					field.setWidth("100%");
					
					Collection<FeesDto> feesDtoCollection = feeTypesBeanItemContainer.getItemIds();
					for(FeesDto feesDto:feesDtoCollection)
					{
						field.addItem(feesDto.getFeeType());
					}
					
					field.setImmediate(true);
					return field;
				}
				
				else if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalBatchPaymentScheduleAmount()));
							
						}
					});
					
					return field;
					
				}
				
				else
				{
					return super.createField(container, itemId, propertyId, uiContext);
				}
			}
		});
		
		//create 50 rows for the batch schedule table
		for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT;i++)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			batchPaymentScheduleBeanItemContainer.addBean(scheduleDto);
		}
		
		batchPaymentScheduleTable.refreshRowCache();
		
		Button saveButton = new Button("Save");
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(5,5);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(courseComboBox,0,0,1,0);
		gridLayout.addComponent(batchComboBox,0,1,1,1);
		gridLayout.addComponent(editBatchFeesCheckBox,0,2,1,2);
		gridLayout.addComponent(batchFeeTable,0,3,1,3);
		gridLayout.addComponent(batchPaymentScheduleTable,2,0,4,3);
		gridLayout.addComponent(horizontalLayout,1,4,4,4);
		
		courseComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				batchComboBox.removeAllItems();
				
				CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
				
				if(selectedCourse!=null)
				{
					try {
						
						ArrayList<BatchDto> batchList = AccountsDao.getBatchListByCourse(selectedCourse.getCourseId());
						for(BatchDto batch:batchList)
						{
							batchComboBox.addItem(batch);
						}
						
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		
		editBatchFeesCheckBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
				if(selectedCourse!=null)
				{
					boolean selected = (Boolean)editBatchFeesCheckBox.getValue();
					if(selected)
					{	
						batchFeeTable.setEditable(true);
					}
					else
					{
						batchFeeTable.setEditable(false);
					}
				}
			}
		});
		
		batchComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				BatchDto batch = (BatchDto)batchComboBox.getValue();
				if(batch!=null)
				{
					editBatchFeesCheckBox.setValue(false);
					editBatchFeesCheckBox.setEnabled(true);
					
					loadBatchFees(batch.getBatchId());

					if(batchFeeBeanItemContainer.size()==0)
					{
						CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
						if(selectedCourse!=null)
						{
							loadFeeFromCourse(selectedCourse.getCourseId());
						}
					}
					loadBatchPaymentSchedule(batch.getBatchId());
				}
				else
				{
					editBatchFeesCheckBox.setValue(false);
					editBatchFeesCheckBox.setEnabled(false);
					batchFeeBeanItemContainer.removeAllItems();
					batchFeeTable.setColumnFooter(COLUMN_AMOUNT, "0");
					batchFeeTable.refreshRowCache();
					
					batchPaymentScheduleBeanItemContainer.removeAllItems();
					for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT;i++)
					{
						ScheduleDto scheduleDto = new ScheduleDto();
						batchPaymentScheduleBeanItemContainer.addBean(scheduleDto);
					}
					batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, "0");
					batchPaymentScheduleTable.refreshRowCache();
				}
			}
		});
		
		
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				
				BatchDto selectedBatch = (BatchDto)batchComboBox.getValue();
				if(selectedBatch!=null)
				{
					ArrayList<BatchFee> batchFeeList = new ArrayList<BatchFee>();
					
					for(int i=0;i<batchFeeBeanItemContainer.size();i++)
					{
						FeesDto feesDto = batchFeeBeanItemContainer.getIdByIndex(i);
						
						BatchFee batchFee = new BatchFeeImpl();
						batchFee.setAmount(feesDto.getAmount());
						batchFee.setBatchId(selectedBatch.getBatchId());
						
						batchFee.setCompanyId(themeDisplay.getCompanyId());
						batchFee.setCreateDate(new Date());
						batchFee.setFeeTypeId(feesDto.getId());
						batchFee.setModifiedDate(new Date());
						batchFee.setUserId(themeDisplay.getUserId());
						
						batchFeeList.add(batchFee);
						
					}
					
					//add the payment schedules
					
					ArrayList<BatchPaymentSchedule> batchPaymentSchedules = new ArrayList<BatchPaymentSchedule>();
					
					for(int i=0;i<batchPaymentScheduleBeanItemContainer.size();i++)
					{
						ScheduleDto schedule = batchPaymentScheduleBeanItemContainer.getIdByIndex(i);
						
						if(schedule.getAmount()!= null && schedule.getFeeType() != null && schedule.getScheduleDate()!=null)
						{
							try {
								List<FeeType> feeTypes = FeeTypeLocalServiceUtil.findByFeeTypeName(schedule.getFeeType());
								if(feeTypes!=null && feeTypes.size()>0)
								{
									schedule.setId(feeTypes.get(0).getFeeTypeId());
								}
							} catch (SystemException e) {
								e.printStackTrace();
							}
							BatchPaymentSchedule batchPaymentSchedule = new BatchPaymentScheduleImpl();
							batchPaymentSchedule.setAmount(schedule.getAmount());
							batchPaymentSchedule.setBatchId(selectedBatch.getBatchId());
							batchPaymentSchedule.setCompanyId(themeDisplay.getCompanyId());
							batchPaymentSchedule.setCreateDate(new Date());
							batchPaymentSchedule.setFeeTypeId(schedule.getId());
							batchPaymentSchedule.setModifiedDate(new Date());
							batchPaymentSchedule.setScheduleDate(schedule.getScheduleDate());
							batchPaymentSchedule.setUserId(themeDisplay.getUserId());
							
							batchPaymentSchedules.add(batchPaymentSchedule);
						}
					}
					
					try {
						BatchFeeLocalServiceUtil.updateBatchFees(selectedBatch.getBatchId(),batchFeeList,batchPaymentSchedules);
						window.showNotification("Batch Fees information saved.");
					} catch (SystemException e) {
						e.printStackTrace();
						window.showNotification("Internal Error! Cannot save Batch Fees information",Notification.TYPE_ERROR_MESSAGE);
					}
				}
				else
				{
					window.showNotification("Please select a batch", Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
			}
		});

		
		return gridLayout;
	}
	
	private GridLayout initStudentFeeSetupLayout()
	{
		studentIdField = new NumericField("Student ID");
		studentIdField.setAllowNegative(false);
		studentIdField.setImmediate(true);
		studentIdField.setNullRepresentation("");
		studentIdField.setNumberType(NumericFieldType.INTEGER);
		studentIdField.setWidth("100%");
		
		nameField = new TextField("Student Name");
		nameField.setReadOnly(true);
		nameField.setWidth("100%");
		
		studentBatchComboBox = new ComboBox("Batch");
		studentBatchComboBox.setImmediate(true);
		studentBatchComboBox.setWidth("100%");
		
		scholarshipField = new NumericField("Scholarships");
		scholarshipField.setWidth("100%");
		scholarshipField.setNullRepresentation("");
		scholarshipField.setNumberType(NumericFieldType.INTEGER);

		discountField = new NumericField("Discount");
		discountField.setWidth("100%");
		discountField.setNullRepresentation("");
		discountField.setNumberType(NumericFieldType.INTEGER);
		
		
		studentFeeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
		studentFeeTable = new Table("Total Fees",studentFeeBeanItemContainer);
		studentFeeTable.setWidth("100%");
		studentFeeTable.setPageLength(8);
		
		studentFeeTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		studentFeeTable.setColumnHeader(COLUMN_AMOUNT, "Amount");

		studentFeeTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		studentPaymentScheduleBeanItemContainer = new BeanItemContainer<ScheduleDto>(ScheduleDto.class);
		studentPaymentScheduleTable = new Table("Payment Schedule",studentPaymentScheduleBeanItemContainer);
		studentPaymentScheduleTable.setEditable(true);
		studentPaymentScheduleTable.setWidth("100%");
		studentPaymentScheduleTable.setPageLength(10);
		
		studentPaymentScheduleTable.setColumnHeader(COLUMN_SCHEDULE_DATE, "Date");
		studentPaymentScheduleTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		studentPaymentScheduleTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		studentPaymentScheduleTable.setColumnExpandRatio(COLUMN_SCHEDULE_DATE, 1);
		studentPaymentScheduleTable.setColumnExpandRatio(COLUMN_FEE_TYPE, 1);
		studentPaymentScheduleTable.setColumnExpandRatio(COLUMN_AMOUNT, 1);
		
		studentPaymentScheduleTable.setVisibleColumns(new String[]{COLUMN_SCHEDULE_DATE,COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		studentPaymentScheduleTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if(propertyId.equals(COLUMN_SCHEDULE_DATE))
				{
					DateField field = new DateField();
					field.setWidth("100%");
					field.setResolution(DateField.RESOLUTION_DAY);
					field.setDateFormat(DATE_FORMAT);
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					ComboBox field = new ComboBox();
					field.setWidth("100%");
					
					Collection<FeesDto> feesDtoCollection = feeTypesBeanItemContainer.getItemIds();
					for(FeesDto feesDto:feesDtoCollection)
					{
						field.addItem(feesDto);
					}
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		//create 50 rows for the batch schedule table
		for(int i=1;i<=50;i++)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			studentPaymentScheduleBeanItemContainer.addBean(scheduleDto);
		}
		
		studentPaymentScheduleTable.refreshRowCache();
		
		Button saveButton = new Button("Save");
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(5,5);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(studentIdField,0,0);
		gridLayout.addComponent(studentBatchComboBox,1,0);
		gridLayout.addComponent(nameField,0,1,1,1);
		gridLayout.addComponent(scholarshipField,0,2);
		gridLayout.addComponent(discountField,1,2);
		gridLayout.addComponent(studentFeeTable,0,3,1,3);
		gridLayout.addComponent(studentPaymentScheduleTable,2,0,4,3);
		gridLayout.addComponent(horizontalLayout,1,4,4,4);
		
		return gridLayout;
	}

	
	private void loadFeeTypesContainer() 
	{
		try
		{
			feeTypesBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
			List<FeeType> feeTypesList = FeeTypeLocalServiceUtil.getFeeTypes(0, FeeTypeLocalServiceUtil.getFeeTypesCount());
			
			for(FeeType feeType:feeTypesList)
			{
				FeesDto accountsDto = new FeesDto();
				accountsDto.setId(feeType.getFeeTypeId());
				accountsDto.setFeeType(feeType.getTypeName());
				accountsDto.setDescription(feeType.getDescription());
				
				feeTypesBeanItemContainer.addBean(accountsDto);
				
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadCourseFeeContainer()
	{
		courseFeeBeanItemContainer.removeAllItems();
		CourseDto selectedCourse = (CourseDto)courseListSelect.getValue();
		long courseId = 0;
		
		if(selectedCourse!=null)
		{
			courseId = selectedCourse.getCourseId();
		}
		
		try {
			List<CourseFee> courseFeeList = CourseFeeLocalServiceUtil.getCourseFeeList(courseId);
			
			long totalFee = 0;
			
			for(CourseFee courseFee:courseFeeList)
			{
				FeesDto accountsDto = new FeesDto();
				accountsDto.setId(courseFee.getFeeTypeId());
				accountsDto.setFeeType(FeeTypeLocalServiceUtil.fetchFeeType(courseFee.getFeeTypeId()).getTypeName());
				accountsDto.setAmount(Math.round(courseFee.getAmount()));
				totalFee += accountsDto.getAmount();
				courseFeeBeanItemContainer.addBean(accountsDto);
			}
			
			//other fee types not setup
			List<FeeType> feeTypesList = FeeTypeLocalServiceUtil.getFeeTypes(0, FeeTypeLocalServiceUtil.getFeeTypesCount());
			
			for(FeeType feeType:feeTypesList)
			{
				Collection<FeesDto> existingAccounts = courseFeeBeanItemContainer.getItemIds();
				Iterator<FeesDto> iterator = existingAccounts.iterator();
				
				boolean exist = false;
				while(iterator.hasNext())
				{
					if(feeType.getFeeTypeId()==iterator.next().getId())
					{
						exist = true;
						break;
					}
				}
				
				if(!exist)
				{
					FeesDto accountsDto = new FeesDto();
					accountsDto.setId(feeType.getFeeTypeId());
					accountsDto.setFeeType(feeType.getTypeName());
				
					courseFeeBeanItemContainer.addBean(accountsDto);
				}
			}
			
			courseFeeTable.refreshRowCache();
			courseFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			courseFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void clearFeeTypesForm()
	{
		feeTypeNameField.setValue("");
		feeTypeDescriptionField.setValue("");
		feeTypesTable.setValue(null);
	}
	
	private FeesDto convertFeeTypeModel(FeeType feeType)
	{
		FeesDto accountsDto = new FeesDto();
		accountsDto.setId(feeType.getFeeTypeId());
		accountsDto.setFeeType(feeType.getTypeName());
		accountsDto.setDescription(feeType.getDescription());
		
		return accountsDto;
	}
	
	public static boolean isFeeTypeDuplicate(FeeType feeType)
	{
		boolean exist = false;
		try {
			List<FeeType> feeTypes= FeeTypeLocalServiceUtil.findByFeeTypeName(feeType.getTypeName());
			
			for(FeeType oldFeeType:feeTypes)
			{
				if(feeType.getFeeTypeId()!=oldFeeType.getFeeTypeId() && feeType.getTypeName().equalsIgnoreCase(oldFeeType.getTypeName()))
				{
					exist = true;
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return exist;
	}
	
	public void loadCourseList()
	{
		try {
			
			List<Course> courses = CourseLocalServiceUtil.getCourses(0, CourseLocalServiceUtil.getCoursesCount());
			
			if(courseList==null)
			{
				courseList = new ArrayList<CourseDto>(courses.size());
			}
			else
			{
				courseList.clear();
			}
			
			for(Course course:courses)
			{
				CourseDto courseDto = new CourseDto();
				courseDto.setCourseId(course.getCourseId());
				courseDto.setCourseCode(course.getCourseCode());
				courseDto.setCourseName(course.getCourseName());
				
				courseList.add(courseDto);
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	

	public void loadFeeFromCourse(long courseId)
	{
		try
		{
			batchFeeBeanItemContainer.removeAllItems();
			List<CourseFee> courseFeeList = CourseFeeLocalServiceUtil.getCourseFeeList(courseId);
			
			long totalFee = 0;
			
			for(CourseFee courseFee:courseFeeList)
			{
				FeesDto feesDto = new FeesDto();
				feesDto.setId(courseFee.getFeeTypeId());
				feesDto.setFeeType(FeeTypeLocalServiceUtil.fetchFeeType(courseFee.getFeeTypeId()).getTypeName());
				feesDto.setAmount(Math.round(courseFee.getAmount()));
				totalFee += feesDto.getAmount();
				batchFeeBeanItemContainer.addBean(feesDto);
			}
			
			batchFeeTable.refreshRowCache();
			batchFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			batchFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));

		}
		catch(SystemException se)
		{
			se.printStackTrace();
		}

	}

	public void loadBatchFees(long batchId)
	{
		try
		{
			batchFeeBeanItemContainer.removeAllItems();
			List<FeesDto> batchFeeList = AccountsDao.getBatchFeesList(batchId);
			
			long totalFee = 0;
			
			for(FeesDto feesDto:batchFeeList)
			{
				totalFee += feesDto.getAmount();
				batchFeeBeanItemContainer.addBean(feesDto);
			}
			
			batchFeeTable.refreshRowCache();
			batchFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			batchFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));

		}
		catch(SystemException se)
		{
			se.printStackTrace();
		}
		catch(PortalException pe)
		{
			pe.printStackTrace();
		}

	}
	
	public void loadBatchPaymentSchedule(long batchId)
	{
		
		try {
			
			batchPaymentScheduleBeanItemContainer.removeAllItems();
			List<ScheduleDto> scheduleList = AccountsDao.getBatchPaymentSchedules(batchId);
			
			long totalFee = 0;
			
			for(ScheduleDto schedule:scheduleList)
			{
				totalFee += schedule.getAmount();
				batchPaymentScheduleBeanItemContainer.addBean(schedule);
			}
			
			for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT-batchPaymentScheduleBeanItemContainer.size();i++)
			{
				ScheduleDto schedule = new ScheduleDto();
				batchPaymentScheduleBeanItemContainer.addBean(schedule);
			}
			
			batchPaymentScheduleTable.refreshRowCache();
			batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(totalFee));
			
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}


	public long getTotalCourseFee()
	{
		long total = 0;
		
		for(int i=0;i<courseFeeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = courseFeeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}

	public long getTotalBatchFee()
	{
		long total = 0;
		
		for(int i=0;i<batchFeeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = batchFeeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}


	public long getTotalBatchPaymentScheduleAmount()
	{
		long total = 0;
		
		for(int i=0;i<batchPaymentScheduleBeanItemContainer.size();i++)
		{
			ScheduleDto scheduleDto = batchPaymentScheduleBeanItemContainer.getIdByIndex(i);
			
			if(scheduleDto.getAmount()!=null)
			{
				total += scheduleDto.getAmount();
			}
		}
		
		return total;
	}
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay 								= (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

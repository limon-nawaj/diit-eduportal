create table EduPortal_AcademicRecord (
	academicRecordId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organisationId LONG,
	degree VARCHAR(75) null,
	board VARCHAR(75) null,
	year INTEGER,
	result VARCHAR(75) null,
	registrationNo VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_AccademicRecord (
	accademicRecordId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organisationId LONG,
	degree VARCHAR(75) null,
	board VARCHAR(75) null,
	year INTEGER,
	result VARCHAR(75) null,
	registrationNo VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_Assessment (
	assessmentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	assessmentDate DATE null,
	resultDate DATE null,
	totalMark DOUBLE,
	assessmentTypeId LONG,
	batchId LONG,
	subjectId LONG,
	status LONG
);

create table EduPortal_AssessmentStudent (
	assessmentStudentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	obtainMark DOUBLE,
	assessmentId LONG,
	studentId LONG,
	status LONG
);

create table EduPortal_AssessmentType (
	assessmentTypeId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	type_ VARCHAR(75) null
);

create table EduPortal_Attendance (
	attendanceId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	campus LONG,
	attendanceDate DATE null,
	attendanceRate LONG,
	batch LONG,
	subject LONG,
	routineIn DATE null,
	routineOut DATE null,
	actualIn DATE null,
	actualOut DATE null,
	routineDuration LONG,
	actualDuration LONG,
	lateEntry LONG,
	lagTime LONG,
	note VARCHAR(75) null
);

create table EduPortal_AttendanceTopic (
	attendanceTopicId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attendanceId LONG,
	topicId LONG,
	description VARCHAR(75) null
);

create table EduPortal_Batch (
	batchId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batchName VARCHAR(75) null,
	courseId LONG,
	sessionId LONG,
	startDate DATE null,
	endDate DATE null,
	batchTeacherId LONG,
	status INTEGER,
	note VARCHAR(75) null
);

create table EduPortal_BatchFee (
	batchFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_BatchPaymentSchedule (
	batchPaymentScheduleId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	scheduleDate DATE null,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_BatchStudent (
	batchStudentId LONG not null primary key IDENTITY,
	studentId LONG,
	batchId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	startDate DATE null,
	endDate DATE null,
	note VARCHAR(75) null,
	status INTEGER
);

create table EduPortal_BatchSubject (
	batchSubjectId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	batchId LONG,
	subjectId LONG,
	teacherId LONG,
	startDate DATE null,
	hours INTEGER
);

create table EduPortal_BatchTeacher (
	batchTeacherId LONG not null primary key IDENTITY,
	teacherId LONG,
	startDate DATE null,
	endDate DATE null,
	batchId LONG
);

create table EduPortal_Chapter (
	chapterId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	sequence LONG,
	name VARCHAR(75) null,
	subjectId LONG,
	note VARCHAR(75) null
);

create table EduPortal_ClassRoutineEvent (
	classRoutineEventId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	subjectId LONG,
	day INTEGER,
	startTime DATE null,
	endTime DATE null,
	roomId LONG
);

create table EduPortal_ClassRoutineEventBatch (
	classRoutineEventBatchId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	classRoutineEventId LONG,
	batchId LONG
);

create table EduPortal_Comments (
	commentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	studentId LONG,
	commentDate DATE null,
	comments VARCHAR(75) null
);

create table EduPortal_CommunicationRecord (
	communicationRecordId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	communicationBy LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batch LONG,
	date_ DATE null,
	media LONG,
	subject LONG,
	communicationWith LONG,
	details VARCHAR(75) null,
	status LONG
);

create table EduPortal_CommunicationStudentRecord (
	CommunicationStudentRecorId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	communicationBy LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	communicationRecordId LONG,
	studentId LONG
);

create table EduPortal_Counseling (
	counselingId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	counselingDate DATE null,
	medium INTEGER,
	name VARCHAR(75) null,
	maximumQualifications VARCHAR(75) null,
	mobile VARCHAR(75) null,
	email VARCHAR(75) null,
	address VARCHAR(75) null,
	note VARCHAR(75) null,
	source INTEGER,
	sourceNote VARCHAR(75) null,
	status INTEGER,
	statusNote VARCHAR(75) null,
	majorInterestCourse LONG
);

create table EduPortal_CounselingCourseInterest (
	CounselingCourseInterestId LONG not null primary key IDENTITY,
	courseId LONG,
	counselingId LONG
);

create table EduPortal_Course (
	courseId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseCode VARCHAR(75) null,
	courseName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table EduPortal_CourseFee (
	courseFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_CourseOrganization (
	courseOrganizationId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG
);

create table EduPortal_CourseSession (
	sessionId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	sessionName VARCHAR(75) null,
	courseId LONG
);

create table EduPortal_CourseSubject (
	courseSubjectId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	subjectId LONG
);

create table EduPortal_DailyWork (
	dailyWorkId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	worKingDay DATE null,
	taskId LONG,
	time_ INTEGER,
	note VARCHAR(75) null
);

create table EduPortal_Designation (
	designationId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	designationTitle VARCHAR(75) null
);

create table EduPortal_Employee (
	employeeId LONG not null primary key,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeUserId LONG,
	name VARCHAR(75) null,
	gender LONG,
	religion LONG,
	blood LONG,
	dob DATE null,
	designation LONG,
	branch LONG,
	status LONG,
	presentAddress VARCHAR(75) null,
	permanentAddress VARCHAR(75) null,
	photo LONG,
	supervisor LONG,
	role LONG
);

create table EduPortal_EmployeeAttendance (
	employeeAttendanceId LONG not null primary key IDENTITY,
	employeeId LONG,
	expectedStart DATE null,
	expectedEnd DATE null,
	realTime DATE null,
	status LONG
);

create table EduPortal_EmployeeEmail (
	emailId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	workEmail VARCHAR(75) null,
	personalEmail VARCHAR(75) null,
	status LONG
);

create table EduPortal_EmployeeMobile (
	mobileId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	mobile VARCHAR(75) null,
	homePhone VARCHAR(75) null,
	status LONG
);

create table EduPortal_EmployeeRole (
	employeeRoleId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	role VARCHAR(75) null,
	startTime DATE null,
	endTime DATE null
);

create table EduPortal_EmployeeTimeSchedule (
	employeeTimeScheduleId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	employeeId LONG,
	day INTEGER,
	startTime DATE null,
	endTime DATE null,
	duration DOUBLE
);

create table EduPortal_Experiance (
	experianceId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	organization VARCHAR(75) null,
	designation VARCHAR(75) null,
	startDate DATE null,
	endDate DATE null,
	currentStatus INTEGER,
	studentId LONG
);

create table EduPortal_FeeType (
	feeTypeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	typeName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table EduPortal_Leave (
	leaveId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	applicationDate DATE null,
	employee LONG,
	responsibleEmployee LONG,
	startDate DATE null,
	endDate DATE null,
	numberOfDay DOUBLE,
	phoneNumber VARCHAR(75) null,
	causeOfLeave VARCHAR(75) null,
	whereEnjoy VARCHAR(75) null,
	firstRecommendation LONG,
	secondRecommendation LONG,
	leaveCategory LONG,
	responsibleEmployeeStatus INTEGER,
	applicationStatus INTEGER,
	comments VARCHAR(75) null,
	complementedBy LONG
);

create table EduPortal_LeaveCategory (
	leaveCategoryId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null,
	totalLeave INTEGER
);

create table EduPortal_LeaveDayDetails (
	leaveDayDetailsId LONG not null primary key IDENTITY,
	leaveId LONG,
	leaveDate DATE null,
	day DOUBLE
);

create table EduPortal_LeaveReceiver (
	leaveReceiverId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	employeeId LONG
);

create table EduPortal_LessonAssessment (
	lessonAssessmentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	lessonPlanId LONG,
	assessmentType LONG,
	number_ LONG
);

create table EduPortal_LessonPlan (
	lessonPlanId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null,
	organization LONG,
	subject LONG,
	planDate DATE null,
	totalClass LONG,
	totalDuration LONG,
	objective VARCHAR(75) null
);

create table EduPortal_LessonTopic (
	lessonTopicId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	lessonPlanId LONG,
	topic LONG,
	classSequence LONG,
	resource VARCHAR(75) null,
	activities VARCHAR(75) null
);

create table EduPortal_Payment (
	paymentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE,
	paymentDate DATE null
);

create table EduPortal_PersonEmail (
	personEmailId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	ownerType INTEGER,
	personEmail VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_PhoneNumber (
	phoneNumberId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	ownerType INTEGER,
	phoneNumber VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_Room (
	roomId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	label VARCHAR(75) null,
	description VARCHAR(75) null,
	roomType INTEGER
);

create table EduPortal_StatusHistory (
	statusHistoryId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	batchStudentId LONG,
	fromStatus INTEGER,
	toStatus INTEGER
);

create table EduPortal_Student (
	studentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	name VARCHAR(75) null,
	fatherName VARCHAR(75) null,
	motherName VARCHAR(75) null,
	presentAddress VARCHAR(75) null,
	permanentAddress VARCHAR(75) null,
	homePhone VARCHAR(75) null,
	dateOfBirth DATE null,
	gender VARCHAR(75) null,
	religion VARCHAR(75) null,
	nationality VARCHAR(75) null,
	photo LONG,
	status INTEGER,
	studentUserID LONG
);

create table EduPortal_StudentAttendance (
	attendanceTopicId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attendanceId LONG,
	studentId LONG,
	status INTEGER,
	attendanceDate DATE null
);

create table EduPortal_StudentDiscount (
	studentDiscountId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	scholarships DOUBLE,
	discount DOUBLE
);

create table EduPortal_StudentDocument (
	documentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	type_ VARCHAR(75) null,
	documentData LONG,
	description VARCHAR(75) null,
	studentId LONG,
	fileEntryId LONG
);

create table EduPortal_StudentFee (
	studentFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_StudentPaymentSchedule (
	studentPaymentScheduleId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	scheduleDate DATE null,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_Subject (
	subjectId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	subjectCode VARCHAR(75) null,
	subjectName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table EduPortal_SubjectLesson (
	subjectLessonId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	batchId LONG,
	subjectId LONG,
	lessonPlanId LONG
);

create table EduPortal_Task (
	taskId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null
);

create table EduPortal_TaskDesignation (
	taskDesignationId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	taskId LONG,
	designationId LONG
);

create table EduPortal_Topic (
	topicId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	chapterId LONG,
	topic VARCHAR(75) null,
	parentTopic LONG,
	time_ LONG,
	note VARCHAR(75) null
);
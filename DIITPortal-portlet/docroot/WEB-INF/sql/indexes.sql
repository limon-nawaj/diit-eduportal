create index IX_DF599EEB on EduPortal_AcademicRecord (studentId);

create index IX_CEB3246 on EduPortal_AccademicRecord (studentId);

create index IX_F8DDFDE0 on EduPortal_Assessment (batchId);
create index IX_B1B05CC6 on EduPortal_Assessment (batchId, status);
create index IX_30DDEF63 on EduPortal_Assessment (companyId);
create index IX_18C7E547 on EduPortal_Assessment (organizationId);
create index IX_D7D8D29D on EduPortal_Assessment (subjectId, assessmentTypeId);
create index IX_85E4A578 on EduPortal_Assessment (subjectId, status);
create index IX_655CAEDF on EduPortal_Assessment (userId);

create index IX_1C9BB513 on EduPortal_AssessmentStudent (assessmentId);
create index IX_38BCABF3 on EduPortal_AssessmentStudent (assessmentId, studentId);
create index IX_B83DA086 on EduPortal_AssessmentStudent (companyId);
create index IX_9A487204 on EduPortal_AssessmentStudent (organizationId);
create index IX_DD00489C on EduPortal_AssessmentStudent (userId);

create index IX_184865C9 on EduPortal_AssessmentType (companyId);
create index IX_4D857EA1 on EduPortal_AssessmentType (organizationId);

create index IX_60AD57DE on EduPortal_Attendance (batch);
create index IX_C38215C9 on EduPortal_Attendance (batch, attendanceDate);
create index IX_48DA7A9C on EduPortal_Attendance (companyId);
create index IX_7F4D06BB on EduPortal_Attendance (subject, attendanceDate);
create index IX_759A6378 on EduPortal_Attendance (userId, batch, subject);

create index IX_DE1CFF27 on EduPortal_AttendanceTopic (attendanceId);

create index IX_A501BE82 on EduPortal_Batch (batchId);
create index IX_63140F85 on EduPortal_Batch (companyId);
create index IX_A0E0B10D on EduPortal_Batch (courseId);
create index IX_560B78A3 on EduPortal_Batch (organizationId, companyId);
create index IX_1F256ED2 on EduPortal_Batch (organizationId, companyId, batchName);
create index IX_6EE8AC2F on EduPortal_Batch (organizationId, courseId);

create index IX_9E5D2136 on EduPortal_BatchFee (batchId);

create index IX_7BDC8E1F on EduPortal_BatchPaymentSchedule (batchId);

create index IX_A5040E1 on EduPortal_BatchStudent (batchId);
create index IX_BBC5B1C6 on EduPortal_BatchStudent (companyId, organizationId);
create index IX_BD70BB7F on EduPortal_BatchStudent (organizationId, batchId);
create index IX_9999AA07 on EduPortal_BatchStudent (organizationId, batchId, studentId);
create index IX_596DACE2 on EduPortal_BatchStudent (studentId);

create index IX_78A9DD50 on EduPortal_BatchSubject (batchId);
create index IX_D5B209BD on EduPortal_BatchSubject (batchId, teacherId);
create index IX_E736C7D7 on EduPortal_BatchSubject (organizationId);
create index IX_790131E4 on EduPortal_BatchSubject (organizationId, courseId, batchId);
create index IX_E788DC02 on EduPortal_BatchSubject (subjectId);
create index IX_6E6A07B8 on EduPortal_BatchSubject (teacherId);

create index IX_4F4838DA on EduPortal_BatchTeacher (batchId);
create index IX_16DFA8C2 on EduPortal_BatchTeacher (teacherId);

create index IX_225809D7 on EduPortal_Chapter (chapterId, sequence);
create index IX_C66FFE12 on EduPortal_Chapter (companyId);
create index IX_C0C92741 on EduPortal_Chapter (subjectId);

create index IX_2B43CDF1 on EduPortal_ClassRoutineEvent (companyId);
create index IX_B38D9821 on EduPortal_ClassRoutineEvent (roomId);
create index IX_C9F87459 on EduPortal_ClassRoutineEvent (subjectId, classRoutineEventId);
create index IX_25CF67AC on EduPortal_ClassRoutineEvent (subjectId, day);

create index IX_B225E3D6 on EduPortal_ClassRoutineEventBatch (batchId);
create index IX_A03DB9EA on EduPortal_ClassRoutineEventBatch (classRoutineEventId);
create index IX_B7C42FD9 on EduPortal_ClassRoutineEventBatch (companyId);

create index IX_DC9C02D1 on EduPortal_Comments (userId);
create index IX_564E1834 on EduPortal_Comments (userId, batchId);
create index IX_B1C94473 on EduPortal_Comments (userId, organizationId);
create index IX_3FF38CD2 on EduPortal_Comments (userId, organizationId, batchId);

create index IX_EC761ED on EduPortal_CommunicationRecord (communicationBy);

create index IX_8ABC2D39 on EduPortal_CommunicationStudentRecord (communicationRecordId);

create index IX_34F1972C on EduPortal_Counseling (counselingId);
create index IX_BBA4D3EA on EduPortal_Counseling (userId);

create index IX_1FD5982A on EduPortal_Course (companyId);
create index IX_BD61915A on EduPortal_Course (courseCode);
create index IX_CF65AE78 on EduPortal_Course (courseName);
create index IX_EDEDA0E0 on EduPortal_Course (organizationId);

create index IX_6573983E on EduPortal_CourseFee (courseId);

create index IX_FB9C49B7 on EduPortal_CourseOrganization (companyId);
create index IX_322F689B on EduPortal_CourseOrganization (courseId);
create index IX_7848DF73 on EduPortal_CourseOrganization (organizationId);

create index IX_93A78564 on EduPortal_CourseSession (companyId);
create index IX_7927834E on EduPortal_CourseSession (courseId);
create index IX_440E7E6 on EduPortal_CourseSession (organizationId);
create index IX_9F1CC6D on EduPortal_CourseSession (sessionName);

create index IX_1947168E on EduPortal_CourseSubject (companyId);
create index IX_121C24E4 on EduPortal_CourseSubject (courseId);
create index IX_E05F80FC on EduPortal_CourseSubject (organizationId);
create index IX_13A03FBD on EduPortal_CourseSubject (subjectId);

create index IX_A68B90F0 on EduPortal_DailyWork (employeeId);
create index IX_67A93A23 on EduPortal_DailyWork (employeeId, taskId, worKingDay);
create index IX_5FC7C32F on EduPortal_DailyWork (employeeId, worKingDay);
create index IX_A641AEC7 on EduPortal_DailyWork (taskId);

create index IX_9B8EC828 on EduPortal_Designation (companyId);

create index IX_4BAE33B7 on EduPortal_Employee (companyId);
create index IX_94D7754D on EduPortal_Employee (companyId, branch);
create index IX_8134E4D9 on EduPortal_Employee (employeeUserId);
create index IX_D8D93573 on EduPortal_Employee (organizationId);
create index IX_14A88E2D on EduPortal_Employee (supervisor);

create index IX_42291D34 on EduPortal_EmployeeEmail (employeeId);

create index IX_20B9E7F0 on EduPortal_EmployeeMobile (employeeId);
create index IX_A04936D6 on EduPortal_EmployeeMobile (employeeId, status);

create index IX_35825061 on EduPortal_EmployeeRole (companyId);
create index IX_DD4E7309 on EduPortal_EmployeeRole (organizationId);

create index IX_D7A79412 on EduPortal_EmployeeTimeSchedule (employeeId);
create index IX_FAFB11FA on EduPortal_EmployeeTimeSchedule (employeeId, day);

create index IX_11791C75 on EduPortal_Experiance (studentId);

create index IX_40870282 on EduPortal_FeeType (typeName);

create index IX_1000C722 on EduPortal_Leave (employee);
create index IX_861AFCA2 on EduPortal_Leave (organizationId);
create index IX_829123F4 on EduPortal_Leave (responsibleEmployee);

create index IX_5BF97840 on EduPortal_LeaveCategory (organizationId);

create index IX_EB62197C on EduPortal_LeaveDayDetails (leaveId);

create index IX_800EDF6C on EduPortal_LeaveReceiver (employeeId);
create index IX_CEB5AAF1 on EduPortal_LeaveReceiver (organizationId);

create index IX_773198ED on EduPortal_LessonAssessment (lessonPlanId);

create index IX_EC3B1038 on EduPortal_LessonPlan (subject);
create index IX_49CCE5DE on EduPortal_LessonPlan (userId);

create index IX_43800750 on EduPortal_LessonTopic (lessonPlanId);
create index IX_EF3DF2CF on EduPortal_LessonTopic (lessonPlanId, topic);

create index IX_59686A0E on EduPortal_Payment (studentId, batchId);

create index IX_AABD7396 on EduPortal_PersonEmail (studentId);

create index IX_95D70086 on EduPortal_PhoneNumber (studentId);

create index IX_450C442A on EduPortal_Room (companyId);
create index IX_1C0274E0 on EduPortal_Room (organizationId);
create index IX_6F6377C4 on EduPortal_Room (organizationId, label);
create index IX_A4F67488 on EduPortal_Room (roomId);

create index IX_D0B05266 on EduPortal_Student (organizationId);
create index IX_612C8CC0 on EduPortal_Student (organizationId, studentId);

create index IX_15378E25 on EduPortal_StudentAttendance (attendanceId);
create index IX_7B6E0EB9 on EduPortal_StudentAttendance (studentId);

create index IX_7057C124 on EduPortal_StudentDiscount (studentId, batchId);

create index IX_96178949 on EduPortal_StudentDocument (documentId);
create index IX_C588C247 on EduPortal_StudentDocument (studentId);

create index IX_45C99EAD on EduPortal_StudentFee (studentId, batchId);

create index IX_790ADFA4 on EduPortal_StudentPaymentSchedule (studentId, batchId);

create index IX_6B43C793 on EduPortal_Subject (companyId);
create index IX_7E31AB17 on EduPortal_Subject (organizationId);
create index IX_689D2BD4 on EduPortal_Subject (subjectCode);
create index IX_7AA148F2 on EduPortal_Subject (subjectName);

create index IX_8EA6895F on EduPortal_SubjectLesson (batchId, subjectId);
create index IX_EE969869 on EduPortal_SubjectLesson (organizationId, userId);

create index IX_3417F3C0 on EduPortal_Task (companyId);

create index IX_9F69CC4D on EduPortal_TaskDesignation (companyId);
create index IX_76F11947 on EduPortal_TaskDesignation (designationId);
create index IX_E4F0173B on EduPortal_TaskDesignation (designationId, taskId);
create index IX_8D25E6EF on EduPortal_TaskDesignation (taskId);

create index IX_6490780 on EduPortal_Topic (chapterId);
create index IX_CA282FB0 on EduPortal_Topic (companyId);
create index IX_8E34031D on EduPortal_Topic (parentTopic);